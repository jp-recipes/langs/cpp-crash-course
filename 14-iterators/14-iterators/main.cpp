#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <deque>
#include <iterator>
#include <forward_list>
#include <list>
#include <vector>
#include <iterator>

/* Iterators */

// Iterators are the STL component that provedes the interface between containers and algorithms to manipulate them
// An iterators is an interface to a type that knos how to traverse a partocular sequence and exposes simple, pointer-like operations to elements

// Every iterator supports at least the following operations
//
//   Access the current element (operator*) for reading and/or writing
//   Go to the next element (operator++)
//   Copy construct

// Iterators are categorized based on which additional operations they support
// This categories determine which algorithms are available and what you can do with an iterator in your generic code

/* Iterator Categories */

/* Output Iterators */

// You can use an output iterator to write into and increment but nothing else

// Again, unless you're writing a C++ library, it's unlikely that you'll have to implement your own output iterator types; however, you'll use them quite a lot

/* Insert Iterators */

// An insert iterator (or inserter) is an output iterator that wraps a container and transforms writes (assignments) into insertions

// Three insert iterators exist int the STL's <iterator> header as class templates

//   std::back_insert_iterator
//   std::front_insert_iterator
//   std::insert_iterator

// The STL also offers three convinience functions for building these iterators

//   std::back_inserter
//   std::front_inserter
//   std::inserter

TEST_CASE("Insert iterators convert writes into container insertions.") {
    std::deque<int> dq;
    auto back_instr = std::back_inserter(dq);
    *back_instr = 2;  // 2
    ++back_instr;
    *back_instr = 4;  // 2 4
    ++back_instr;

    auto front_instr = std::front_inserter(dq);
    *front_instr = 1; // 1 2 4
    ++front_instr;

    auto instr = std::inserter(dq, dq.begin()+2);
    *instr = 3;      // 1 2 3 4
    instr++;

    REQUIRE(dq[0] == 1);
    REQUIRE(dq[1] == 2);
    REQUIRE(dq[2] == 3);
    REQUIRE(dq[3] == 4);
}

/* Input Iterators */

// You can use an input iterator to read from, increment, and check for equality
// It's the FOIL to the output iterator
// You can only iterate through an input iterator once

// The usual pattern when reading from an input iterator is to obtain a half-open range with a begin and an end iterator
// To read through the range, you read the begin iterator using operator* followed by an incremante with operator++
// Next, you evaluate whether the iterator equals end. If it does, you've exhausted the range
// If it doesn't you can continue reading/incrementing

// A canonical usage of an input iterator is to wrap a program's standard input (usually the keyboard)
// Once you've read a value from standard input, it's gone. You cannot go back to the beginning and replay

TEST_CASE("std::forward_list begin and end provide input iterators") {
    const std::forward_list<int> easy_as{ 1, 2, 3 };
    auto itr = easy_as.begin();
    REQUIRE(*itr == 1);
    itr++;
    REQUIRE(*itr == 2);
    itr++;
    REQUIRE(*itr == 3);
    itr++;
    REQUIRE(itr == easy_as.end());
}

/* Forward Iterators */

// A forward iterator is an input iterator with additional features; a forward iterator can also traverse multiple times, default construct, and copy assign
// You can use a forward iterator in place of a input iterator in all cases
// All STL containers provide forward iterators

TEST_CASE("std::forward_list's begin and end provide forward iterators") {
    const std::forward_list<int> easy_as{ 1, 2, 3 };
    auto itr1 = easy_as.begin();
    auto itr2{ itr1 };
    int double_sum{};
    while (itr1 != easy_as.end()) {
        double_sum += *(itr1++);
    }
    while (itr2 != easy_as.end()) {
        double_sum += *(itr2++);
    }
    REQUIRE(double_sum == 12);
}

/* Bidirectional Iterators */

// A bidirectional iterator is a forward iterator that can also iterate backward
// You can use a bidirectional iterator in place of a forward or input iterator

// Bidirectional iterators permit backward iteration with operator-- and operator-(int)
// The STL containers that provide bidirectional iterators are array, list, deque, vector, and all of the ordered associative containers

TEST_CASE("std::list begin and end provide bidirectional iterators") {
    const std::list<int> easy_as{ 1, 2, 3 };
    auto itr = easy_as.begin();
    REQUIRE(*itr == 1);
    itr++;
    REQUIRE(*itr == 2);
    itr--;
    REQUIRE(*itr == 1);
    REQUIRE(itr == easy_as.cbegin());
}

/* Random-Access Iterators */

// A random-access iterator is a bidirectional iterator that supports random element access
// You can use random-access iterator in place of bidirectional, forward, and input iterators in all cases
// Random-access iterators permit access with operator[] and also iterator arithmetic, such as adding or substracting integer values and substracting other iterators to find distances

// The STL containers that provide random-access iterators are array, vector, and deque

TEST_CASE("std::vector begin and end provide random-access iterators") {
    const std::vector<int> easy_as{ 1, 2, 3 };
    auto itr = easy_as.begin();
    REQUIRE(itr[0] == 1);
    itr++;
    REQUIRE(*(easy_as.cbegin()+2) == 3);
    REQUIRE(easy_as.cend() - itr == 2);
}

/* Contiguous Iterators */

// A contiguous iterator is a random-access iterator with elements adjacent in memory
// For a contiguous iterator itr, all elements itr[n] and itr[n+1] satisfy the following realtion for all valid selections of indicies n and offsets i

//   &itr[n] + i == &itr[n+i]

/* Mutable Iterators */

// All forward iterators, bidirectional iterators, random-access iterators, and contiguous iterators can support read-only or read-write modes

/* Auxiliary Iterator Functions */

/* std::advance */

// The std::advance auxiliary iterator function allows you to increment or decrement by the desired amount

//   void std::advance(InputIterator& itr, Distance d);


// The advance function doesn't perform bounds checking, so you must ensure that you've not exceeded the valid range for the iterator's position

//   Input iterator
//     The advance function will invoke itr++ the correct number of times; dis cannot be negative
//
//   Bidirection iterator
//     The function will invoke itr++ otr itr-- the correct number of times
//
//   Random access iterator
//     It will invoke itr+=dist; dist can be negative

TEST_CASE("advance modifies input iterators") {
    std::vector<unsigned char> mission{
        0x9e, 0xc4, 0xc1, 0x29,
        0x49, 0xa4, 0xf3, 0x14,
        0x74, 0xf2, 0x99, 0x05,
        0x8c, 0xe2, 0xb2, 0x2a
    };

    auto itr = mission.begin();
    std::advance(itr, 4);
    REQUIRE(*itr == 0x49);
    std::advance(itr, 4);
    REQUIRE(*itr == 0x74);
    std::advance(itr, -8);
    REQUIRE(*itr == 0x9e);
}

/* std::next and std::prev */

// The std::next and std::prev auxiliary iterator functions are function templates that compute offset from a given iterator

//   ForwardIterator std::next(ForwardIterator& itr, Distance d=1);
//   BidirectionalIterator std::prev(BidirectionalIterator& itr, Distance d=1);

// The function next accepts at least a foward iterator and optionally a distance

TEST_CASE("next returns iterators at given offsets") {
    std::vector<unsigned char> mission{
        0x9e, 0xc4, 0xc1, 0x29,
        0x49, 0xa4, 0xf3, 0x14,
        0x74, 0xf2, 0x99, 0x05,
        0x8c, 0xe2, 0xb2, 0x2a
    };

    auto itr1 = mission.begin();
    std::advance(itr1, 4);
    REQUIRE(*itr1 == 0x49);

    auto itr2 = std::next(itr1);
    REQUIRE(*itr2 == 0xa4);

    auto itr3 = std::next(itr1, 4);
    REQUIRE(*itr3 == 0x74);

    REQUIRE(*itr1 == 0x49);
}

/* std::distance */

// The std::distance auxiliary iterator function enables you to compute the distance between two input iterators itr1 and itr2

//   Distance std::distance(InputIterator itr1, InputIterator itr2);

// If the iterators are not random access, itr2 must refer to an element after itr1
// It's good idea to ensure that itr2 comes after itr1, because you'll get undefined behavior if you accidentally violate this requirement and the iterators are not random access

TEST_CASE("distance returns the number of elements between iterators") {
    std::vector<unsigned char> mission{
        0x9e, 0xc4, 0xc1, 0x29,
        0x49, 0xa4, 0xf3, 0x14,
        0x74, 0xf2, 0x99, 0x05,
        0x8c, 0xe2, 0xb2, 0x2a
    };

    auto eighth = std::next(mission.begin(), 8);
    auto fifth = std::prev(eighth, 3);
    REQUIRE(std::distance(fifth, eighth));
}

/* std::iter_swap */

// The std::iter_swap auxiliary iterator function allows you to swap the values pointed to by two forward iterators itr1 and itr2

//   Distance std::iter_swap(ForwardIterator itr1, ForwardIterator itr2);

TEST_CASE("iter_swap swaps pointed-to elements") {
    std::vector<long> easy_as{ 3, 2, 1 };
    std::iter_swap(easy_as.begin(), std::next(easy_as.begin(), 2));
    REQUIRE(easy_as[0] == 1);
    REQUIRE(easy_as[1] == 2);
    REQUIRE(easy_as[2] == 3);
}

/* Additional Iterator Adapters */

// In addition to insert iterators, the STL provides move iterator adapters and reverse iterator adapters to modify iterator behavior

// Note
// The STL also provides stream iterator adapters, which you'll learn

/* Move Iterator Adapters */

// A move iterator adapter is a class template that converts all iterator accesses into move operations
// The convenience function template std::make_move_iterator in the <iterator> header accepts a single iterator argument and return a move iterator adapter

struct Movable {
    Movable(int id): id{ id }{}
    Movable(Movable&& m) {
        id = m.id;
        m.id = -1;
    }
    int id;
};

TEST_CASE("move iterators convert accesses into move operations") {
    std::vector<Movable> donor;
    donor.emplace_back(1);
    donor.emplace_back(2);
    donor.emplace_back(3);
    std::vector<Movable> recipient { // Range Constructor
        std::make_move_iterator(donor.begin()),
        std::make_move_iterator(donor.end())
    };
    REQUIRE(donor[0].id == -1);
    REQUIRE(donor[1].id == -1);
    REQUIRE(donor[2].id == -1);
    REQUIRE(recipient[0].id == 1);
    REQUIRE(recipient[1].id == 2);
    REQUIRE(recipient[2].id == 3);
}

/* Reverse Iterator Adapters */

// A reverse iterator adapter is a class template that swaps an iterator's increment and decrement operators

// Almost all containers reviews until now expose reverse iterators with rbegin/rend/crbegin/crend

TEST_CASE("reverse iterators can initialize containers") {
    std::list<int> original{ 3, 2, 1 };
    std::vector<int> easy_as{ original.crbegin(), original.crend() };
    REQUIRE(easy_as[0] == 1);
    REQUIRE(easy_as[1] == 2);
    REQUIRE(easy_as[2] == 3);
}

TEST_CASE("make_revers_iterator converts a normal iterator") {
    std::list<int> original{ 3, 2, 1 };
    auto begin = std::make_reverse_iterator(original.cend());
    auto end = std::make_reverse_iterator(original.cbegin());
    std::vector<int> easy_as{ begin, end };
    REQUIRE(easy_as[0] == 1);
    REQUIRE(easy_as[1] == 2);
    REQUIRE(easy_as[2] == 3);
}
