#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <string>
#include <regex>

/* Strings */

// The STL provides a special string container for human-language data, such as words, sentences, and markup languages
// Available int the <string> header, the std:basic_string is a class template that you can specialize on a string's underlying character type
// As a sequential container, basic_string is essentially similar to a vector but with some special facilities for manipulating language

// STL, basic_string provides major safety and feature improvements over C-style or null-terminated strings

/* std::string */

// The STL provides four basic_string specialization in the <string> header
//
//   std::string
//     for char is used for character sets like ASCII
//   std::wstring
//     for wchar_t is large enough to contain the largest character of the implementation's locale
//   std::u16string
//     for char16_t is used for character sets like UTF-16
//   std::u32string
//     for char32_t is used for character sets like UTF-32

/* Constructing */

// The basic_string container takes three template parameters
//
//   The underlying character type, T
//   The underlying type's traits, Traits
//   An allocator, Alloc

// Of these, only T is required

//   std::basic_string<T, Traits=std::char_traits<T>, Alloc=std::allocator<T>>

// The basic_string<T> container supports the same constructors as vector<T>

TEST_CASE("std::string supports constructing") {
    SECTION("empty strings") {
        std::string cheese;
        REQUIRE(cheese.empty());
    }
    SECTION("repeated characters") {
        std::string readside_assistance(3, 'A');
        REQUIRE(readside_assistance == "AAA");
    }
}

// The string constructor also offers two const char*-based constructors
// If the argument points to a null-terminated

TEST_CASE("std::string supports constructing substrings ") {
    auto word = "gobbledygook";
    REQUIRE(std::string(word) == "gobbledygook");
    REQUIRE(std::string(word, 6) == "gobble");
}

// You can construct strings from other strings
// As an STL container, string fully supports copy and move semantics
// You can also construct a string from a substring

TEST_CASE("std::string support") {
    std::string word("catawampus");
    SECTION("copy constructing") {
        REQUIRE(std::string(word) == "catawampus");
    }
    SECTION("move constructing") {
        REQUIRE(std::string(move(word)) == "catawampus");
    }
    SECTION("constructing from substring") {
        REQUIRE(std::string(word, 0 , 3) == "cat");
        REQUIRE(std::string(word, 4) == "wampus");
    }
}

// The string class also supports literal construction with std::string_literals::operator""s

TEST_CASE("constructing a string with") {
    SECTION("std::string(char*) stops at embedded nulls") {
        std::string str("idioglossia\0ellohay!");
        REQUIRE(str.length() == 11);
    }
    SECTION("operator\"\"s incorporates embedded nulls") {
        using namespace std::string_literals;
        auto str_lit = "idioglossia\0ellohay!"s;
        REQUIRE(str_lit.length() == 20);
    }
}

/* String Storage and Small String Optimization */

// The most popular STL implementations have small string optimizations (SSO)
// The SSO places the contents of a string within the object's storage (rather than dynamic storage) if the contents are small enough
// As a general rule, a string with fewer than 24 bytes is an SSO candidate
// Implementers make this optimization because in many modern programs, mostr strings are shorts

// Note
// Practically, SSO affects moves in two ways
// First, any references to the elements of a strings will invalidate if the string moves
// Second, moves are potentially slower for string than vectors because string need to check for SSO

/* Element and Iterator Access */

// For interoperation with C-style APIs, string also exposes a c_str method, which returns a non-modifiable, null-terminated version of the string as a const char*

TEST_CASE("string's c_str makes null-terminated strings") {
    std::string word("horripilation");
    auto as_cstr = word.c_str();
    REQUIRE(as_cstr[0] == 'h');
    REQUIRE(as_cstr[1] == 'o');
    REQUIRE(as_cstr[11] == 'o');
    REQUIRE(as_cstr[12] == 'n');
    REQUIRE(as_cstr[13] == '\0');
}

// Generally, c_str and data produce identical results except that reference returned by data can be non-const

/* String Comparisons */

// Note
// Technically, lexicographical comparisons depends on the encoding of the string
// It's theorically posible that a system could use a default encoding where the alphabet is in some completely jumbled order (such as the nearly onsolete EBCDIC encoding, which put lowercase letters before uppercase letters), which would affect string comparison
// For ASCII-compatible encodings, you don't need to worry since they imply the expected lexicographical behavior

TEST_CASE("std::string supports comparison with") {
    using namespace std::literals::string_literals;
    std::string word("allusion");
    SECTION("operator== and !=") {
        REQUIRE(word == "allusion");
        REQUIRE(word == "allusion"s);
        REQUIRE(word != "Allusion"s);
        REQUIRE(word != "illusion"s);
        REQUIRE_FALSE(word == "illusion"s);
    }
    SECTION("operator<") {
        REQUIRE(word < "illusion");
        REQUIRE(word < "illusion"s);
        REQUIRE(word > "Illusion"s);
    }
}

/* Manipulating Elements */

// For manipulating elements, string has a lot of methods
// It supports all the methods of vectr<char> plus many others useful to manipulating human-language data

/* Adding Elements */

TEST_CASE("std::string supports appending with ") {
    std::string word("butt");
    SECTION("push_back") {
        word.push_back('e');
        REQUIRE(word == "butte");
    }
    SECTION("operator+=") {
        word += "erfinger";
        REQUIRE(word == "butterfinger");
    }
    SECTION("append char") {
        word.append(1, 's');
        REQUIRE(word == "butts");
    }
    SECTION("append char*") {
        word.append("stockings", 5);
        REQUIRE(word == "buttstock");
    }
    SECTION("append (half-open range)") {
        std::string other{"onomatopoeia"};
        word.append(other.begin(), other.begin()+2);
        REQUIRE(word == "button");
    }
}

/* Removing Elements */

TEST_CASE("std::string supports removal with") {
    std::string word{"therein"};
    SECTION("pop_back") {
        word.pop_back();
        word.pop_back();
        REQUIRE(word == "there");
    }
    SECTION("clear") {
        word.clear();
        REQUIRE(word.empty());
    }
    SECTION("erase using half-open range") {
        word.erase(word.begin(), word.begin()+3);
        REQUIRE(word == "rein");
    }
    SECTION("erase using an index and length") {
        word.erase(5, 2);
        REQUIRE(word == "there");
    }
}

/* Replacing Elements */

// To insert and remove elements simultaneously, use string to the replace method, which has manu overloads

TEST_CASE("std:string replace works with") {
    std::string word("substitution");
    SECTION("a range and char*") {
        word.replace(word.begin()+9, word.end(), "e");
        REQUIRE(word == "substitute");
    }
    SECTION("two ranges") {
        std::string other("innuendo");
        word.replace(word.begin(), word.begin()+3, other.begin(), other.begin()+2);
        REQUIRE(word == "institution");
    }
    SECTION("an index/length and a string") {
        std::string other("vers");
        word.replace(3, 6, other);
        REQUIRE(word == "subversion");
    }
}

// The string class offers a resize method to manually set the length of string

TEST_CASE("std::string resize") {
    std::string word("shamp");
    SECTION("can remove elements") {
        word.resize(4);
        REQUIRE(word == "sham");
    }
    SECTION("can add elements") {
        word.resize(7, 'o');
        REQUIRE(word == "shampoo");
    }
}

/* Search */

/* find */

// The find method accepts a string, a C-style string, or a char as its first argument

TEST_CASE("std::string find") {
    using namespace std::literals::string_literals;
    std::string word("pizzazz");
    SECTION("locates substrings from strings") {
        REQUIRE(word.find("zz"s) == 2);   // pi(z)zazz
    }
    SECTION("accepts a position argument") {
        REQUIRE(word.find("zz", 3) == 5); // pizza(z)z
    }
    SECTION("locates substrings from char*") {
        REQUIRE(word.find("zaz") == 3);   // piz(z)azz
    }
    SECTION("returns npos when not found") {
        REQUIRE(word.find('x') == std::string::npos);
    }
}

/* rfind */

// The rfind method is an alternative to find that takes the same arguments but searches in reverse

TEST_CASE("std::string rfind") {
    using namespace std::literals::string_literals;
    std::string word("pizzazz");
    SECTION("locates substrings from strings") {
        REQUIRE(word.rfind("zz"s) == 5);    // pizza(z)z
    }
    SECTION("accepts a position argument") {
        REQUIRE(word.rfind("zz"s, 3) == 2); // pi(z)zazz
    }
    SECTION("locates substring from char*") {
        REQUIRE(word.rfind("zaz") == 3);    // piz(z)azz
    }
    SECTION("returns npos when not found") {
        REQUIRE(word.rfind('x') == std::string::npos);
    }
}

/* find_first_of */

// The find_first_of function accepts a string and locates the first character in this contained in the argument

TEST_CASE("std::string find_first_of") {
    using namespace std::literals::string_literals;
    std::string sentence("I am a Zizzer-Zazzer-Zuzz as you can plainly see.");
    SECTION("locates characters within another string") {
        REQUIRE(sentence.find_first_of("Zz"s) == 7);      // (Z)izzer
    }
    SECTION("accepts a position argument") {
        REQUIRE(sentence.find_first_of("Zz"s, 11) == 14); // (Z)azzer
    }
    SECTION("returns npos when not found") {
        REQUIRE(sentence.find_first_of("Xx"s) == std::string::npos);
    }
}

// A strubg iffers three find_first_of variations

//   find_first_not_of
//     Returns the first character not contained in the string argument
//   find_last_of
//     Performs matching in reverse
//   find_last_not_of
//     Combines the two prior variations: you pass a string containt elements you don't want to find, and find_last_not_of searches in regerse

TEST_CASE("std::string") {
    using namespace std::literals::string_literals;
    std::string sentence("I am a Zizzer-Zazzer-Zuzz as you can plainly see.");
    SECTION("find_last_of find last element within another string") {
        REQUIRE(sentence.find_last_of("Zz"s) == 24);              // Zuz(z)
    }
    SECTION("find_first_not_of finds first element not within another string") {
        REQUIRE(sentence.find_first_not_of(" -IZaeimrz"s) == 22); // Z(u)zz
    }
    SECTION("find_last_not_of finds last element not within another string") {
        REQUIRE(sentence.find_last_not_of(" .es"s) == 43);        // plainl(y)
    }
}

/* Numeric Conversions */

TEST_CASE("STL string conversion function") {
    using namespace std::literals::string_literals;
    SECTION("to_string") {
        REQUIRE("8675309"s == std::to_string(8675309));
    }
    SECTION("to_wstring") {
        REQUIRE(L"109951.1627776"s == std::to_wstring(109951.1627776));
    }
}

// Note
// Thanks to the inherent inaccuracy of the double type, the second unit test might fail on your system

TEST_CASE("STL string conversion functions") {
    using namespace std::literals::string_literals;
    SECTION("stoi") {
        REQUIRE(std::stoi("8675309"s) == 8675309);
    }
    SECTION("stoi") {
        REQUIRE_THROWS_AS(std::stoi("1099511627776"), std::out_of_range);
    }
    SECTION("stoul with all valid characters") {
        size_t last_character{};
        const auto result = std::stoul("0xD3C34C3D"s, &last_character, 16);
        REQUIRE(result == 0xD3C34C3D);
        REQUIRE(last_character == 10);
    }
    SECTION("stoul") {
        size_t last_character{};
        const auto result = std::stoul("42six"s, &last_character);
        REQUIRE(result == 42);
        REQUIRE(last_character == 2);
    }
    SECTION("stod") {
        REQUIRE(std::stod("2.7182818"s) == Approx(2.7182818));
    }
}

/* String View */

// A string view is an object that represents a constant, contiguous sequence of characters
// It's very similar to a const string reference
// In fact, string view classes are often implemented as a pointer to a character sequence and a length

// The STL offers the class template std::basic_string_view int the <string_view> header

/* Constructing */

TEST_CASE("std::string_view supports") {
    SECTION("default construction") {
        std::string_view view;
        REQUIRE(view.data() == nullptr);
        REQUIRE(view.size() == 0);
        REQUIRE(view.empty());
    }
    SECTION("construction from string") {
        std::string word("sacrosanct");
        std::string_view view(word);
        REQUIRE(view == "sacrosanct");
    }
    SECTION("construction from C-string") {
        auto word = "viewership";
        std::string_view view(word);
        REQUIRE(view == "viewership");
    }
    SECTION("construction from C-string and length") {
        auto word = "viewership";
        std::string_view view(word, 4);
        REQUIRE(view == "view");
    }
}

TEST_CASE("std::string_view is modifiable with") {
    std::string_view view("previewing");
    SECTION("remove_prefix") {
        view.remove_prefix(3);
        REQUIRE(view == "viewing");
    }
    SECTION("remove_suffix") {
        view.remove_suffix(3);
        REQUIRE(view == "preview");
    }
}

/* Ownership, Usage, and Efficiency */

// Because string_view doesn't own the sequence to which it refers, it's up to you to ensure that the lifetime of the string_view is a subset of the referred-to sequence's lifetime
// Perhaps the most common usage of string_view is as a function parameter
// When you need to interact with an immutable sequence of characters, it's the first port to call

// Consider the `count_vees` function, which count the frecuency of the letter v in a sequence of characters

size_t count_vees(std::string_view my_view) {
    size_t result{};
    for(auto letter : my_view)
        if (letter == 'v') result++;
    return result;
}

// You could reimplement it by simply replacing `string_view` with `const string&`
//
//   size_t count_vees(const std::string& my_view) {
//       --snip--
//   }

// If string_view is just a drop-in replacement for a `const string&` why bother having it?
// Well, if you invoke count_vees with a std::string, there's no difference
// If you instead invoke count_vees wuth a string literal, there's a big difference: when you pass a string literal for a const string&, you construct a string. WHen you pass s string literal for a string_view, you construct a string_view
// Constructing a string is probably more expensive, because it might have to allocate dynamic memory and it definitely has to copy characters
// A string_view is just a pointer and a length (no copying or allocating is required)

/* Regular Expressions */

/* basic_regex */

// The STL's std::basic_regex class template in the <regex> header represents a regular expression constructed from a pattern
// You'all almost always want to use one of the convenience specializations: std::regex for std::basic_regex<char> or std::wregex for std::basic_regex<wchar_t>

//   assign(s) reassings the patterns to s
//   mark_count() returns the number of groups in the pattern
//   flags() return the syntax flags issued at construction

TEST_CASE("std::basic_regex construct from a string literal") {
    std::regex zip_regex{ R"((\w{2})?(\d{5})(-\d{4})?)" };
    REQUIRE(zip_regex.mark_count() == 3);
}

/* Algorithms */

// The <regex> class contains three algorithms for applying std::basic_regex to a target string: matching, searching, or replacing. Which you choose dependes on the task at hand

/* Matching */

// Matching attempts to marry a regular expression to an entire string
// The STL provides the std::regex_match function for matching, which has four overloads
// First, you can provide regex_match a string, a C-string, or a begin and end iterator forming a half-open range
// The next paramter is an optional reference to a std::match_results that recieves details about the match
// The next paramter is a std::basic_regex that defines the matching, and the final parameter is an optional std::regex_constants::match_flag_type that specifies additional matching options for advances use cases
// The refex_match function returns a bool, which is true if it bound a match

// To summarize, you can invoke regex_match in the following ways

//   regex_match(beg, end, [mrg], rgx, [flg])
//   regex_match(str, [mr], rgx, [flg])

// Either provide a half-open range from beg to end or a string/C-string str search

// The match_results class stores zero or more `std::sub_match` instances
// Somewhat confusingly, if regex_match successfully matches a string, match_results stores the entire matched string as its first element and then stores any submatches as subsequent elements

// The std::sub_match class template has predefined specializations to work with common string types
//
//   std::csub_match for a const char*
//   std::wcsub_match for a const wchar_t*
//   std::ssub_match for a std::string
//   std::wssub_match for a std::wstring

TEST_CASE("std::sub_match") {
    std::regex regex{ R"((\w{2})(\d{5})(-\d{4})?)" };
    std::smatch results;
    SECTION("returns true given matching string") {
        std::string zip("NJ07936-3173");
        const auto matched = std::regex_match(zip, results, regex);
        REQUIRE(matched);
        REQUIRE(results[0] == "NJ07936-3173");
        REQUIRE(results[1] == "NJ");
        REQUIRE(results[2] == "07936");
        REQUIRE(results[3] == "-3173");
    }
    SECTION("returns false given non-matching string") {
        std::string zip("Iomega Zip 100");
        const auto matched = std::regex_match(zip, results, regex);
        REQUIRE_FALSE(matched);
    }
}

/* Searching */

// Searching attempts to match a regular expression to a part of a string
// The STL provides the std::regex_search function for searching

TEST_CASE("when only part of a string matches a regex, std::regex_ ") {
    std::regex regex{ R"((\w{2})(\d{5})(-\d{4})?)" };
    std::string sentence("The string NJ07936-3173 is a ZIP Code.");
    SECTION("match returns false") {
        REQUIRE_FALSE(std::regex_match(sentence, regex));
    }
    SECTION("search returns true") {
        REQUIRE(std::regex_search(sentence, regex));
    }
}

/* Replacing */

// Replacing substitutes regular expression occurences with replacement text
// The STL provides the std::regex_replace function for replacing

// In its most basic usage, you pass regex_replace three arguments
//
//   A source string/C-string/half-open range to search
//   A regular expression
//   A replacement string

TEST_CASE("std::regex_replace") {
    std::regex regex{ "[aeiou]" };
    std::string phrase("queueing and cooeeing in eutopia");
    const auto result = std::regex_replace(phrase, regex, "_");
    REQUIRE(result == "q_____ng _nd c_____ng _n __t_p__");
}

/* Boost String Algorithms */

// SKIPPED
