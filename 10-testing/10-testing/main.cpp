#include <iostream>
#include <stdexcept>
#include <functional>

/* An Extended Example: Taking a Brake */

/*
 Your team is responsible for the autonomous braking service
 The service must determine whether a collision is about to happen and, if so, tell the car to brake
 Your services subscribes to two event types: the SpeedUpdate and CarDetected classes

 SpeedUpdate:
    It tells you that the car's speed has changed

 CarDetected:
    It tells you that some other car has been detected in front of you

 Your system is responsible for publishing a BrakeCommand to the services bus whenever an imminent collision is detected
 */

struct SpeedUpdate {
    double velocity_mps;
};

struct CarDetected {
    double distance_m;
    double velocity_mps;
};

struct BrakeCommand {
    double time_to_collision_s;
};

struct ServiceBus {
    void publish(const BrakeCommand&) { /* TODO */ }
};

/* Implementing AutoBrake */

/*
 AutoBrake will consider the car's initial speed zero

 AutoBrake should have a configurable sensitivity threshold based on how many seconds are forecast until a collision
 The sensitivity must not be less than 1 second
 The default sensitivity is 5 seconds

 AutoBrake must save the car's speed in between SpeedUpdate observations

 Each time AutoBrake observes a CarDetected event, it must publish a BrakeCommand if a collision is forecasted in less time than the configured sensitivity threshold
*/

/* Advantages of TDD */

/*
 The process of writing a test that encodes a requirement 'before' implementing the solution is the fundamental idea behind TDD
 Proponents say htat code written this way tends to be more modular, robust, clean, and well designed
 Writing good tests is the best way to document your code for other developers
 A good test suite is a fully working set of examples that never gets out of sync
 It protects against regressions in functionality whenever you add new features
 */

/* Red-Green-Refactor */

/*
 Red is the first step, and it means to implement a failing tests
 This is done for reveral reasons, principal which is to make sure you're actually testing something
 You might be surprised how common it is to accidentally desing a test that doesn't make any assertions

 Next, you implement code that makes the test pass, no more, no less
 This turns the test from red to green

 Now that you have woking code and a passing test, you can refactor your production code
 To refactor means to restructure existing code without changing its functionality
 */

/* Writing a Skeleton AutoBrake Class */

// Before you can write tests, you need to write a skeleton class, which implements an interface but provides no functionality
// It's useful int TDD because you can't compile a tests without a sheel of the class you are testing

template <typename T>
struct AutoBrake {
    AutoBrake(const T& publish) : publish{ publish }, speed_mps{}, collision_threshold_s{ 5 } { }

    void observe(const SpeedUpdate& x) {
        speed_mps = x.velocity_mps;
    }
    void observe(const CarDetected& cd) {
        const auto relative_velocity_mps = speed_mps - cd.velocity_mps;
        const auto time_to_collision_s = cd.distance_m / relative_velocity_mps;

        if (time_to_collision_s > 0 && time_to_collision_s <= collision_threshold_s) {
            publish(BrakeCommand{ time_to_collision_s });
        }
    }

    void set_collision_threshold_s(double x) {
        if (x < 1) throw std::runtime_error{ "Collision less than 1." };
        collision_threshold_s = x;
    }
    double get_collision_threshold_s() const {
        return collision_threshold_s;
    }
    double get_speed_mps() const {
        return speed_mps;
    }
private:
    double collision_threshold_s;
    double speed_mps;
    const T& publish;
};

/* Assertions. The Building Blocks of Unit Tests */

constexpr void assert_that(bool statement, const char* message) {
    if (!statement) throw std::runtime_error{ message };
}

/* Requirement: Initial Speed Is Zero */

// Before implementing this functionality in AutoBrake, you need to write a unit test that encodes this requirement

void initial_speed_is_zero() {
    AutoBrake auto_brake{ [](const BrakeCommand&){} };
    assert_that(auto_brake.get_speed_mps() == 0L, "speed not equal 0");
}

/* Test Harnesses */

// Is code that executes unit tests

void run_test(void(*unit_test)(), const char* name) {
    try {
        unit_test();
        printf("[+] Test %s successful.\n", name);
    } catch (const std::exception& e) {
        printf("[-] Test failure in %s. %s.\n", name, e.what());
    }
}

/* Getting the Test to Pass */

// To get initial_speed_is_zero to pass, all that's required is to initialize speed_mps to zero in the constructor of AutoBrake

/* Requirement: Default Collision Threshold is Five */

// The default collision threshold needs to be 5

void initial_sensitivity_is_five() {
    AutoBrake auto_brake{ [](const BrakeCommand&) {} };
    assert_that(auto_brake.get_collision_threshold_s() == 5L, "sensitivity is not 5");
}

/* Requirement: Sensitivity Must Always Be Greater Than One */

// To encode the sensitivity validation errors using exceptions, you can build a test that expects an exception to be thrown when collision_threshold_s is set to a value less than 1

void sensitivity_grater_than_1() {
    AutoBrake auto_brake{ [](const BrakeCommand&){} };
    try {
        auto_brake.set_collision_threshold_s(0.5L);
    } catch (const std::exception&) {
        return;
    }
    assert_that(false, "no exception thrown");
}

/* Requirement: Save the Car's Speed Between Updates */

void speed_is_saved() {
    AutoBrake auto_brake{ [](const BrakeCommand&){} };
    auto_brake.observe(SpeedUpdate{ 100L });
    assert_that(100L == auto_brake.get_speed_mps(), "speed not saved to 100");
    auto_brake.observe(SpeedUpdate{ 50L });
    assert_that(50L == auto_brake.get_speed_mps(), "speed not saved to 50");
    auto_brake.observe(SpeedUpdate{ 0L });
    assert_that(0L == auto_brake.get_speed_mps(), "speed not saved to 0");
}

/* Requirement: AutoBrake Publishes a BrakeCommand When Collision Detected */

// The relevant equations for computing times to collision come directly from hish school physics
// Velocity(relative) = Velocity(our car) - Velocity(other car)
// If your relative velocity is constant and positive, the cars will eventually collide
// Time Collision = Distance / Velocity(relative)

void alert_when_imminent() {
    int brake_commands_published{};
    AutoBrake auto_brake{
        [&brake_commands_published](const BrakeCommand&) {
            brake_commands_published++;
        }
    };

    auto_brake.set_collision_threshold_s(10L);
    auto_brake.observe(SpeedUpdate{ 100L });
    auto_brake.observe(CarDetected{ 100L, 0L });

    assert_that(brake_commands_published == 1, "brake commands published not one");
}

void testing_suite() {
    run_test(initial_speed_is_zero, "initial speed is 0"); // Add speed_mps{} initializer
    run_test(initial_sensitivity_is_five, "initial sensitivity is 5"); // Add collision_threshold_s{} initializer
    run_test(sensitivity_grater_than_1, "sensitivity grater than 1"); // Add checked exception to setter
    run_test(speed_is_saved, "speed is saved"); // Save speed into property
    run_test(alert_when_imminent, "alert when imminent"); // Implement observer for CarDetected
}

/* Adding a Service-Bus Interface */

// The AutoBrake class has a few dependencies: CarDetected, SpeedUpdate, and a generic dependency on some publish object callable with a single BrakeCommand parameter
// The CarDetected and SpeedUpdated classes are plain-old-data types that are easy to use directly in your unit tests
// The publish object is a little more complicated to initialize, but thanks to lambdas, it's really not bad
// Suppose you want to refactor the service bus. You want to accept a std::function to subscribe to each service, asi in the new IServiceBus interface

using SpeedUpdateCallback = std::function<void(const SpeedUpdate&)>;
using CarDetectedCallback = std::function<void(const CarDetected&)>;

struct IServiceBus {
    virtual ~IServiceBus() = default;
    virtual void publish(const BrakeCommand&) = 0;
    virtual void subscribe(SpeedUpdateCallback) = 0;
    virtual void subscribe(CarDetectedCallback) = 0;
};

/* Mocking Dependencies */

struct MockServiceBus : IServiceBus {
    void publish(const BrakeCommand &cmd) override {
        commands_published++;
        last_command = cmd;
    }
    void subscribe(SpeedUpdateCallback callback) override {
        speed_update_callback = callback;
    }
    void subscribe(CarDetectedCallback callback) override {
        car_detected_callback = callback;
    }

    BrakeCommand last_command{};
    int commands_published{};
    SpeedUpdateCallback speed_update_callback{};
    CarDetectedCallback car_detected_callback{};
};

/* Refactoring AutoBrake */

namespace Refactor {
    struct AutoBrake {
        AutoBrake(IServiceBus& bus) : speed_mps{}, collision_threshold_s{ 5 } {
            bus.subscribe([this](const SpeedUpdate& update) {
                speed_mps = update.velocity_mps;
            });

            bus.subscribe([this, &bus](const CarDetected& cd) {
                const auto relative_velocity_mps = speed_mps - cd.velocity_mps;
                const auto time_to_collision_s = cd.distance_m / relative_velocity_mps;

                if (time_to_collision_s > 0 && time_to_collision_s <= collision_threshold_s) {
                    bus.publish(BrakeCommand{ time_to_collision_s });
                }
            });
        }
        void set_collision_threshold_s(double x) {
            if (x < 1) throw std::runtime_error{ "Collision less than 1." };
            collision_threshold_s = x;
        }
        double get_collision_threshold_s() const {
            return collision_threshold_s;
        }
        double get_speed_mps() const {
            return speed_mps;
        }
    private:
        double collision_threshold_s;
        double speed_mps;
    };
};

/* Refactoring the Unit Tests */

namespace Refactor {
    void initial_speed_is_zero() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };
        assert_that(auto_brake.get_speed_mps() == 0L, "speed not equal to 0");
    }

    void initial_sensitivity_is_five() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };
        assert_that(auto_brake.get_collision_threshold_s() == 5, "sensitivity is not 5");
    }

    void sensitivity_greater_than_1() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };
        try {
            auto_brake.set_collision_threshold_s(0.5L);
        } catch (const std::exception&) {
            return;
        }
        assert_that(false, "no exception thrown");
    }

    void speed_is_saved() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };

        bus.speed_update_callback(SpeedUpdate{ 100L });
        assert_that(100L == auto_brake.get_speed_mps(), "speed not saved to 100");
        bus.speed_update_callback(SpeedUpdate{ 50L });
        assert_that(50L == auto_brake.get_speed_mps(), "speed not saved to 50");
        bus.speed_update_callback(SpeedUpdate{ 0L });
        assert_that(0L == auto_brake.get_speed_mps(), "speed not saved to 0");
    }

    void no_alert_when_no_imminent() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };
        auto_brake.set_collision_threshold_s(2L);

        bus.speed_update_callback(SpeedUpdate{ 100L });
        bus.car_detected_callback(CarDetected{ 1000L, 50L });

        assert_that(bus.commands_published == 0, "brake commands were published");
    }

    void alert_when_imminent() {
        MockServiceBus bus{};
        AutoBrake auto_brake{ bus };
        auto_brake.set_collision_threshold_s(10L);

        bus.speed_update_callback(SpeedUpdate{ 100L });
        bus.car_detected_callback(CarDetected{ 100L, 0L });

        assert_that(bus.commands_published == 1, "1 brake commands was not published.");
        assert_that(bus.last_command.time_to_collision_s == 1L, "time to collision not computed correctly.");
    }

    void testing_suite() {
        run_test(initial_speed_is_zero, "initial speed is 0"); // Add speed_mps{} initializer
        run_test(initial_sensitivity_is_five, "initial sensitivity is 5"); // Add collision_threshold_s{} initializer
        run_test(sensitivity_grater_than_1, "sensitivity grater than 1"); // Add checked exception to setter
        run_test(speed_is_saved, "speed is saved"); // Save speed into property
        run_test(no_alert_when_no_imminent, "no alert when no imminent"); // Implement observer for CarDetected
        run_test(alert_when_imminent, "alert when imminent"); // Implement observer for CarDetected
    }
};

int main(int argc, const char * argv[]) {
    Refactor::testing_suite();
    return 0;
}
