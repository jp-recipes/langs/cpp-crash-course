#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <string>
#include <filesystem>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <unordered_set>

using namespace std;

/* Filesystems */

/* Filesystem Concepts */

// A file is a filesystem object that supports input and output and holds data
// Files exist in container called 'directories', which can be nested within other directorios
// For simplicity, directories are considered files
// The directory containing a file is called that file's parent directory

// A path is a string that identifies a specific file
// Paths begin with an optional root name, which is an implementation-specific string

// A hard link is a directory entry that assigns a name to an existing file
// A symbolic link (or symlink) assigns a name to a path (which might or might not exist)

// A path whose location is specified in relation to another path is called a relative path
// A canonical path unambiguously identifies a file's location, doesn't containe the especial names "." and "..", and doesn't contain any symbolic links
// An asbolute path is any path that unambiguously identifies a file's location

// A major difference between a canonical path and an absolute path is that canonical path cannot contain the espcial names "." and ".."

/* std::filesystem::path */

// The std::fielsystem::path is the Filesystem library's class form modeling a path

/* Constructing Paths */

// The path class supprots comparison woth other path objects with string objects using the operator==

TEST_CASE("std::filesystem::path supports == and .empty()") {
    std::__fs::filesystem::path empty_path;
    std::__fs::filesystem::path shadow_path{ "/etc/shadow" };
    REQUIRE(empty_path.empty());
    REQUIRE(shadow_path == std::string{ "/etc/shadow" });
}

/* Decomposing Paths */

// root_name() returns the root name
// root_directory() returns the root directory
// root_path() returns the root path
// relative_path() returns a path relative to the root
// parent_path() returns the parent path
// filename() returns the filename component
// stem() returns the filename stripped of its extension
// extension() return the extension

void using_paths() {
    const __fs::filesystem::path kernel32{ R"(C:\Windows\System32\kernel32.dll)" };
    cout << "Root name: " << kernel32.root_name()
        << "\nRoot direcotry: " << kernel32.root_directory()
        << "\nRoot path: " << kernel32.root_path()
        << "\nRelative path: " << kernel32.relative_path()
        << "\nParent path: " << kernel32.parent_path()
        << "\nFilename: " << kernel32.filename()
        << "\nStem: " << kernel32.stem()
        << "\nExtension: " << kernel32.extension()
        << endl;

    // Root name: "C:"
    // Root directory: "\\"
    // Root path: "C:\\"
    // Relative path: "Windows\\System32\\kernel32.dll"
    // Parent path: "C:\\Windows\\System32"
    // Filename: "kernel32.dll"
    // Stem: "kernel32"
    // Extension: ".dll"
}

/* Modifying Paths */

// In addition to decomposition methods, path offers serveral modifier methods

//   clear()
//     Empties the path
//   make_preferred()
//     Convertes all the directory esperators to the implementation-preferred directory separator
//     For example, on Windows this converts the generic separator / to the system preferred separator \
//   remove_filename()
//     Removes the filename portion of the path
//   replace_filename(p)
//     Replaces the path's filename with that of another path p
//   replace_extension(p)
//     Replaces the path's extension with that of another path p
//   remove_extension()
//     Removes the extension portion of the path

void using_path_modifiers() {
    __fs::filesystem::path path{ R"(C:/Windows/System32/kernel32.dll)" };
    cout << path << endl;

    path.make_preferred();
    cout << path << endl;

    path.replace_filename("win32kfull.sys");
    cout << path << endl;

    path.remove_filename();
    cout << path << endl;

    path.clear();
    cout << "Is empty: " << boolalpha << path.empty() << endl;

    // "C:/Windows/System32/kernel32.dll"
    // "C:\\Windows\\System32\\kernel32.dll"
    // "C:\\Windows\\System32\\win32kfull.sys"
    // "C:\\Windows\\System32\\"
    // Is empty: true
}

/* Summary of Filesystem Path Methods */

/* Error Handling */

// Each function has two overloads: one that allows you to pass a reference to a std::system_error and one that omits this paramater
// If you provide the reference, the function will set the system_error equal to an error condition
// If you don't provide this reference, the function will throw a std::filesystem::filesystem_error

/* Path-Composing Functions */

// As an alternative to using the constructor of path, you can construct various kinds of paths
//
//   absolute(p, [ec])
//     Returns an absolute path referencing the same locations a p but where is_absoulute is true
//   canonical(p, [ec])
//     Returns a canonical path referencing the same location as p
//   current_path([ec])
//     Returns the current path
//   relative(p, [base], [ec])
//     Returns a path where p is made relative to base
//   temp_directory_path([ec])
//     Returns a directory for temporary files
//     The result is guaranteed to be an existing directory

void using_path_composing_functions() {
    const auto temp_path = __fs::filesystem::temp_directory_path();
    const auto relative = __fs::filesystem::relative(temp_path);

    try {
    cout << boolalpha
        << "Temporary directory path: " << temp_path
        << "\nTemporary directory absolute: " << temp_path.is_absolute()
        << "\nCurrent path: " << __fs::filesystem::current_path()
        << "\nTemporary directory's relative path: " << relative
        << "\nRelative directory absolute: " << relative.is_absolute()
        << "\nChanging current directory to temp.";

    __fs::filesystem::current_path(temp_path);
    cout << "\nCurrent directory: " << __fs::filesystem::current_path();
    } catch (const exception& e) {
        cerr << "Error: " << e.what();
    }

    // Temporary directory path: "C:\\Users\\lospi\\AppData\\Local\\Temp\\"
    // Temporary directory absolute: true
    // Current path: "c:\\Users\\lospi\\Desktop"
    // Temporary directory's relative path: "..\\AppData\\Local\\Temp"
    // Relative directory absolute: false
    // Changing current directory to temp.
    // Current directory: "C:\\Users\\lospi\\AppData\\Local\\Temp"
}

/* Inspecting File Types */

// You can inspect a file's attributes given a path by using the following functions

//   is_block_file(p, [ec])
//     Determines if p is a block file, a espcial file in some operating systems
//     For example, block devices in Linux that allow you ti transfer randomly accessible data in fixed-size blocks
//   is_character_file(p, [ec])
//     Determines if p is a character file, a especial file in some operating systems
//     For example, character devices in Linux that allow you to send and receive single characters
//   is_regular_file(p, [ec])
//     Determines if p is a regular file
//   is_symlink(p, [ec])
//     Determines if p is a symlink, which is a reference to another file or directory
//   is_empty(p, [ec])
//     Determines if p is either an empty file or an empty directory
//   is_directory(p, [ec])
//     Determines if p is a directory
//   is_fifo(p, [ec])
//     Determines if p is a named pipe, a special kind of interprocess communication mechanism in many operating systems
//   is_socket(p, [ec])
//     Determines if p is a socket, another special kind of interprocess communication mechanism in many operating systems
//   is_other(p, [ec])
//     Determines if p is some kind of file other than a regular file, a directory, or a symlink

void describe(const __fs::filesystem::path& p) {
    cout << boolalpha << "Path: " << p << endl;
    try {
        cout << "Is directory: " << __fs::filesystem::is_directory(p) << endl;
        cout << "Is regular file: " << __fs::filesystem::is_regular_file(p) << endl;
    } catch (const exception& e) {
        cerr << "Exception: " << e.what() << endl;
    }
}

void using_file_inspection_functions() {
    __fs::filesystem::path win_path{ R"(C:/Windows/System32/kernel32.dll)" };
    describe(win_path);
    win_path.remove_filename();
    describe(win_path);

    __fs::filesystem::path nix_path{ R"(/bin/bash)" };
    describe(nix_path);
    nix_path.remove_filename();
    describe(nix_path);

    // On a Windows 10 x64 machine
    //
    // Path: "C:/Windows/System32/kernel32.dll"
    // Is directory: false
    // Is regular file: true
    // Path: "C:/Windows/System32/"
    // Is directory: true
    // Is regular file: false
    // Path: "/bin/bash"
    // Is directory: false
    // Is regular file: false
    // Path: "/bin/"
    // Is directory: false
    // Is regular file: false

    // On Ubuntu 18.04 x64
    //
    // Path: "C:/Windows/System32/kernel32.dll"
    // Is directory: false
    // Is regular file: false
    // Path: "C:/Windows/System32/"
    // Is directory: false
    // Is regular file: false
    // Path: "/bin/bash"
    // Is directory: false
    // Is regular file: true
    // Path: "/bin/"
    // Is directory: true
    // Is regular file: false
}

/* Inspoecting Files and Directories */

// You can inspect various filesystem attributes using the folowwing functions

//   current_path([p], [ec])
//     which, if p is provided, sets the program's current path to p
//     otherwise, it returns the program's current path
//
//   exists(p, [ec])
//     returns whether a file or directory exists at p
//
//   equivalent(p1, p2, [ec])
//     returns whether p1 and p2 refer to the same file or directory
//
//   file_size(p, [ec])
//     returns the size in bytes of the regular at p
//
//   hard_link_count(p, [ec])
//     returns the number of hard links for p
//
//   last_write_time(p, [t], [ec])
//     which, if t is provided, sets p's last modified time to t
//     otherwise, it returns the last time p was modified (t is a std::chrono::time_point)
//
//   permissions(p, prm, [ec])
//     sets p's permissions
//     prm is of type std::fylesystem::perms, which is an enum class modeled
//       after POSIX permissions bits
//
//   read_symlink(p, prm, [ec])
//     returns the target of the symlink p
//
//   space(p, [ec])
//     returns space information about the filesystem p occupies in the form of a
//       std::filesystem::space_info
//     This class contains a type method that accepts no parameters and returns an
//       object of type std::filesytem::file_type, which is an enum class that takes values
//       describing a file's type, such as not_found, regular, directory
//     The symlink file_status class also offers a permissions method that accepts no
//       paramaters and returns an object of type std::filesystem::perms
//
//   symlink_status(p, [ec])
//     is like a status that won't follow symlinks

/* Manipulating Files and Directories */

// Additionally, the Filesystem library contains a number of methods for manipulating files and directories
//
//   copy(p1, p2, [opt], [ec])
//     copies files or directorios from p1 to p2
//     You can provide a std::filesystem::copy_options `opt` to customize the
//       behabior of copy_file
//     This enum class can take several values, including
//       none (report an error if the destination already exists),
//       skip_existing (to keep existing),
//       overwrite_existing (to overwrite), and
//       update_existing (to overwrite if p1 is newer)
//
//   copy_file(p1, p2, [opt], [ec])
//     is like copy except it will genera an error if p1 is anything but a regular file
//
//   create_directory(p, [ec])
//     creates the directory p
//
//   create_directories(p, [ec])
//     is like calling create_directory recursively, so if a nested path contains
//       parents that don't exist, use this form
//
//   create_hard_link(tgt, lnk, [ec])
//     creates a hard link to tgt at lnk
//
//   create_symlink(tgt, lnk, [ec])
//     creates a symlink to tgt at lnk
//
//   create_directory_symlink(tgt, lnk, [ec])
//     should be used for directories instead of create_symlink
//
//   remove(p, [ec])
//     removes a file or empty directory p (without following symlinks)
//
//   removeAll(p, [ec])
//     removes a file or directory recursively p (without following symlinks)
//
//   rename(p1, p2, [ec])
//     renames p1 to p2
//
//   resize_file(p, new_size, [ec])
//     changes the size of p (if it's a regular file) to `new_size`
//     If this operation grows the file, zeros fill the new space
//     Otherwise, the operation trims p from the end

void using_copies_resizes_deletes() {
    using namespace std::__fs::filesystem;
    using namespace std::chrono;

    auto write_info = [](const path& p){
        if (!std::__fs::filesystem::exists(p)) {
            cout << p << "does not exist." << endl;
        }

        const auto last_write = last_write_time(p).time_since_epoch();
        const auto in_hours = duration_cast<hours>(last_write).count();
        cout << p << "\t" << in_hours << "\t" << file_size(p) << "\n";
    };

    const path win_path{ R"(C:/Windows/System32/kernel32.dll)" };
    const auto readme_path = temp_directory_path() / "README";

    try {
        write_info(win_path);
        write_info(readme_path);

        cout << "Copying" << win_path.filename() << " to " << readme_path.filename() << "\n";
        copy_file(win_path, readme_path);
        write_info(readme_path);

        cout << "Resizing " << readme_path.filename() << "\n";
        resize_file(readme_path, 1024);
        write_info(readme_path);

        cout << "Removing " << readme_path.filename() << "\n";
        remove(readme_path);
        write_info(readme_path);

        // "C:/Windows/System32/kernel32.dll" 3657767 720632
        // "C:\\Users\\lospi\\AppData\\Local\\Temp\\REAMDE" does not exist.
        // Copying "kernel32.dll" to "REAMDE"
        // "C:\\Users\\lospi\\AppData\\Local\\Temp\\REAMDE" 3657767 720632
        // Resizing "REAMDE"
        // "C:\\Users\\lospi\\AppData\\Local\\Temp\\REAMDE" 3659294 1024
        // Removing "REAMDE"
        // "C:\\Users\\lospi\\AppData\\Local\\Temp\\REAMDE" does not exist.
    } catch (const exception& e) {
        cerr << "Exception: " << e.what() << endl;
    }
}

/* Direcotry Iterators */

// The Filesystem library provodes two classes for iterating over the elements of a directory
//   std::filesystem::directory_iterator
//   std::filesystem::revursive_directory_iterator

/* Constructing */

// The default constructor of directory_iterator produces the end iterator
//   (Recall that an input end iterator indicates when an input range is exhausted)
// Another constructor accepts path, which indicates the directory you want to enumerate
// Optionally, you can provide std::filesystem::directory_options, which is an enum class bitmask with the following constants
//
//   none
//     Directs the iterator to skip directory symlinks
//     If the iterator encounters a permission denial, it produces an error
//
//   follow_directory_symlink
//     Follows symlinks
//
//   skip_permission_denied
//     Skips directories if the iterator encounters a permission denial

// Additionally, you can provide a std::error_code, which, like all other Filesystem library functions that accept an error_code, will set this paramter rather than throwing an exception if an error occurs during construction

// Note that p is path and d is directory, op is directory_options, and ec is error_code
//
//   directory_iterator {}
//     Constructs the end iterator
//
//   directory_iterator { p, [op], [ec] }
//     Construct a directory iterator referring to the directory p
//     The argument `op` defaults to `none`
//     If provided, ec reveives error conditions rahter than throwing an exception
//
//   directory_iterator { d }
//     Copies construction/assignment
//     d1 = d2
//
//   directory_iterator { move(d) }
//     Move construction/assignment
//     d1 = move(d2)

/* Directory Entries */

// The input iterators directory_iterator and recursive_directory_iterator produce a
//   std::filesystem::directory element

// A summary of std::filesystem::directory_entry operations
//
//   path()
//     Return the referenced path
//
//   exists()
//     Returns true if the referenced path exists on the filesystem
//
//   is_block_file()
//     Returns true of the referenced path is a block device
//
//   is character_file()
//     Returns true of the referenced path is a character device
//
//   is_directory()
//     Returns true of the referenced path is a directory
//
//   is_fifo()
//     Returns true of the referenced path is a named pipe
//
//   is_regular_file()
//     Returns true of the referenced path is a regular file
//
//   is_socket()
//     Returns true of the referenced path is a socket
//
//   is_symlink()
//     Returns true of the referenced path is a symlink
//
//   is_other()
//     Returns true of the referenced path is something else
//
//   file_size()
//     Returns the size
//
//   hard_link_count()
//     Returns the number of hard links to the referenced path
//
//   last_write_time([t])
//     If `t` is provided, sets the last modified time of the referenced path
//       otherwise, it returns the last modified path
//
//   status()
//     Returns a std::filesystem::file_status for the referenced path
//
//   symlink_status()
//     Returns a std::filesystem::file_status for the referenced path

void using_directory_entry() {
    using namespace std::__fs::filesystem;
    using namespace std::chrono;

    auto describe = [](const directory_entry& entry) {
        try {
            if (entry.is_directory()) {
                cout << "           *";
            } else {
                cout << setw(12) << entry.file_size();
            }

            const auto lw_time = duration_cast<seconds>(
                entry.last_write_time().time_since_epoch()
            );

            cout << setw(12) << lw_time.count()
                << " " << entry.path().filename().string() << "\n";
        } catch (const exception& e) {
            cout << "Error accessing " << entry.path().string() << ": " << e.what() << endl;
        }
    };

    const char* argv[2] = { "/tmp", "/bin" };
    const path sys_path{ argv[1] };
    cout << "Size         Last Write  Name\n";
    cout << "------------ ----------- ------------\n";
    for (const auto& entry : directory_iterator{ sys_path })
        describe(entry);

    // > listdir c:\Windows
    //           Size         Last Write  Name
    //           ------------ ----------- ------------
    //                      * 13177963504 addins
    //                      * 13171360979 appcompat
    // --snip--
    //                      * 13173551028 WinSxS
    //                 316640 13167963236 WMSysPr9.prx
    //                  11264 13167963259 write.exe
}

/* Recursive Directory Iteration */

// The recursive_directory_iterator is a drop-in replacement for directory_iterator in the sense that it supports all the same operations but will enumerate subdirectories

struct Attributes {
    Attributes& operator+=(const Attributes& other) {
        this->size_byte += other.size_byte;
        this->n_directories += other.n_directories;
        this->n_files += other.n_files;
        return *this;
    }
    size_t size_byte;
    size_t n_directories;
    size_t n_files;
};

void print_line(const Attributes& attributes, string_view path) {
    cout << setw(14) << attributes.size_byte
         << setw(7) << attributes.n_files
         << setw(7) << attributes.n_directories
         << " " << path << "\n";
}

Attributes explore(const std::__fs::filesystem::directory_entry& directory) {
    Attributes attributes{};

    for (const auto& entry : std::__fs::filesystem::recursive_directory_iterator{ directory.path() }) {
        if (entry.is_directory()) {
            attributes.n_directories++;
        } else {
            attributes.n_files++;
            attributes.size_byte += entry.file_size();
        }
    }

    return attributes;
}

void using_recursive_directory_iterator() {
    const char* argv[] = { "/tmp", "/bin" };

    const std::__fs::filesystem::path sys_path{ argv[1] };

    cout << "Size           Files  Dirs   Name\n";
    cout << "-------------- ------ ------ x------------\n";
    Attributes root_attributes{};
    for (const auto& entry : std::__fs::filesystem::directory_iterator{ sys_path }) {
        try {
            if (entry.is_directory()) {
                const auto attributes = explore(entry);
                root_attributes += attributes;
                print_line(attributes, entry.path().string());
                root_attributes.n_directories++;
            } else {
                root_attributes.n_files++;
                error_code ec;
                root_attributes.size_byte += entry.file_size(ec);
                if (ec)
                    cerr << "Error reading file size: "
                         << entry.path().string() << endl;
            }
        } catch (const exception& e) {
            // meh
        }
    }
    print_line(root_attributes, argv[1]);

    // > treedir C:\Windows
    // Size         Files  Dirs  Name
    // ------------ ------ ----- ------------
    //          802      1     0 C:\Windows\addins
    //      8267330      9     5 C:\Windows\apppatch
    // --snip--
    //  11396916465  73383 20480 C:\Windows\WinSxS
    //  21038460348 110950 26513 C:\Windows
}

/* fstream Interoperation */

// You can construct file streams (basic_ifstream, basic_ofstream, or basic_fstream) using std::filesystem::path or std::filesystem::directory_entry

void using_fstream() {
    const char* argv[] = { "/tmp", "/bin" };

    const unordered_set<string> pe_extension {
        ".acm", ".ax", ".cpl", ".dll", ".drv",
        ".efi", ".exe", ".mui", ".ocx", ".scr",
        ".sys", ".tsp"
    };

    const std::__fs::filesystem::path sys_path{ argv[1] };
    cout << "Searching " << sys_path << " recursively.\n";

    size_t n_searched{};
    auto iterator = std::__fs::filesystem::recursive_directory_iterator{
        sys_path, std::__fs::filesystem::directory_options::skip_permission_denied
    };

    for (const auto& entry : iterator) {
        try {
            if (!entry.is_regular_file())
                continue;
            const auto& extension = entry.path().extension().string();
            const auto is_pe = pe_extension.find(extension) != pe_extension.end();
            if (!is_pe)
                continue;

            ifstream file{ entry.path() };
            char first{}, second{};
            if (file) file >> first;
            if (file) file >> second;
            if (first != 'M' || second != 'Z')
                cout << "Invalid PE found: " << entry.path().string() << "\n";
            ++n_searched;
        } catch (const exception& e) {
            cerr << "Error reading " << entry.path().string() << ": " << e.what() << endl;
        }
    }
}
