#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <limits>

using namespace std;

/* Streams */

// A stream models a 'stream of data'
// In a stream, data flows between objects, and those objects can perform arbitrary processing on the data
// When you're working with streams, output is data going into the stream and input is data coming out of the stream
// These terms reflect the sreams as viewed from the user's perspective

// In C++, streams are the primary mechanism for performing input and output (I/O)
// Regardless of the source or destination, you can use streams as the common language to connect input to outputs

// The STL uses class inheritance to encode the relationship between various stream types
// The primary types in this hierarchy are:
//
//   The std::basic_ostream class template in the <ostream> header
//     It represents an output device
//   The std::basic_istream class template in the <istream> header
//     It represents an input device
//   The std::basic_iostream class template in the <iostream> header
//     It representas devices that are input and output

/* Stream Classes */

// All STL stream classes that users interact with derive from basic_istream, basic_ostream, or both view basic_iostream
// The headers that declare each type aslo provide char and wchar_t specializations for those templates

// Template Specializations for the Primary Stream Templates
//
// Template         Parameter     Specialization     Header
//
// basic_istream    char          istream            <istream>
// basic_ostream    char          ostream            <ostream>
// basic_iostream   char          iostream           <iostream>
// basic_istream    wchar_t       wistream           <istream>
// basic_ostream    wchar_t       wostream           <ostream>
// basic_iostream   wchar_t       wiostream          <iostream>

/* Global Stream Objects */

// The STL provides several global stream objects in the <iostream> header that wrap the input, output, and error streams stdin, stdout, and stderr
// These implementation-defined standard streams are preconnected channels between your program and its executing environment

// The Global Stream Objects (stdin, stdout, and stderr)
//
// Object         Type                  Purpose
// cout, wcout    ostream, wostream     Output, like a streen
// cin, wcin      istream, wistream     Input, like a keyboard
// cerr, wcerr    ostream, wostream     Error output (unbuffered)
// clog, wclog    ostream, wostream     Error output (buffered)

// Stream classes support operations that you can partition into two categories
//
// Formatted operations
//   Might perform some preprocessing on their input parameters before performing I/O
//
// Unformatted operations
//   Perform I/O directly

/* Formatted Operations */

// All formatted I/O passes through two functions:
//   The standard stream operators, operator<< and operator>>


// This example illustrates how to use the output operator to write various types into cout
void using_ostreams() {
    bitset<8> s{ "01110011" };
    string str{ "Crying zeros and I'm hearing " };
    size_t num{ 111 };
    cout << s;
    cout << '\n';
    cout << str;
    cout << num;
    cout << "s\n";

    // 01110011
    // Crying zeros and I'm hearing 111
}

// One very nice feature of the standard stream operators is that they generally return a reference to the stream

//   ostream& operator<<(ostream&, char);

void using_chained_ostream() {
    bitset<8> s{ "01110011" };
    string str{ "Crying zeros and I'm hearing " };
    size_t num{ 111 };
    cout << s << '\n' << str  << num << "s\n";
}

// Input streams overload operator>>, which is referred to as the input operator or the extractor

void using_istream_and_ostream() {
    double x, y;
    cout << "X: ";
    cin >> x;
    cout << "Y: ";
    cin >> y;

    string op;
    cout << "Operation: ";
    cin >> op;
    if (op == "+") {
        cout << x + y;
    } else if (op == "-") {
        cout << x - y;
    } else if (op == "*") {
        cout << x * y;
    } else if (op == "/") {
        cout << x / y;
    } else {
        cout << "Unknown operation" << op;
    }
}

/* Unformatted Operations */

// When you're working with text-based streams, you'll usually want to use formatted operators; however, if you're working with binary data or if you're writing code that needs low-level access to streams, you'll want to know about the unformatted operations

// The istream class has many unformatted input methods
// These methods manipulate streams at the byte level and are summarized next

/* Special Formatting for Fundamental Types */

// All fundamental types, in addition to void and nullptr, have input and output operator overloads, but some have special rules

// char and wchar_t
//   The input operator skips whitespace when assigning character types
// char* and wchar_t*
//   The input operator first skips whitespace and then reads the string until it encounters another whitespace or an end-of-life
//   You must reserve enough space for the input
// void*
//   Address formats are implementation dependent for input and output operators
// bool
//   The input and output operators treat Boolean values as numbers
//   1 for true and 0 for sale
// Numeric types
//   The input operator requires that input begin with at least one digit
//   Badly formed input numbers yield a zero-valued result

// Note
// Avoid reading into C-style strings, because it's up tu you to ensure that you've allocated enoguh space for the input data
// Failure to performa adequate checking results in undefined behavior and possibly major security vulnerabilities.
// Use std::string instead

/* Stream State */

// A stream's state indicates whether I/O failed
// Each stream type exposes the constant static members referred to collectively as its bits,

void counting_stream_words() {
    std::string word;
    size_t count{};
    while (std::cin >> word)
        count++;
    std::cout << "Discovered " << count << " words.\n";
}

void checking_stream_state() {
    cin.exceptions(istream::badbit);
    string word;
    size_t count{};
    try {
        while (cin >> word)
            count++;
        cout << "Discovered " << count << " words.\n";
    } catch (const exception& e) {
        cerr << "Error occurred reading from stdin: " << e.what();
    }
}

/* Buffering and Flushing */

// Many ostream class template involve operating system calls under the hood, for example, to write to a console, a file, or a network socket
// Relative to other function calls, system calls are usually slow
// Rather than invoking a system for each output element, an application can wait for multiple elements and then send them all together ti improve performance

// The queuing behavior is called buffering
// When the stream empties the buffered output it's called flushing
// Usually, this behavior is completely transparent to the user, but sometimes you want to manually flush the ostream
// For this (and other tasks), you turn to manipulators

/* Manipulators */

// Manipulators are special objectos that modify how streams interpret input or format output
// Manipulators exist to perform many kinds of stream alterations
// For example, std::ws modifies an istream to skip over whitespace
// Here are some other manipulators that work on ostream
//
//   std::flush empties any buffered output directly to an ostream
//   std::ends send a null byte
//   std::endl is like std::flush except it sends a newline before flushing

//   cout << "Discovered " << cout << " words." << endl;

// Note
// As a general rule, use std::endl when your program has finished outputting text to the stream for a while and \n when you know your program will output more text soon

// The stdlib provides many other manipulators in the <ios> header

// Manipulators apply to all subsequent objects you insert into a stream

void using_manipulators() {
    cout << "Gotham needs its " << boolalpha << true << " hero.";
    cout << "\nMark it " << noboolalpha << false << "!";
    cout << "\nThere are " << 69 << "," << oct << 105 << " leaves in here.";
    cout << "\nYabba " << hex << 3669732608 << "!";
    cout << "\nAvogadro's number: " << scientific << 6.0221415e-23;
    cout << "\nAlways eliminate " << 3735929054;
    cout << setw(4) << "\n"
         << 0x1 << "\n"
         << 0x10 << "\n"
         << 0x100 << "\n"
         << 0x1000 << "\n";
}

/* User-Defined Types */

// You can make user-defined types work woth streams by implementing certian non-member functions

//   ostream& operator<<(ostream& s, const YourType& m);

template <typename T>
ostream& operator<<(ostream& s, vector<T> v) {
    s << "Size: " << v.size()
      << "\nCapacity: " << v.capacity()
      << "\nElements:\n";
    for (const auto& element : v)
        s << "\t" << element << "\n";
    return s;
}

void using_user_defined_ostream_operator() {
    const vector<string> characters {
        "Bobby Shaftoe",
        "Lawrence Waterhouse",
        "Gunter Bischoff",
        "Earl Comstock"
    };
    cout << characters << endl;

    const vector<bool> bits { true, false, true, false };
    cout << boolalpha << bits << endl;
}

template <typename T>
istream& operator>>(istream& s, deque<T>& t) {
    T element;
    while (s >> element)
        t.emplace_back(move(element));
    return s;
}

void using_user_defined_istream_operator() {
    cout << "Give me numbers: ";
    deque<int> numbers;
    cin >> numbers;
    int sum{};
    cout << "Cumulative sum:\n";
    for (const auto& element : numbers) {
        sum += element;
        cout << sum << "\n";
    }
}

// Note
// The preceding examples are simple user-defined implementations of input and output operators
// You might want to elaborate these implementations in production code
// For example, the implementations only work with ostream classes, which implies that won't work with any non-char sequences

/* String Streams */

// The string stream classes provide facilities for reading from and writing to character sequences
// These classes are useful in serverl situations
// Input strings are especial useful if you want to parse string data into types
// Output strings are excellent for building up strings from variable-length input

/* Output String Streams */

// Output string strems provide output-stream semantics for character sequences, and they all derive from the class template std::basic_ostringstream in the <sstream> header, which provides the following specializations
//
//   using ostringstream = basic_ostringstream<char>;
//   using wostringstream = basic_ostringstream<wchar_t>;

TEST_CASE("ostringstream produces string with str") {
    std::ostringstream ss;
    ss << "By Grabthar's hammer, ";
    ss << "by the suns of Worvan. ";
    ss << "you shall be avenged.";
    const auto lazarus = ss.str();

    ss.str("I am Groot.");
    const auto groot = ss.str();

    REQUIRE(lazarus == "By Grabthar's hammer, by the suns of Worvan. you shall be avenged.");
    REQUIRE(groot == "I am Groot.");
}

/* Input String Streams */

// Input string streams provide input stream semantics for character sequences, and they all derive from the class template std::basic_istringstream inthe <sstream> header, which provides the following specializations
//
//   using istringstream = basic_istringstream<char>;
//   using wistringstream = basic_istringstream<wchar_t>;

TEST_CASE("istringstream supports construction from a string") {
    std::string numbers("1 2.23606 2");
    std::istringstream ss{ numbers };

    int a;
    float b, c, d;

    ss >> a;
    ss >> b;
    ss >> c;

    REQUIRE(a == 1);
    REQUIRE(b == Approx(2.23606));
    REQUIRE(c == Approx(2));
    REQUIRE_FALSE(ss >> d);
}

/* String Stream Supporting Input and Output */

// Additionally, if you want a string stream that supports input and output operations, you can use the basic_stringstream, which has the following specializations
//
//   using stringstream = basic_stringstream<char>;
//   using wstringstream = basic_stringstream<wchar_t>;

TEST_CASE("stringstream supports all string stream operations") {
    std::stringstream ss;
    ss << "Zed's DEAD";

    std::string who;
    ss >> who;
    int what;
    ss >> std::hex >> what;

    REQUIRE(who == "Zed's");
    REQUIRE(what == 0xdead);
}

// Note
// All string stream are moveable

/* File Streams */

// The file stream classes provide facilities for reading from and writing to character sequences
// File stream classes provide the following major benefits over using native system calls to interact with file contentsw
//
//   You get the usual stream interfaces, which provide a rich set of features for formatting and manipulating output
//   The file stream classes are RAII wrappers around the files, meaning it's impossible to leak resources, such as files
//   File stream classes support move semantics, so you can have tight control over where files are in scope

/* Opening Files with Streams */

// You have two options for opening a file with any file stream
//
// The first option is the open method, which accepts a const char* filename and an optional std::ios_base::openmode bitmask argument
// The openmode argument can be one of the many possible combinations of values
//
// Flag                 File                        Meaning
//
// in                   Must exist                  Read
// out                  Created if doesn't exist    Erase the file; then write
// app                  Created if doesn't exist    Append
// in|out               Must exist                  Read and write from beginning
// in|app               Created if doesn't exist    Update at end
// out|app              Created if doesn't exist    Append
// out|trunc            Created if doesn't exist    Erase the file; then read and write
// in|out|app           Created if doesn't exist    Update at end
// in|out|trunc         Created if doesn't exist    Erase the file; then read and write

// Additionaly, you can add the binary flag to any of these combinations to put the file in binary mode
// In binary mode, the stream won't convert special character sequences, like end of file

// To check whether a file is open, invoke `is_open` method

/* Output File Streams */

// Output file streams provide output stream semantics for character sequences, and they all derive from the class template std::basic_ofstream in the <fstream> header, which provides the following specializations
//
//   using ofstream = basic_ofstream<char>;
//   using wofstream = basic_ofstream<wchar_t>;

// Whenever you send input to a file stream, the stream writes the data to the corresponding file

void unsing_ofstream_to_open_a_file_and_appending_a_message_to_it() {
    ofstream file{ "launchtime.txt", ios::out|ios::app };
    file << "Time is an illusion." << endl;
    file << "lunch time, " << 2 << "x so." << endl;

    // launchtime.txt:
    // Time is an illusion.
    // Lunch time, 2x so.
}

/* Input File Streams */

// Input file streams provide input stream semantics for character sequence, and they all derive from the class template std::basic_ifstream in the <fstream> header, which provides the following specializations
//
//   using ifstream = basic_ifstream<char>;
//   using wifstream = basic_ifstream<wchar_t>;

void using_ifstream() {
    ifstream file { "numbers.txt" };
    auto maximum = numeric_limits<int>::min();
    int value;
    while (file >> value)
        maximum = maximum < value ? value : maximum;
    cout << "Maximum found was " << maximum << endl;
}

/* Handling Failure */

// As with other streams, file streams fail silently
// If you use a file stream cosntructor to open a file, you must check the is_open method to determine whether the stream successfully opened the file
// This design differs from most other stdlib objects where invariants are enforced by exceptions
// The fact is that you can opt into an exception-based approach fairly easily

ifstream open(const char* path, ios_base::openmode mode = ios_base::in) {
    ifstream file{ path, mode };
    if (!file.is_open()) {
        string err{ "Unable to open file" };
        err.append(path);
        throw runtime_error{ err };
    }
    file.exceptions(ifstream::badbit);
    return file;
}

/* Stream Buffers */

// Streams don't read and write directly
// Under the covers, they use stream bufffer classes
// At a high level, stream buffer classes are templates that send or extract characters

/* Writing Files to sdout */

// Sometimes you just want to write the contents of an input file stream directly into an output stream
// To do this, you can extract the stream buffer pointer from the file stream and pass ot to the output operator
// For example, you can dump the contents of a file to stdout using cout in the following way
//
//   cout << my_ifstream.rdbug()

/* Output Stream Buffer Iterators */

// Output stream buffer iterators are template classes that expose an output iterator interfaces that translates writes into output operations on the underlying stream buffer

void using_output_stream_buffer_iterator() {
    ostreambuf_iterator<char> itr{ cout };
    *itr = 'H';
    ++itr;
    *itr = 'i';
}

/* Input Stream Buffer Iterators */

// Input stream buffer iterators are template classes that expose an input iterator interface that translates reads into read operations on the underlying stream buffer
// These are entirely analogous to output stream buffer iterators

void using_input_stream_buffer_operator() {
    istreambuf_iterator<char> cin_itr{ cin.rdbuf() }, end{};
    cout << "What is your name? ";
    const string name{ cin_itr, end };
    cout << "\nGoodby, " << name;

    // What is your name? <TYPE> josh
    // Goodbye, josh
}

/* Random Access */

// Sometimes you'll want random access into a stream (especially a file stream)
// The input and output operators clearly don't support this use case, so basic_istream and basic_ostream offer separate methods for random access
// These methods keep track of the cursor or position, the index of the stream's current character
// The position indicates the next byte that in input stream will read or an output stream will write

// For input streams,you can use the two methods `tellg` and `seekg`
// The tellg method takes no arguments and returns the position
// The seekg method allows you to set the cursor position, and it has two overloads
// Your first option is to provide a `pos_type` position argument, which sets the read position
// The second is to provide an off_type offset argument plus an ios_base::seekdir direction argument
// The pos_type and off_type are determined by the template argument to the basic_stream or basic_ostream, but usually these convert to/from integer types
//
//   ios_base::beg
//     Specifies that the position argument is relative to the beginning
//   ios_base::cur
//     Specifies that the position argument is relative to the current position
//   ios_base::end
//     Specifies that the position argument is relative to the end

// For output streams, you can use the two methods `tellp` and `seekp`

// introspection.txt: The problem with introspection is that it has no end.
void using_random_access() {
    try {
        auto intro = open("introspection.txt");
        cout << "Contents: " << intro.rdbuf() << endl;
        intro.seekg(0);
        cout << "Contents after seekg(0):" << intro.rdbuf() << endl;
        intro.seekg(-4, ios_base::end);
        cout << "tellg() after seekg(-4, ios_base::end):" << intro.tellg() << endl;
        cout << "Contents after seekg(-4, ios_base::end)" << intro.rdbuf() << endl;

        // Contents: The problem with introspection is that it has no end.
        // Contents after seekg(0): The problem with introspection is that it has no end.
        // tellg() after seekg(-4, ios_base::end): 49
        // Contents after seekg(-4, ios_base::end): end.
    } catch (const exception& e){
        cerr << e.what();
    }
}
