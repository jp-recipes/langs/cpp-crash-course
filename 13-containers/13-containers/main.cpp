#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <array>
#include <vector>
#include <utility>
#include <deque>
#include <list>
#include <queue>
#include <bitset>
#include <functional>
#include <unordered_set>
#include <map>

/* Containers */

// The standard template library (STL) is the portion of the stdlib that provides containers and the alrgorithms to manipulate them, with iterators serving as the interface between the two

// Sequence containers store elements consecutively, as in an array
// Associative containers store sorted elements
// Unordered associative containers store hashed objects


/* Sequence Containers */

/* Arrays */

// The STL provides std::array in the <array> header
// An array is a sequential container that holds a fixed-size, contiguous series of elements

// You should use array instead of built-in arrays in virtually all situations
// It supports almost all the same usage patterns as operator[] to access elements, so there aren't manu situations in which you'll need a built.in array

// Constructing

// The array<T, S> class template takes two template parameters
// The contained type T
// The dixed size of the array S

std::array<int, 10> static_array;

TEST_CASE("std::array") {
    REQUIRE(static_array[0] == 0);

    SECTION("uninitialized without braced initializers") {
        std::array<int, 10> local_array;
        REQUIRE(local_array[0] == 0);
    }

    SECTION("initialized with braced initializers") {
        std::array<int, 10> local_array{ 1, 1, 2, 3 };
        REQUIRE(local_array[0] == 1);
        REQUIRE(local_array[1] == 1);
        REQUIRE(local_array[2] == 2);
        REQUIRE(local_array[3] == 3);
        REQUIRE(local_array[4] == 0);
    }
}

// Element access

// The operator[] and `at` methods take a single size_t argument
// <if the index argument is out of bounds, `at`will throw a std::out_of_range exception, whereas operator[] will cause undefined behavior
// The function template `get` takes a template paramter of the same specification

TEST_CASE("std::array access") {
    std::array<int, 4> fib{ 1, 1, 0, 3 };

    SECTION("operator[] can get and set elements") {
        fib[2] = 2;
        REQUIRE(fib[2] == 2);
    }

    SECTION("at() can get and set elements") {
        fib.at(2) = 2;
        REQUIRE(fib.at(2) == 2);
        REQUIRE_THROWS_AS(fib.at(4), std::out_of_range);
    }

    SECTION("get can get and set elements") {
        std::get<2>(fib) = 2;
        REQUIRE(std::get<2>(fib) == 2);
    }
}

// You've also have a front and a back method, which return references to the first and last elements of the array
// You'll get undefined behavior if you call one of these methods if the array has zero length

TEST_CASE("std::array has convenience methods") {
    std::array<int, 4> fib{ 0, 1, 2, 0 };

    SECTION("front") {
        fib.front() = 1;
        REQUIRE(fib.front() == 1);
        REQUIRE(fib.front() == fib[0]);
    }

    SECTION("back") {
        fib.back() = 3;
        REQUIRE(fib.back() == 3);
        REQUIRE(fib.back() == fib[3]);
    }
}

/* Storage Model */

// An array doesn't make allocations, like a built-in array, it contains all of its elements
// This means copies will generally be expensive, because each constitutent element needs to be copied. Moves can be expensive, depending on the whether the underlying type of the array also has move construction and move assignment, which are relatively inexpensive

// Each array is just a built-in array underneath
// In fact, you can extract a pointer to the first element of an array using four distinct methods

// - Te go-to method is to use the `data` method. As advertised, this returns a pointer to the first element
// - The other three method involve using the address-of operator on the first element, which you can obtain using the operator[], at, and front

// You should use data. If the array is empty, the address-of-based approaches will return undefined behavior

TEST_CASE("We can obtain a pointer to the first element using") {
    std::array<char, 9> color{ 'o', 'c', 't', 'a', 'r', 'i', 'n', 'e' };
    const auto* color_ptr = color.data();

    SECTION("data") {
        REQUIRE(*color_ptr == 'o');
    }
    SECTION("address-of front") {
        REQUIRE(&color.front() == color_ptr);
    }
    SECTION("address-of at(0)") {
        REQUIRE(&color.at(0) == color_ptr);
    }
    SECTION("address-of [0]") {
        REQUIRE(&color[0] == color_ptr);
    }
}

/* A Crash Course in Iterators */

// The interface between containers and algorithms is the iterator
// An iterator is a type that knows the internal structure of a container and exposes  simple, pointer-like operations to a container's element

// Iterators come in various flavors, but they all support at least the following operations

// 1. Get the current element (operator*)
// 2. Go to the next element (operator++)
// 3. Assign an iterator equal to another iterator (operator=)

// You can extract iterators from all STL containers (including array) using `begin` and `end` methods
// The begin method returns an iterator pointing to the first element, and the end method returns a pointer to one element past the last element

TEST_CASE("std::array begin/end from a half-open range") {
    std::array<int, 0> e{};
    REQUIRE(e.begin() == e.end());
}

// Use iterators to perform pointer-like operations over a non-empty array

TEST_CASE("std::array iterators are pointer-like") {
    std::array<int, 3> easy_as{ 1, 2, 3 };
    auto iter = easy_as.begin();
    REQUIRE(*iter == 1);
    ++iter;
    REQUIRE(*iter == 2);
    ++iter;
    REQUIRE(*iter == 3);
    ++iter;
    REQUIRE(iter == easy_as.end());
}

// You can build your own types for use in range expression by exposing a begin and an end method
// Containers already do all this work for you, meaning you can use any STL container as a range expression

TEST_CASE("std::array can be used as a range expression") {
    std::array<int, 5> fib{ 1, 1, 2, 3, 5 };
    int sum{};
    for (const auto element : fib)
        sum += element;

    REQUIRE(sum == 12);
}

/* Vectors */

// std::vector is a sequential container that holds a dynamically sized, contiguous series of elements

// Constructingpcfac
// The class tamplate std::vector<T, Allocator> takes two templates paramters

// A vector supports a user-defined allocators because vectors need to allocate dynamic memory
// You can default construct an empty vector so you can fill it with a variable number of elements

TEST_CASE("std::vector supports default construction") {
    std::vector<const char*> vec;
    REQUIRE(vec.empty());
}


// You can use brace initialization with a vector

TEST_CASE("std::vector supports braced initialization") {
    std::vector<int> fib{ 1, 1, 2, 3, 5 };
    REQUIRE(fib[4] == 5);
}

// The initilizer list/braced initialization general rules for STL containers

TEST_CASE("std::vector supports") {
    SECTION("braced initialization") {
        std::vector<int> five_nine{ 5, 9 };
        REQUIRE(five_nine[0] == 5);
        REQUIRE(five_nine[1] == 9);
    }

    SECTION("fill constructor") {
        std::vector<int> five_nines(5, 9);
        REQUIRE(five_nines[0] == 9);
        REQUIRE(five_nines[4] == 9);
    }
}

// You can also construct vectors from a half-open range by passing in the begin and end iterators of the range you want to copy

TEST_CASE("st::vector supports construction from iterators") {
    std::array<int, 5> fib_arr{ 1, 1, 2, 3, 5 };
    std::vector<int> fib_vec(fib_arr.begin(), fib_arr.end());
    REQUIRE(fib_vec[4] == 5);
    REQUIRE(fib_vec.size() == fib_arr.size());
}

/* Move and Copy Semantics */

// With vectors, you have full, copy/move construction/assignment support
// Any vector copy oprtation is potentially very expensive, because these are element-wise or deep copies
// Move operations, on the other hand, are usually very fast, because the contained elements reside in dynamic memory and the moved-from vector can simply pass ownership to the moved-into vector; there's no need to move the contained elements

/* Adding Elements */

// If you want to replace all the elements in a vector, you can use the assign method, which takes an initialization list and replaces all existing elements

TEST_CASE("std::vector assign replaces existing elements") {
    std::vector<int> message{ 13, 80, 110, 114, 102, 110, 101 };
    REQUIRE(message.size() == 7);
    message.assign({ 67, 97, 101, 115, 97, 114 });
    REQUIRE(message[5] == 114);
    REQUIRE(message.size() == 6);
}

TEST_CASE("std::vector insert places new elements") {
    std::vector<int> zeros(3, 0);
    auto third_element = zeros.begin() + 2;
    zeros.insert(third_element, 10);
    REQUIRE(zeros[2] == 10);
    REQUIRE(zeros.size() == 4);
}

// To insert an elemtn to the end of a vector, you use the `push_back` method
// Unlike insert, push_back doesn't require an iterator argument
// You simply provide the element to copy into the vector

TEST_CASE("std::vector push_back places new elements") {
    std::vector<int> zeros(3, 0);
    zeros.push_back(10);
    REQUIRE(zeros[3] == 10);
}

// You can construct new elements in place using the `emplace` and `emplace_back` methods
// The emplace method is a variadic template that, like insert, accepts an iterator as its first argument
// The remaining arguments get forwaded to the appropriate constructor

TEST_CASE("std::vector emplace methods forward arguments") {
    std::vector<std::pair<int, int>> factors;
    factors.emplace_back(2, 30);
    factors.emplace_back(3, 20);
    factors.emplace_back(4, 15);
    factors.emplace(factors.begin(), 1, 60);
    REQUIRE(factors[0].first == 1);
    REQUIRE(factors[0].second == 60);
}

/* Storage Model */

// A vector has dynamic size, so it must be able to resize
// The allocator of a vector manages dynamic mempory underpinning the vector
// Because allocations are expensive, a vector will request more memory than it needs to contain the current number of elements

TEST_CASE("std::vector exposes size management methods") {
    std::vector<std::array<uint8_t, 1024>> kb_store;
    REQUIRE(kb_store.max_size() > 0);
    REQUIRE(kb_store.empty());

    size_t elements{ 1024 };
    kb_store.reserve(elements);
    REQUIRE(kb_store.empty());
    REQUIRE(kb_store.capacity() == elements);

    kb_store.emplace_back();
    kb_store.emplace_back();
    kb_store.emplace_back();
    REQUIRE(kb_store.size() == 3);

    kb_store.shrink_to_fit();
    REQUIRE(kb_store.capacity() >= 3);

    kb_store.clear();
    REQUIRE(kb_store.empty());
    REQUIRE(kb_store.capacity() >= 3);
}


/* Niche Sequential Containers */

// If you know the number of elements you'll need ahead of time, use an array
// If you don't, use a vector

/* Deque */

// A deque is a sequential container with fast insert and remove operations from the front and back

// A vector and deque have very similar interfaces, but internally their storage models are totally different
// A vectir guarantees that all elements are sequential in memory, whereas a deque's memory is usually scattered about

// Constructing and accessing members are identical operations for vectors and deques

// Because the interanl structure of deque is complex, it doesn't expose a `data` method

// The vector methods not implemented by deque
//
//   capacity, reserve
//     Because internal structure is complicated,
//     it might not be efficient to compute capacity
//     Also, deque allocations are relatively fast because a deque desn't relocate
//     elements, so reserving memory ahead of time is unnecessary
//
//   data
//     The elements of deque are not contiguous

TEST_CASE("std::deque supports front insertion") {
    std::deque<char> deckard;
    deckard.push_front('a');
    deckard.push_back('i');
    deckard.push_front('c');
    deckard.push_back('n');
    REQUIRE(deckard[0] == 'c');
    REQUIRE(deckard[1] == 'a');
    REQUIRE(deckard[2] == 'i');
    REQUIRE(deckard[3] == 'n');
}

/* List */

// A list is a sequence container with fast insert/remove operations everywhere but with no random element access

// The list is implemented as a doubly linked list, a data structure composed of nodes
// Each node contains an element, a forward link (flink), and a backward link (blink)
// This is completely different from vector, which stores elements in contiguous memory
// As a result, you cannot use operator[] or at to access arbitrary elements in a list, because such operations would be very inefficient

// All the vector methods not implemented by list
//   capacity, reserve, shrink_to_fit
//     Because lists acquires memory incrementally, it doesn't require periodic resizing
//   operator[], at
//     Random elements access is prohibitively expensive on list
//   data
//     Unneeded because lists elements are not contiguous

TEST_CASE("std::list supports front insertion") {
    std::list<int> odds{ 11, 22, 33, 44, 55 };
    odds.remove_if([](int x) { return x % 2 == 0; }); // lambda predicate
    auto odds_iter = odds.begin();
    REQUIRE(*odds_iter == 11);
    ++odds_iter;
    REQUIRE(*odds_iter == 33);
    ++odds_iter;
    REQUIRE(*odds_iter == 55);
    ++odds_iter;
    REQUIRE(odds_iter == odds.end());
}

/* Stack */

// The STL provides three `container adapters` that encapsulate other STL containers and expose special interfaces for tailored situations
// The adapters are stack, the queue, and the priority queue

// The stack is a data structure with two fundamental operations: push and pop
// When you push an element onto a stack, you insert the element onto the stack's end
// When you pop an element off a stack, you remove the element from the stack's end
// This arrangement is called last-in, first-out

// The STL offers the std::stack in the <stack> header
// The class template stack takes two template paramters
// The first is the underlying type of the wrapped container, and the second is the type of the wrapped container, such as deque or vector

TEST_CASE("std::stack supports push/pop/top operations") {
    std::vector<int> vec{ 1, 3 };
    std::stack<int, decltype(vec)> easy_as(vec); // stack<T, [ctr_type<T>]>([ctr])

    REQUIRE(easy_as.top() == 3);
    easy_as.pop();
    easy_as.push(2);
    REQUIRE(easy_as.top() == 2);
    easy_as.pop();
    REQUIRE(easy_as.top() == 1);
    easy_as.pop();
    REQUIRE(easy_as.empty());
}

/* Queues */

// A queue is a data structure that, like a stack, has push and pop as its fundamental operations
// Unlike a stack, a queue is a first-in, first-out
// When you push an element into a queue, you insert onto the queue's end
// When yoy pop an element off the queue, you remove from the queue's begining
// This way, the element that has been in the queue the longest is the one to get popped off

// Like stack, queue takes two template parameters
// The first paramter is the underlying type of the wrapped container, and the optional second paramter is the type of the wrapped container, which also defaults to *deque*

// You can access the element at the front or back of a queue using the front and back methods

TEST_CASE("std::supports push/pop/front/back") {
    std::deque<int> deq{ 1, 2 };
    std::queue<int> easy_as(deq); // queue<T, [ctr_type<T>]>([ctr]) // ctr == container

    REQUIRE(easy_as.front() == 1);
    REQUIRE(easy_as.back() == 2);
    easy_as.pop();
    easy_as.push(3);
    REQUIRE(easy_as.front() == 2);
    REQUIRE(easy_as.back() == 3);
    easy_as.pop();
    REQUIRE(easy_as.front() == 3);
    easy_as.pop();
    REQUIRE(easy_as.empty());
}

/* Priority Queues (Heaps) */

// A priority queue (also called a heap) is a data structure that supports push and pop operations and keeps elements sorted according to some user-specified comparator object
// The comparator object is a function object invokable with two parameters, returning true if the first argument is less than the second
// When you pop and element from a priority queue, you remove the element that is greatest according to the comparator object

// The STL offers the std::priority_queue in the <queue> header
// A priority_queue has thress templates paramters
//   The underlying type of the wrapped container
//   The type of the wrapped container
//   The type of the comparator object

// Only the underlying type is mandatory.
// The wrapped container type defaults to vector (probably because it's the most widely used sequential container)
// The comparator object type defaults to std::less

// Note
// The std::less class template is available from the <functional> header, and it returns true if the first argument is less than the second

TEST_CASE("std::priority_queue supports push/pop") {
    std::priority_queue<double> prique;
    prique.push(1.0); // 1.0
    prique.push(2.0); // 2.0 1.0
    prique.push(1.5); // 2.0 1.5 1.0

    REQUIRE(prique.top() == Approx(2.0));
    prique.pop();     // 1.5 1.0
    prique.push(1.0); // 1.5 1.0 1.0

    REQUIRE(prique.top() == Approx(1.5));
    prique.pop();     // 1.0 1.0
    REQUIRE(prique.top() == Approx(1.0));
    prique.pop();     // 1.0
    REQUIRE(prique.top() == Approx(1.0));
    prique.pop();     //
    REQUIRE(prique.empty());
}

// Note
// A priority_queue holds its elements in a tree structure, so if you peered into its underlying container, the memory ordening wouldn't match the orders implied by

/* Bitsets */

// A bitset is a data structure that stores a fixed-size bit sequence
// You can manipulate each bit

// The STL offers the std::bitset in the <bitset> header

TEST_CASE("std::bitset supports integer initialization") {
    std::bitset<4> bs(0b0101);
    REQUIRE_FALSE(bs[0]);
    REQUIRE(bs[1]);
    REQUIRE_FALSE(bs[2]);
    REQUIRE(bs[3]);
}

// You can also provide string representation

TEST_CASE("std::bitset supports string initialization") {
    std::bitset<4> bs1(0b0110);
    std::bitset<4> bs2("0110");
    REQUIRE(bs1 == bs2);
}

/* Associative Containers */

// This container family splits along three axes

// Whether elements contain keys (a set) or key-value pairs (a map)
// Whether elements are ordered
// Whether key are unique

/* Sets */

// The std::set is an associative container that contains sorted, unique elements called keys
// Beacause set stores sorted elements, you can insert, remove, and search efficiently

// Constructing

// The class template set<T, Comparator, Allocator> takes three template paramters
//   The key type T
//   The comparator type that defaults to std::less
//   The allocator type that defaults to std::allocator<T>

TEST_CASE("std::set supports") {
    std::set<int> emp;
    std::set<int> fib{ 1, 1, 2, 3, 5 };

    SECTION("deault construction") {
        REQUIRE(emp.empty());
    }
    SECTION("braced initialization") {
        REQUIRE(fib.size() == 4);
    }
    SECTION("copy construction") {
        auto fib_copy(fib);
        REQUIRE(fib.size() == 4);
        REQUIRE(fib_copy.size() == 4);
    }
    SECTION("move construction") {
        auto fib_moved(std::move(fib));
        REQUIRE(fib.empty());
        REQUIRE(fib_moved.size() == 4);
    }
    SECTION("range construction") {
        std::array<int, 5> fib_array{ 1, 1, 2, 3, 5 };
        std::set<int> fib_set(fib_array.cbegin(), fib_array.cend());
        REQUIRE(fib_set.size() == 4);
    }
}

/* Element Access */

// The basic method is find, which takes a const reference to a key and returns an iterator
// If the set contains an element-matching key, `find` will return an iterator pointing to the found element
// If the set does not, it will return an iterator pointing to end
// The lower_bound method returns an iterator to the first element not less than the key argument, whereas the upper_bound method returns the first element greater than the given key

TEST_CASE("std::set allows access") {
    std::set<int> fib{ 1, 1, 2, 3, 5 };
    SECTION("with find") {
        REQUIRE(*fib.find(3) == 3);
        REQUIRE(fib.find(100) == fib.end());
    }
    SECTION("with count") {
        REQUIRE(fib.count(3) == 1);
        REQUIRE(fib.count(100) == 0);
    }
    SECTION("with lower_bound") {
        auto itr = fib.lower_bound(3);
        REQUIRE(*itr == 3);
    }
    SECTION("with upper_bound") {
        auto itr = fib.upper_bound(3);
        REQUIRE(*itr == 5);
    }
    SECTION("with equal_range") {
        auto pair_itr =fib.equal_range(3);
        REQUIRE(*pair_itr.first == 3);
        REQUIRE(*pair_itr.second == 5);
    }
}

// A set also exposes iterators through its begin and end methods, soy you can use range-based for loops to iterate through the set from least element to greatest

/* Adding Elements */

// You have three options when adding elements to a set

//   insert
//     to copy an existing element into the set
//   emplace
//     to in-place construct a new element into the set
//   emplace_hint
//     to in-place construct a new element, just like emplace
//     The difference is the emplace_hint method takes an iterator as its first argument
//     The iterator is the search's starting point (a hint)
//     If the iterator is close to the correct position for the newly inserted element,
//     this can provide a substantial speedup

TEST_CASE("std::set allows insertion") {
    std::set<int> fib{ 1, 1, 2, 3, 5 };

    SECTION("with insert") {
        fib.insert(8);
        REQUIRE(fib.find(8) != fib.end());
    }
    SECTION("with emplace") {
        fib.emplace(8);
        REQUIRE(fib.find(8) != fib.end());
    }
    SECTION("with emplace_hint") {
        fib.emplace_hint(fib.end(), 8);
        REQUIRE(fib.find(8) != fib.end());
    }
}

/* Removing Elements */

TEST_CASE("std::set allows removal") {
    std::set<int> fib{ 1, 1, 2, 3, 5 };

    SECTION("with erase") {
        fib.erase(3);
        REQUIRE(fib.find(3) == fib.end());
    }
    SECTION("with clear") {
        fib.clear();
        REQUIRE(fib.empty());
    }
}


/* Mutlisets */

// The std::multiset available in the STLs <set> header is an associative container that contains sorted, non-unique keys
// A multiset supports the same operations as set, but it will store redundant elements
// This has important ramifications for two methods
//
//   The method count can return values other than 0 or 1
//   The count method of multiset will the how many elements matched the given key
//
//   The method equal_range can return half-open ranges containing more than one element
//   The equal_range method of multise will return a range containing alll the elements
//   matching the given key

TEST_CASE("std::multiset handles non-unique elements") {
    std::multiset<int> fib{ 1, 1, 2, 3, 5 };
    SECTION("as reflected by size") {
        REQUIRE(fib.size() == 5);
    }
    SECTION("and count returns values greater than 1") {
        REQUIRE(fib.count(1) == 2);
    }
    SECTION("and equal_range returns non-trivial ranges") {
        auto [begin, end] = fib.equal_range(1);
        REQUIRE(*begin == 1);
        ++begin;
        REQUIRE(*begin == 1);
        ++begin;
        REQUIRE(begin == end);
    }
}

/* Unordered Sets */

// The std::unordered_set available in the STL's <unordered_set> header is an associative container that contains unsorted, unique keys
// The unorderedd_set supports most of the same operations as set and multiset, but its internal storage model si completely different

// Rather than using a comparator to sort elements into red-black tree, an unordered_set us usually implemented as a hash table

/* Storage Model: Hash Tables */

// A hash functions, or hasher, is a function that accepts a key and returns a unique size_t value called a hash code
// The unordered_set organizes its elements into a hash table, which associate a hash code with a collection of one or more elements called a `bucket`
// To find an element, an unordered_set computes its hash code and then searches through the corresponding bucket in the hash table

// A hash function has several requirements
//
//   It accepts a Key and returns a size_t hash code
//   It doesn't throw exceptions
//   Equal keys yield equal hash codes
//   Unequal keys yield unequal hashcodes with high probability

// The STL provides the hasher class template std::hash<T> in the <functional> header, which contains specializations for fundamental types, enumeration types, pointer types, optional, variant, smart pointers, and more

TEST_CASE("std::hash<long> returns") {
    std::hash<long> hasher;
    auto hash_code_42 = hasher(42);
    SECTION("equal hash codes for equal keys") {
        REQUIRE(hash_code_42 == hasher(42));
    }
    SECTION("unequal hash codes for unequal keys") {
        REQUIRE(hash_code_42 != hasher(43));
    }
}

// The STL provides a class template std::equal<T> into the <functional> header, which simply invokes operator== on its arguments

TEST_CASE("std::equal_to<long> returns") {
    std::equal_to<long> long_equal_to;
    SECTION("true when arguments equal") {
        REQUIRE(long_equal_to(42, 42));
    }
    SECTION("false when arguments unequal") {
        REQUIRE_FALSE(long_equal_to(42, 43));
    }
}

/* Constructing */

// The class template std::unordered_set<T, Hash, KeyEqual, Allocator> takes four template paramters
//
//   Key type T
//   The Hash hash function type, which defaults to to std::hash<T>
//   The KeyEqual equality function type, which defaults to std::equal_to<T>
//   The Allocator allocator type, which defaults to std::allocator<T>

// An unordered_set supports constructors to set with adjustments for the different template paramters (set needs a Comparator, whereas unordered_set needs a Hash and a KeyEqual)

// An unordered_set supports all set operations, except for lower_bound and upper_bound, because unordered_set doesn't sort its elements

/* Bucket Management */

// Generally, the reason you reach for an unordered_set is its high performance
// Unfortunately, this performance comes at a cost: unodered_set objects have a somwhat complicated interior structure

TEST_CASE("std::unordered_set") {
    std::unordered_set<unsigned long> sheep(100);
    SECTION("allows bucket count specificatio on construction") {
        REQUIRE(sheep.bucket_count() >= 100);
        REQUIRE(sheep.bucket_count() <= sheep.max_bucket_count());
        REQUIRE(sheep.max_load_factor() == Approx(1.0));
    }
    SECTION("allows us to reserve space for elements") {
        sheep.reserve(100'000);
        sheep.insert(0);
        REQUIRE(sheep.load_factor() <= 0.00001);

        while (sheep.size() < 100'000) {
            sheep.insert(sheep.size());
        }
        REQUIRE(sheep.load_factor() <= 1.0);
    }
}

/* Maps */

// The std::map available in the STL's <map> header is an associative container that contains key-value pairs
// The keys of a mao are sorted and unique, and map supports all the same operations as set
// In fact, you can think of a set as a special kind of map containing keys and empty values
// Accordingly, map supports efficient insertion, removal, and serach, and you have control over sorting with comparator objects

/* Constructing */

// The class template map<Key, Value, Comparator, Allocator> takes four template paramters

auto colour_of_magic = "Colour of Magic";
auto the_light_fantastic = "The Light Fantastic";
auto equal_rites = "Equal Rites";
auto mort = "Mort";

TEST_CASE("std::map supports") {
    SECTION("default construction") {
        std::map<const char*, int> emp;
        REQUIRE(emp.empty());
    }
    SECTION("braced intitialization") {
        std::map<const char*, int> pub_year {
            { colour_of_magic, 1983 },
            { the_light_fantastic, 1986 },
            { equal_rites, 1987 },
            { mort, 1987 }
        };
        REQUIRE(pub_year.size() == 4);
    }
}

/* Element Access */

TEST_CASE("std::map is an associative array with") {
    std::map<const char*, int> pub_year {
        { colour_of_magic, 1983 },
        { the_light_fantastic, 1986 }
    };
    SECTION("operator[]") {
        REQUIRE(pub_year[colour_of_magic] == 1983);

        pub_year[equal_rites] = 1987;
        REQUIRE(pub_year[equal_rites] == 1987);

        REQUIRE(pub_year[mort] == 0);
    }
    SECTION("an at method") {
        REQUIRE(pub_year.at(colour_of_magic) == 1983);

        REQUIRE_THROWS_AS(pub_year.at(equal_rites), std::out_of_range);
    }
}

/* Adding Elements */

TEST_CASE("std::map supports insert") {
    std::map<const char*, int> pub_year;
    pub_year.insert({ colour_of_magic, 1983 });
    REQUIRE(pub_year.size() == 1);

    std::pair<const char*, int> tlfp{ the_light_fantastic, 1986 };
    pub_year.insert(tlfp);
    REQUIRE(pub_year.size() == 2);

    auto [itr, is_new] = pub_year.insert({ the_light_fantastic, 9999 });
    REQUIRE(itr->first == the_light_fantastic);
    REQUIRE(itr->second == 1986);
    REQUIRE_FALSE(is_new);
    REQUIRE(pub_year.size() == 2);
}

// A map also offers an insert_or_assign method, which, unlike insert, will overwrite an existing value

TEST_CASE("std::map supports insert_or_assign") {
    std::map<const char*, int> pub_year{
        { the_light_fantastic, 9999 }
    };
    auto [itr, is_new] = pub_year.insert_or_assign(the_light_fantastic, 1986);
    REQUIRE(itr->second == 1986);
    REQUIRE_FALSE(is_new);
}

/* Removing Elements */

TEST_CASE("We can remove std::map elements using") {
    std::map<const char*, int> pub_year {
        { colour_of_magic, 1983 },
        { mort, 1987 }
    };
    SECTION("erase") {
        pub_year.erase(mort);
        REQUIRE(pub_year.find(mort) == pub_year.end());
    }
    SECTION("clear") {
        pub_year.clear();
        REQUIRE(pub_year.empty());
    }
}
