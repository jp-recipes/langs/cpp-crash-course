#include <cstdio>
#include <wchar.h>

// MARK: - Formats
void binary_format() {
    unsigned short a = 0b10101010;
    printf("%hu\n", a);
}

void octal_format() {
    int b = 0123;
    printf("%d\n", b);
}

void exadecimal_format() {
    unsigned long long d = 0xFFFFFFFFFFFFFFFF;
    printf("%llu\n", d);
}

// MARK: - Character Types

void characters_size() {
    /// Always 1 byte, may or may not be signed (ASCII)
    printf("char: %zd\n", sizeof(char));

    /// 2-byte character sets (UTF-16)
    printf("char16_t: %zd\n", sizeof(char16_t));

    /// 4-byte character sets (UTF-32)
    printf("char32_t: %zd\n", sizeof(char32_t));

    /// Large enough to contain the largest character of the implementation 's locale
    printf("wchar_t: %zd\n", sizeof(wchar_t));
}

void arrays() {
    unsigned long maximum = 0;
    unsigned long values[] = { 10, 50, 20, 40, 0 };

    // Why using `size_t` instead of `int` ?
    // 'values' could theoretically take up the maximum storage allowed
    // Although `size_t` is guaranted to be able to index any value within it, int is not
    // In practice, it makes little difference, but technically `size_t` is correct
    for(size_t i = 0; i < 5; i++) {
        if(values[i] > maximum) {
            maximum = values[i];
        }
    }

    printf("The maximum value is %lu", maximum);
}

void range_based_for_loop() {
    unsigned long maximum = 0;
    unsigned long values[] = { 10, 50, 20, 40, 0 };

    for(unsigned long value: values) {
        if(value > maximum) {
            maximum = value;
        }
    }

    printf("The maximum value is %lu", maximum);
}

void number_of_elements_in_an_array() {
    short array[] = { 104, 105, 32, 98, 105, 108, 0 };

    size_t n_elements = sizeof(array) / sizeof(short);

    printf("Size: %zu\n", n_elements);
}

void c_style_strings() {
    char english[] = "A book holds a house of gold.";
    char16_t chinese[] = u"\u4e66\u4e2d\u81ea\u6709\u9ec4\u91d1\u5c4b";

    printf("English: %s\n", english);
    printf("Chinese: %s\n", chinese);
}

// An `enum class` is one of two kinds of enumerations: it's called a _scoped enum_
// For comaptibility with C, C++ also supports an unescoped enum, wich is declared with _enum_ only
void enumeration_types() {
    enum class Race {
        Dinan, Teklan, Ivyn, Moiran, Camite, Julian, Aidan
    };

    Race race = Race::Aidan;

    switch (race) {
        case Race::Dinan: {
            printf("You work hard.");
        } break;
        case Race::Teklan: {
            printf("You are very strong.");
        } break;
        default: {
            printf("Error: unknown race!");
        }
    }
}

// PODs are simple containers
// Pods have some userful low-level features: they are C compatible
// You can employ machine instructions that are highly efficient to copy or move them
// They can be efficiently represented in memory
//
// C++ guaranted that members will be sequential in memory
// Some implementations require members to be aligned algon word boundaries wich depende on CPU register length
// As a general rule, you should order members from largest to smallest within POD definitions
void plain_old_data_classes() {
    struct Book {
        char name[256];
        int year;
        int pages;
        bool hardcover;
    };

    Book neuromancer;
    neuromancer.pages = 271;
    printf("Neuromancer has %d pages.", neuromancer.pages);
}

// The union is a cousin of the POD
// The main problem with unions: it's up to you to keep track of wich interpretation is appropriate
// You should avoid using unions in all but the rarest of cases
void unions() {
    union Variant {
        char string[10];
        int integer;
        double floating_point;
    };

    Variant v;

    v.integer = 42;
    printf("The ultimate answer: %d\n", v.integer);

    v.floating_point = 2.518281824;
    printf("Euler's number e:   %f\n", v.floating_point);
    printf("A dumpster fire:    %d\n", v.integer);
}

void fully_featured_cpp_classes() {
    struct ClockOfTheLongNow {
        void add_year() {
            year++;
        }

        int year;
    };

    ClockOfTheLongNow clock;
    clock.year = 2017;
    clock.add_year();
    printf("year: %d\n", clock.year);
    clock.add_year();
    printf("year: %d\n", clock.year);
}

// All struct members are public by default
void struct_access_control() {
    struct ClockOfTheLongNow {
        void add_year() { year++; }

        bool set_year(int new_year) {
            if (new_year < 2019) return false;
            year = new_year;
            return true;
        }
    private:
        int year;
    };
}

// A class declare members private by default
void class_access_control() {
    class ClockOfTheLongNow {
        int year;

    public:
        void add_year() { year++; }

        bool set_year(int new_year) {
            if (new_year < 2019) return false;
            year = new_year;
            return true;
        }

        int get_year() { return year; }
    };
}

void constructors() {
    struct ClockOfTheLongNow {
        ClockOfTheLongNow() {
            year = 2019;
        }

        ClockOfTheLongNow(int year_in) {
            if(!set_year(year_in)) {
                year = 2019;
            }
        }

        bool set_year(int new_year) {
            if (new_year < 2019) return false;
            year = new_year;
            return true;
        }

        int get_year() { return year; }
    private:
        int year;
    };

    ClockOfTheLongNow defaultConstructorClock;
    printf("Year: %d\n", defaultConstructorClock.get_year());
    ClockOfTheLongNow clock{ 2020 };
    printf("Year: %d\n", clock.get_year());
}

void initializing_a_fundamental_type_to_zero() {
    int a = 0;  // Initialized to 0
    int b{};    // Initialized to 0
    int c = {}; // Initialized to 0
    int d;      // Initialized to 0 (maybe)
}

void initializing_a_fundamental_type_to_an_arbitrary_value() {
    int e = 42;     // equals
    int f{ 42 };    // braced initialization
    int g = { 42 }; // equals-plus-braces initialization
    int h(42);      // parentheses
}

void initializing_pods() {
    struct PodStruct {
        u_int64_t a;
        char b[256];
        bool c;
    };

    PodStruct initialized_pod1{};    // All fields zeroed
    PodStruct initialized_pod2 = {}; // All fields zeroed

    PodStruct initialized_pod3{ 42, "Hello" };       // Fields a & b set; c = 0
    PodStruct initialized_pod4{ 42, "Hello", true }; // All fields set
}

void initializing_arrays() {
    int array_1[]{ 1, 2, 3 };  // Array of length 3; 1, 2, 3
    int array_2[5]{};          // Array of length 5; 0, 0, 0, 0, 0
    int array_3[5]{ 1, 2, 3 }; // Array of length 5; 1, 2, 3, 0, 0
    int array_4[5];            // Array of length 5; uninitialized values
}

void fully_featured_classes() {
    struct Taxonomist {
        Taxonomist() {
            printf("(no argument)\n");
        }
        Taxonomist(char x) {
            printf("char: %c\n", x);
        }
        Taxonomist(int x) {
            printf("int: %d\n", x);
        }
        Taxonomist(float x) {
            printf("float: %f\n", x);
        }
    };

    Taxonomist t1;              // (no argument)
    Taxonomist t2{ 'c' };       // char: c
    Taxonomist t3{ 65537 };     // int: 65537
    Taxonomist t4{ 6.02e23f };  // float: 602000017271895229464576.000000
    Taxonomist t5('g');         // char: g
    Taxonomist t6 = { 'l' };    // char: l
    Taxonomist t7{};            // (no argument)
    //Taxonomist t8(); This is a function declaration so it won't be called
}

void narrowing_conversions() {
    float a{ 1 };
    float b{ 2 };
    int narrowed_result(a/b);   // Potentially nasty narrowing conversion. NOT RECOMMENDED

    // Compiler genearates warning and won't let us build the project
    // int result{ a/b };   RECOMMENDED syntax
}

void the_destructor() {
    struct Earth {
        ~Earth() {
            printf("Making way for hyperspace bypass\n");
        }
    };

    Earth e;
}

void initializing_class_members() {
    bool gold = true;
    int year_of_smelting_accident{ 1970 };
    char ley_location[8] = { "x-rated" };
}

// MARK: - Main
int main(int argc, const char * argv[]) {
    the_destructor();
    return 0;
}
