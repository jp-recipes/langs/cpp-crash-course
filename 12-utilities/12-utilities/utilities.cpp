#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <boost/logic/tribool.hpp>
#include <optional>
#include <utility>
#include <any>
#include <variant>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <thread>
#include <chrono>
#include <boost/numeric/conversion/converter.hpp>


/* Data Structures */

/* tribool */

// The tribool is a bool-like type that supports three states rather than two: true, false, and indeterminate
// Boost offers boost::logic::tribool

using boost::logic::indeterminate;
boost::logic::tribool t = true, f = false, i = indeterminate;

TEST_CASE("Boost tribool converts to bool") {
    REQUIRE(t);
    REQUIRE_FALSE(f);
    REQUIRE(!f);
    REQUIRE_FALSE(!t);
    REQUIRE(indeterminate(i));
    REQUIRE_FALSE(indeterminate(t));
}

/* Boolean Operators */

// The tribool class support all the Boolean operators
// Whenever a tribool expression deson't involve an indeterminate value, the result is the same as the equivalente Boolean expression
// Whenever an indeterminate is involved, the result can be indeterminate

TEST_CASE("Boost tribool supports Boolean operations"){
    auto t_or_f = t || f;
    REQUIRE(t_or_f);
    REQUIRE(indeterminate(t && indeterminate));
    REQUIRE(indeterminate(f || indeterminate));
    REQUIRE(indeterminate(!i));
}

/* When to Use tribool */

// You can use a tribool in settings in which operations can take a long time
// In such sttings, a tribool could describe whether the operation was successful
// An indeterminate value could model that the operation is still pending

TEST_CASE("Boost tribool works nicely wioth if statements") {
    if (i) FAIL("Indeterminate is true");
    else if (!i) FAIL("Indeterminate is false");
    else {} // OK, indeterminate
}

/* optional */

// An optional is a class template that contains a value that might or might not be present
// The primary use case for an optional is the return type of a function that might fail
// Rather than throwing an exception or returining multiple values, a function can instead return an optional that will contain a value if the function succeeded

struct TheMatrix {
    TheMatrix(int x) : iteration { x } { }
    const int iteration;
};

enum Pill { Red, Blue };

std::optional<TheMatrix> take(Pill pill) {
    if (pill == Pill::Blue) return TheMatrix{ 6 };
    return std::nullopt;
}

TEST_CASE("std::optional contains types") {
    if (auto matrix_opt = take(Pill::Blue)) {
        REQUIRE(matrix_opt->iteration == 6);
        auto& matrix = matrix_opt.value();
        REQUIRE(matrix.iteration == 6);
    } else {
        FAIL("The oprtional evaluated to false.");
    }
}

TEST_CASE("std::optional can be empty") {
    auto matrix_opt = take(Pill::Red);
    if (matrix_opt) FAIL("The Matrix is not empty.");
    REQUIRE_FALSE(matrix_opt.has_value());
}

/* pair */

// A pair is a class template that contains two objects of different types in a single object
// The objects are ordered, and you can access them via the members first and second
// A pair supports comparison operators, has defaulted copy/move constructors, and works with structured binding syntax
// THe stdlib has std::pair in the <utility> heasder, and Boost has boost::pair in the <boost/pair.hpp> header

// Note
// Boost also has boost:compressed_pair available in the <boost/compressed_pair.hpp> header
// It's slightly more efficient when one of the members is empty

struct Socialite { const char* birthname; };
struct Valet { const char* surname; };
Socialite bertie{ "Wilberforce" };
Valet reginald{ "Jeeves" };

TEST_CASE("std::pair permits access to members") {
    std::pair<Socialite, Valet> inimitable_duo{ bertie, reginald };
    REQUIRE(inimitable_duo.first.birthname == bertie.birthname);
    REQUIRE(inimitable_duo.second.surname == reginald.surname);
}

// Structured Binding

TEST_CASE("std::pair works with structured binding") {
    std::pair<Socialite, Valet> inimitable_duo{ bertie, reginald };

    auto& [idle_rich, butler] = inimitable_duo;

    REQUIRE(idle_rich.birthname == bertie.birthname);
    REQUIRE(butler.surname == reginald.surname);
}

/* tuple */

// A tuple is a class template that takes an arbitrary number of heterogeneous elements
// It's a generalization of pair, but a tuple doesn't expose its members as first, second, and so on like a pair
// Instead, you use  the non-member function template `get` to extract elements

// The stdlib has std::tuplem and std::get in the <tuple> header, and Boost has boost::tuple and boost::get in the <boost/tuple/tuple.hpp> header

struct Acquaintance { const char* nickname; };
Acquaintance hildebrand{ "Tuppy" };

TEST_CASE("std::tuple permits access to members with std::get") {
    using Trio = std::tuple<Socialite, Valet, Acquaintance>;
    Trio truculent_trio{ bertie, reginald, hildebrand };

    auto& bertie_ref = std::get<0>(truculent_trio);
    REQUIRE(bertie_ref.birthname == bertie.birthname);

    auto& tuppy_ref = std::get<Acquaintance>(truculent_trio);
    REQUIRE(tuppy_ref.nickname == hildebrand.nickname);
}

/* any */

// An any is a class that stores single values of any type
// It is not a class template
// To convert an any into a concrete type, you use an any cast, which is a non-member function template
// Any cast conversions are type safe; if you attempt to cast any and the type doesn't match, you get an exception
// With any, you can perform some kinds of generic programming without templates

// The stdlib has std::any in the <any> header, and Boost hast boost::any in the <boost/any.hpp>

struct EscapeCapsule {
    EscapeCapsule(int x) : weight_kg{ x } {}
    int weight_kg;
};

TEST_CASE("std::any allows us to std::anu_cast into a type") {
    std::any hagunemnon;
    hagunemnon.emplace<EscapeCapsule>(600);

    auto capsule = std::any_cast<EscapeCapsule>(hagunemnon);

    REQUIRE(capsule.weight_kg == 600);
    REQUIRE_THROWS_AS(std::any_cast<float>(hagunemnon), std::bad_any_cast);
}

/* variant */

// A variant is a class template that stores single values whose types are restricted to the user-defined list provided as template parameters
// The variant is a type-sage union
// It shares a lot of functionality with the any tupe, but variant requires that you explicitly enumerate all the types that you'll store

struct BugblatterBeast {
    BugblatterBeast() : is_ravenous{ true }, weight_kg{ 20000 } { }

    bool is_ravenous;
    int weight_kg;
};

/* Constructing a variant */

// A variant can only be default constructed if one of two conditions is met:
//  The first template parameter is default constructible
//  It is monostate, a type intended to communicate that a variant can have an empty state

// Because BugblatterBeast is default constructible (it has a default constructor), make it the first type in the template parameter list so your variant is also default constructible

// std::variant<BugblatterBeast, EscapeCapsule> hagunemnon;

// To store a value into a variant, you use the emplace method template

TEST_CASE("std::variant") {
    std::variant<BugblatterBeast, EscapeCapsule> hagunemnon;
    REQUIRE(hagunemnon.index() == 0);

    hagunemnon.emplace<EscapeCapsule>(600);
    REQUIRE(hagunemnon.index() == 1);

    REQUIRE(std::get<EscapeCapsule>(hagunemnon).weight_kg == 600);
    REQUIRE(std::get<1>(hagunemnon).weight_kg == 600);
    REQUIRE_THROWS_AS(std::get<0>(hagunemnon), std::bad_variant_access);
}

// You can use the non-member function `std::visit` to apply a callable object to a variant
// This has the advantage of dispatching the correct function to handle whatever the contained object is without having to specify explicitly  with std::get

TEST_CASE("std::visit") {
    std::variant<BugblatterBeast, EscapeCapsule> hagunemnon;
    hagunemnon.emplace<EscapeCapsule>(600);

    auto lbs = std::visit([](auto& x){ return 2.2*x.weight_kg; }, hagunemnon);
    REQUIRE(lbs == 1320);
}

/* Date and Time */

// When handling calendar dates and times, look to Boost's DateTime library
// When you're trying to get the current time or measure elapsed time, look to Boost's or stdlib's Chrono libraries and to Boost's Timer library

/* Boost DateTime */

// Boost DateTime library supports date programming with a rich system based on the Gregorian calendar, which is the most widely used civil calendar internationally

/* Constructing a date */

// You can default construct a date, which sets tis value to the especial date boost::gregorian::not_a_date_time
// To construct a date with a valid date, you can use a constructor that accepts three arguments

boost::gregorian::date d{ 1987, 12, 27 };

// You can construct a date from a string

auto s_d = boost::gregorian::from_string("1987/12/27");

TEST_CASE("invalid boost::gregorian::dates throws exceptions") {
    using boost::gregorian::date;
    using boost::gregorian::bad_day_of_month;

    REQUIRE_THROWS_AS(date(1987, 12, 32), bad_day_of_month);
}

// You can obtain the current day from the environment using the non-member function boost::gregorian::day_clock::local_day or boost::gregorian::day_clock::universal_day to obtain the local day based on the system's time zone setting and the UTC day

auto d_local = boost::gregorian::day_clock::local_day();
auto d_univ = boost::gregorian::day_clock::universal_day();

TEST_CASE("boost::gregorian::date supports basic calendar functions") {
    boost::gregorian::date d{ 1987, 12, 27 };
    REQUIRE(d.year() == 1987);
    REQUIRE(d.month() == 12);
    REQUIRE(d.day() == 27);
    REQUIRE(d.day_of_year() == 361);
    REQUIRE(d.day_of_week() == boost::date_time::Sunday);
}

/* Calendar Math */

// Substract one date from another

TEST_CASE("boost::gregorian::date supports calendar arithmetic") {
    boost::gregorian::date d1{ 1987, 12, 27 };
    boost::gregorian::date d2{ 1988,  4, 23 };

    auto duration = d2 - d1;

    REQUIRE(duration.days() == 118);
}

// Add days to a date

TEST_CASE("date and date_duration support addition") {
    boost::gregorian::date d1{ 1987, 12, 27 };
    boost::gregorian::date_duration dur{ 118 };

    auto d2 = d1 + dur;

    REQUIRE(d2 == boost::gregorian::from_string("1988/4/23"));
}

/* Date Periods */

// A date period represents the interval between two dates
// DateTime provides a boost::gregorian::date_period class

TEST_CASE("boost::gregorian::date supports periods") {
    boost::gregorian::date d1{ 1987, 12, 27 };
    boost::gregorian::date d2{ 1988,  4, 23 };

    boost::gregorian::date_period p{ d1, d2 };

    REQUIRE(p.contains(boost::gregorian::date{ 1988, 1, 1 }));
}

/* Other DateTime Features */

// The Boost DateTime library contains three broad categories of programming

/* Date */

// Date programming is the calendar-based programming you just toured

/* Time */

// Time programming, which allows you to work with clocks with microsecondsm is available in the <boost/date_time/posix_time/posix_time.hpp>
// The mechanics are similar to date programming, but you work with clocks instead of Gregorian calendars

/* Local-time */
// Local-time programming is simplu tome-zone-aware time programming
// It's available in the <boost/date_time/time_zone_base.hpp> header

/* Chrono */

// The stdlib Chrono library provides a variety of clocks in the <chrono> header
// You tipically use these when you need to program something that depends on time or for timing your code

// Note
// Boost also offers a CHrono library in the <boost/chrono.hpp> header
// It's a superset of stdlib's Chrono library, which includes, for example, process- and thread-specific clocks and user-defined output formats for time

/* Clocks */

// Three clocks are available in Chrono library; each provides a different guarantee, and all reside in the std::chrono namespace:

// std::chrono:system_clock
// It's the system-wide, real-time clock
// It's sometimes also called the wall clock, the elapsed real time since an impelementation specific start date
// Most implementations specify the Unix start date of January 1, 1970, at midnight

// std::chrono::steady_clock
// It guarantees that its value will never decrease
// This might seem absurd to guarantee, but measuring time is more complicated than it seems
// For example, a system might have to contend with leap seconds or inaccurate clocks

// std::chrono::high_resolution_clock
// It has the shortest tick period available
// A tick is the smallest atomic change that the clock can measure

/* Time Points */

// A time point represents a moment in time, and Chrono encoded time points using the std::chrono::time_point type
// From a user perspective, time_point objects are very simple
// They provide a time_since_epoch method that returns the amount of time elapsed between the time point and the clock's epoch
// This elapsed time us called duration

/* Durations */

// A std::chrono::duration represents the time between the two time_point objects

TEST_CASE("std::chrono supports several clocks") {
    auto sys_now = std::chrono::system_clock::now();
    auto hires_now = std::chrono::high_resolution_clock::now();
    auto steady_now = std::chrono::steady_clock::now();

    REQUIRE(sys_now.time_since_epoch().count() > 0);
    REQUIRE(hires_now.time_since_epoch().count() > 0);
    REQUIRE(steady_now.time_since_epoch().count() > 0);
}

TEST_CASE("std::chrono supports several units of measurement") {
    using namespace std::literals::chrono_literals;
    auto one_s = std::chrono::seconds(1);
    auto thousand_ms = 1000ms;

    REQUIRE(one_s == thousand_ms);
}

TEST_CASE("std::chrono support durant_cast") {
    using namespace std::chrono;
    auto billion_ns_as_s = duration_cast<seconds>(1'000'000'000ns);
    REQUIRE(billion_ns_as_s.count() == 1);
}

/* Waiting */

// Sometimes, you''ll use durations to specify some period fof time for your program to wait
// The stdlib provides concurrency primitives in the <thread> header, which contains the non-member function std::this_thread::sleep_for

TEST_CASE("std::chrono used to sleep") {
    using namespace std::literals::chrono_literals;
    auto start = std::chrono::system_clock::now();
    std::this_thread::sleep_for(100ms);
    auto end = std::chrono::system_clock::now();
    REQUIRE(end - start >= 100ms);
}

/* Numeric */

TEST_CASE("std::numeric_limits::min provides the smallest finite value.") {
    auto my_cup = std::numeric_limits<int>::min();
    auto underfloweth = my_cup - 1;
    REQUIRE(my_cup < underfloweth);
}

/* Boost Numeric Conversion */

using double_to_int = boost::numeric::converter<int, double>;

TEST_CASE("boost::converter offers the static method conerter") {
    REQUIRE(double_to_int::convert(3.14159) == 3);
}

TEST_CASE("boost::numeric::converter implements operator()") {
    double_to_int dti;
    REQUIRE(dti(3.14159) == 3);
    REQUIRE(double_to_int{}(3.15159) == 3);
}

TEST_CASE("boost::numeric::converter checks for overflow") {
    auto yuge = std::numeric_limits<double>::max();
    double_to_int dti;
    REQUIRE_THROWS_AS(dti(yuge), boost::numeric::positive_overflow);
}

TEST_CASE("boost::numeric_cast checks overflow") {
    auto yuge = std::numeric_limits<double>::max();
    REQUIRE_THROWS_AS(boost::numeric_cast<int>(yuge), boost::numeric::positive_overflow);
}
