#include <cstdio>
#include <cstdint>
#include <chrono>

/* Timing */

// To optimize code, you absolutely need to accurate measurements. Ypu can use Chrono to measure how long a series of operations takes
// Boost's Timer library contains the boost::timer::auto_cpu_timer class in the <boost/timer/timer.hpp> header, which is a RAII object that begins timing in its constructor and stop timing in its destructor
// You can build your own makeshift Stopwatch class using just the stdlib Chrono library

struct Stopwatch {
    Stopwatch(std::chrono::nanoseconds& result)
      : result{ result }, start{ std::chrono::high_resolution_clock::now() } { }
    ~Stopwatch() {
        result = std::chrono::high_resolution_clock::now() - start;
    }
private:
    std::chrono::nanoseconds& result;
    const std::chrono::time_point<std::chrono::high_resolution_clock> start;
};

void using_timing() {
    const size_t n = 1'000'000;
    std::chrono::nanoseconds elapsed;
    {
        Stopwatch stopwatch{ elapsed };
        volatile double result{ 1.23e45 };
        for (double i = 1; i < n; i++) {
            result /= i;
        }
    }
    auto time_per_division = elapsed.count() / double{ n };
    printf("Took %gns per division.", time_per_division);
}
