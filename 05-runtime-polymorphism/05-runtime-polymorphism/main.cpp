#include <cstdio>
#include <stdexcept>

/* Inheritance in a Nutshell */

struct BaseClass {};
struct DerivedClass : BaseClass {};
void are_belong_to_us(BaseClass& base) {}

void using_inheritance() {
    DerivedClass derived;
    are_belong_to_us(derived);
}

/* Member inheritance */

// Derived classes inherit non-private members from their base classes

struct MembersBaseClass {
    int the_answer() const { return 42; }

    const char* member = "gold";
private:
    const char* holistic_detective = "Dirk Gently";
};

struct DerivedMembersClass: MembersBaseClass { };

void using_inherited_members() {
    DerivedMembersClass x;
    printf("The answer is %d\n", x.the_answer());
    printf("The answer is %s\n", x.member);
    // This line doesn't compile:
    // printf("%s's Holistic Detective Agency\n", x.holistic_detective);
}

/* Virtual Methods */

// If you want to permite a derived class to override a base class's methods, you use the virutla keyword

struct OverrideBaseClasse {
    virtual const char* final_message() const = 0;
};

struct DerivedOverrideClass: OverrideBaseClasse {
    const char* final_message() const override {
        return "We apologize for the incovenience.";
    }
};

void using_override() {
    //OverrideBaseClasse base; Bang, class is abstract
    DerivedOverrideClass derived;
    OverrideBaseClasse& ref = derived;

    //printf("BaseClass:    %s\n", base.final_message());
    printf("DerivedClass: %s\n", derived.final_message());
    printf("BaseClass&:   %s\n", ref.final_message());
}

/* Abstract Base Class */

struct AbstractBaseClass {
    virtual const char* final_message() const = 0;
};

struct ConcreteClass : AbstractBaseClass {
    const char* final_message() const override {
        return "We apologize for the inconvenience.";
    }
};

void using_concrete_class() {
    // AbstractBaseClass base; // Bang!
    ConcreteClass derived;
    AbstractBaseClass& ref = derived;
    printf("ConcreteClass:      %s\n", derived.final_message());
    printf("AbstractBaseClass&: %s\n", ref.final_message());
}

/* Interfaces */

struct Logger {
    virtual ~Logger() = default;
    virtual void log_transfer(long from, long to, double amount) = 0;
};

struct FileLogger : Logger {
    void log_transfer(long from, long to, double amount) override {
        printf("[file] %ld -> % ld: %f\n", from, to, amount);
    }
};

struct ConsoleLogger : Logger {
    void log_transfer(long from, long to, double amount) override {
        printf("[cons] %ld -> % ld: %f\n", from, to, amount);
    }
};

enum class LoggerType {
    Console,
    File
};

struct Bank {
    Bank() : type { LoggerType::Console } { }

    void set_logger(LoggerType new_type) {
        type = new_type;
    }

    void make_transfer(long from, long to, double amount) {
        switch(type) {
            case LoggerType::Console: {
                consoleLogger.log_transfer(from, to, amount);
                break;
            } case LoggerType::File: {
                fileLogger.log_transfer(from, to, amount);
                break;
            } default: {
                throw std::logic_error("Unknown Logger type encountered.");
            }
        }
    }
private:
    LoggerType type;
    ConsoleLogger consoleLogger;
    FileLogger fileLogger;
};

/* Pure-Virtual Classes and Virtual Destructor */

// You achive interface inheritance through deriving from base classes that contain only pure-virtual methods
// Such class are referred to as pure-virtual classes

// Usually, you add virtual destructor to interfaces.
// In some rare circumstances, it's posible to leak resources if you fail to mark destructor as vritual

struct LeakedBaseClass {};

struct LeakedDerivedClass : LeakedBaseClass {
    LeakedDerivedClass() { printf("LeakedDerivedClass() invoked.\n"); }
    ~LeakedDerivedClass() { printf("~LeakedDerivedClass() invoked.\n"); }
};

void proving_base_class_without_destructor_will_leak_and_derived_class_destrictor_wont_be_invoked() {
    printf("Constructing LeakedDerivedClass x.\n");
    LeakedBaseClass* x{ new LeakedDerivedClass{} };
    printf("Deleting x as a LeakedBaseClass*.\n");
    delete x;
}

// Adding virtual to base class destructor solves the problem

struct NonLeakedBaseClass {
    virtual ~NonLeakedBaseClass() = default;
};

struct NonLeakedDerivedClass : NonLeakedBaseClass {
    NonLeakedDerivedClass() { printf("NonLeakedDerivedClass() invoked.\n"); }
    ~NonLeakedDerivedClass() { printf("~NonLeakedDerivedClass() invoked.\n"); }
};

void proving_base_class_wit_default_vritual_destructor_wont_leak_derived_classes() {
    printf("Constructing NonLeakedDerivedClass x.\n");
    NonLeakedBaseClass* x{ new NonLeakedDerivedClass{} };
    printf("Deleting x as a NonLeakedBaseClass*.\n");
    delete x;
}

// Declaring a protected non-virtual destructor is a good alternative to declaring a public virtual destructor
// It will cause a compilation error when writing code that deletes a base class pointer

// Some don't like this approach because you eventually have to make a class with a public destructor
// If you derive from that class, you run into the same issues

/* Constructor injection */

//

void constructor_injection() {
    struct Bank {
        // Asigna el valor del logger usando member initializer
        // Los references no pueden ser reseteados, aso que el objeto al que apunto logger no cambiará por el resto del ciclo de vida de Bank
        // Fijas ti elección de logger en la construcción de Bank
        Bank(Logger& logger) : logger{ logger } { }

        void make_transfer(long from, long to, double amount) {
            logger.log_transfer(from, to, amount);
        }
    private:
        Logger& logger;
    };

    ConsoleLogger logger;
    Bank bank{ logger };
    bank.make_transfer(1000, 2000, 49.95);
    bank.make_transfer(2000, 4000, 20.00);
}

/* Property injection */

// This approach uses a pointer instead of a reference. Because ponters can be reseated (unlike references)
// You can change the behavior of Bank whenever

void property_injection() {
    struct Bank {
        void set_logger(Logger* new_logger) {
            logger = new_logger;
        }

        void make_transfer(long from, long to, double amount) {
            if (logger) logger->log_transfer(from, to, amount);
        }
    private:
        Logger* logger{};
    };

    ConsoleLogger console_logger;
    FileLogger file_logger;
    Bank bank;

    bank.set_logger(&console_logger);
    bank.make_transfer(1000, 2000, 49.95);

    bank.set_logger(&file_logger);
    bank.make_transfer(2000, 4000, 20.00);
}

/* Constructor and Property injection */

void constructor_and_property_injection() {
    struct Bank {
        Bank(Logger* logger) : logger{ logger } { }

        void set_logger(Logger* new_logger) {
            logger = new_logger;
        }

        void make_transfer(long from, long to, double amount) {
            if (logger) logger->log_transfer(from, to, amount);
        }
    private:
        Logger* logger{};
    };

    ConsoleLogger console_logger;
    FileLogger file_logger;
    Bank bank { &console_logger };

    bank.make_transfer(1000, 2000, 49.95);

    bank.set_logger(&file_logger);
    bank.make_transfer(2000, 4000, 20.00);
}

int main(int argc, const char * argv[]) {
    property_injection();
    return 0;
}
