#include <cstdio>

struct ClockOfTheLongNow {
    ClockOfTheLongNow() {
        year = 2019;
    }

    ClockOfTheLongNow(int year) {
        this->year = year;
    }

    void set_year(int new_year) { year = new_year; }
    int get_year() const { return year; }
private:
    int year;
};

// The member-of-pointer, or arrow operator (->), performs two simultaneous operations
// It dereferences a pointer
// It accesses a member of the pointed-to object
void member_of_pointer_operator() {
    ClockOfTheLongNow clock;
    ClockOfTheLongNow* clock_ptr = &clock;
    clock_ptr->set_year(2020);

    printf("Address of clock; %p\n", clock_ptr);
    printf("Value of clock's year: %d\n", clock_ptr->get_year());
    printf("Value of clock's year: %d\n", (*clock_ptr).get_year());
}

struct College {
    char name[256];
};

// Array decays into a pointer
void print_names(College* colleges, size_t n_colleges) {
    for (size_t i = 0; i < n_colleges; i++) {
        printf("%s College\n", colleges[i].name);
    }
}

void array_decay_into_a_pointer() {
    College oxford[] = { "Magdalen", "Nuffield", "Kellogg" };
    print_names(oxford, sizeof(oxford) / sizeof(College));
}

void add_year(ClockOfTheLongNow& clock) {
    clock.set_year(clock.get_year() + 1); // No defer operator needed
}

// References are safer, more conveninet versiones of pointers
// References can not be assigned to null (easily) and can not be reseated (or reassigned)
void references() {
    ClockOfTheLongNow clock;
    printf("The year is %d.\n", clock.get_year());

    add_year(clock); // Clock is implicitly passed by reference
    printf("The year is %d.\n", clock.get_year());
}

struct Element {
    Element* next{};
    void insert_after(Element* new_element) {
        new_element->next = next;
        next = new_element;
    }
    char prefix[2];
    short operating_number;
};

void forward_linked_list() {
    Element trooper1, trooper2, trooper3;
    trooper1.prefix[0] = 'T';
    trooper1.prefix[1] = 'K';
    trooper1.operating_number = 421;
    trooper1.insert_after(&trooper2);

    trooper2.prefix[0] = 'F';
    trooper2.prefix[1] = 'N';
    trooper3.operating_number = 2187;
    trooper2.insert_after(&trooper3);

    trooper3.prefix[0] = 'L';
    trooper3.prefix[1] = 'S';
    trooper3.operating_number = 005;

    for (Element *cursor = &trooper1; cursor; cursor = cursor->next) {
        printf("stormtrooper %c%c-%d\n", cursor->prefix[0], cursor->prefix[1], cursor->operating_number);
    }
}

// References can not be reseated
void employing_references() {
    int original = 100;
    int& original_ref = original;
    printf("Original: %d\n", original);         // Original:  100
    printf("Reference: %d\n", original_ref);    // Reference: 100

    int new_value = 200;
    original_ref = new_value;                   // Its original' value what's is changed
    printf("Original:  %d\n", original);        // Original:  200
    printf("New Value: %d\n", new_value);       // New Value: 200
    printf("Reference: %d\n", original_ref);    // Reference: 200
}

void petruchio(const char* shrew) {
    printf("Fear not, sweet wench, they shall no touch thee, %s.", shrew);
    // shrew[0] = "K"; // Compiler error! The shrew cannot be tamed.
}

// Making an argument `const` precludes its modification within a dunction's scope
// A `const` pointer or reference provides you with an efficient mechanism to pass and object into a function for read-only use
void const_arguments() {
    char shrew[] = "123";
    petruchio(shrew); // & not necessary for array
}

// Making a methods `const` communicates that you promise not to modufy the current object's state within the `const` method
// Had get_year not been marked a `const` method, would not compile becouse clock is a `const` reference and cannot be modified within is_leap_year
void const_methods() {
    struct ClockOfTheLongNow {
        int get_year() const { return year; }

        bool is_leap_year(const ClockOfTheLongNow& clock) {
            if (clock.get_year() % 4 > 0) return false;
            if (clock.get_year() % 100 > 0) return true;
            if (clock.get_year() % 400 > 0) return false;
            return true;
        }
    private:
        int year;
    };
}

void const_member_variables() {
    struct Avout {
        const char* name = "Erasmas";
        ClockOfTheLongNow apert;
    };
}

void member_initializer_list() {
    struct Avout {
        Avout(const char* name, int year_of_apert)
            : name { name }, apert { year_of_apert } { }

        void announce() const {
            printf("My name is %s and my next apert is %d.\n", name, apert.get_year());
        }

    private:
        const char* name;
        ClockOfTheLongNow apert;
    };

    Avout raz{ "Erasmas", 3010 };
    Avout jad{ "Jad", 4000 };
    raz.announce();
    jad.announce();
}

void initialization_with_auto() {
    int answer = 42;                    // int

    auto the_answer { 42 };             // int
    auto foot { 12L };                  // long
    auto rootbeer { 5.0F };             // float
    auto cheeseburger { 10.0 };         // double
    auto politifact_claims { false };   // bool
    auto cheese { "string" };           // char[7]
}

void auto_and_reference_types() {
    auto year { 2019 };                 // int
    auto& year_ref = year;              // int&
    const auto& year_cref = year;       // const int&
    auto* year_ptr = &year;             // int*
    const auto* year_cptr = &year;      // const int*
}

void auto_and_code_refactorings() {
    struct Dwarf {

    };

    Dwarf dwarves[13];

    struct Contract {
        void add(const Dwarf&);
    };

    Contract contract;

    for (const auto& dwarf : dwarves) {
        contract.add(dwarf);
    }
}

int main(int argc, const char * argv[]) {
    member_initializer_list();
    return 0;
}
