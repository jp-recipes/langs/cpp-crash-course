#include <cstdio>
#include <stdexcept>
#include <cstddef>

/* Template Class Definition */

template <typename X, typename Y, typename Z>
struct MyTemplateClass {
    X foo(Y&);
private:
    Z* member;
};

/* template Function Definitions */

template <typename X, typename Y, typename Z>
X my_template_function(Y& arg1, const Z* arg2) {
    return nullptr;
}

/* Const Cast */

void carbon_thaw(const int& encased_solo) {
    // encased_solo++ // Compiler error; modifying const
    auto& hibernation_sick_solo = const_cast<int&>(encased_solo);
    hibernation_sick_solo++;
}

/* Static Cast */

// The static_cast reverses a well-defined implicit conversion, such as an integer type to another integer type
// The object-to-cast is of some type that the desired-type implicitly converts to
// The reason you might need static_cast os that generally, implicit casts aren't reversible

short incremente_as_short(void* target) {
    auto as_short = static_cast<short *>(target);
    *as_short = *as_short + 1;
    return *as_short;
}

void using_static_cast() {
    short beast{ 665 };
    auto mark_of_the_beast = incremente_as_short(&beast);
    printf("%d is the mark_of_the_beast.", mark_of_the_beast);
}

/* Reinterpret cast */

// Sometimes in low-level programming, you must perform type conversions that are not well defined
// In system programming and especially in embedded environments, you ofen need complete control over how to interprete memory

void using_reinterpret_cast() {
    auto timer = reinterpret_cast<const unsigned long*>(0x1000); // Converts int to a pointer ??? BAD
    // This compiles, but it will explode at runtime
    printf("Timer is %lu.", *timer);
}

/* Narrow cast */

// static_cast performs a runtime check for `narrowing`
// Narrowing is a loss in information. Think about converting from an `int` to a `short`

template <typename To, typename From>
To narrow_cast(From value) {
    const auto converted = static_cast<To>(value);
    const auto backwards = static_cast<From>(converted);

    if (value != backwards) throw std::runtime_error{ "Narrowed!" };

    return converted;
}

void using_narrow_cast() {
    int perfect{ 496 };
    const auto perfect_short = narrow_cast<short>(perfect);
    printf("perfect_short: %d\n", perfect_short);

    try {
        int cyclic{ 142857 };
        const auto cyclic_short = narrow_cast<short>(cyclic);
        printf("cyclic_short: %d\n", cyclic_short);
    } catch (const std::runtime_error& e) {
        printf("Exception: %s\n", e.what());
    }
}

/* mena: A Template Function Example */

double mean(const double* values, size_t length) {
    double result{};
    for(size_t i{}; i < length; i++) {
        result += values[i];
    }
    return result / length;
}

// Genericizing mean

template<typename T>
T mean(const T* values, size_t length) {
    T result{};
    for(size_t i{}; i < length; i++) {
        result += values[i];
    }
    return result / length;
}

void using_template_for_mean() {
    const double nums_d[] { 1.0, 2.0, 3.0, 4.0 };
    const auto result1 = mean<double>(nums_d, 4); // template instantiated
    printf("double: %f\n", result1);

    const float nums_f[] { 1.0f, 2.0f, 3.0f, 4.0f };
    const auto result2 = mean<float>(nums_f, 4); // template instantiated
    printf("float: %f\n", result2);

    const size_t nums_c[] { 1, 2, 3, 4 };
    const auto result3 = mean<size_t>(nums_c, 4); // template instantiated
    printf("size_t: %zd\n", result3);
}

/* SimpleUniquePointer: A Template Class Example */

// A unique pointer is a RAII wrapper around a free-storage allocated object
// As its name suggests, the unique pointer has a single owner at a time

template <typename T>
struct SimpleUniquePointer {
    // Constructos
    SimpleUniquePointer() = default; // Apply deafult constructor features (pointer will be null)
    SimpleUniquePointer(T* pointer) : pointer{ pointer } { }
    // Destructor
    ~SimpleUniquePointer() {
        if(pointer) delete pointer;
    }
    // Copy Constructor (disabled)
    SimpleUniquePointer(const SimpleUniquePointer&) = delete;
    // Copy Assignment (disabled)
    SimpleUniquePointer& operator=(const SimpleUniquePointer&) = delete;
    // Move Constructor (we want our unique pointer to be movable)
    SimpleUniquePointer(SimpleUniquePointer&& other) noexcept : pointer{ other.pointer } {
        other.pointer = nullptr;
    }
    // Move Assignment (we want our unique pointer to be movable)
    SimpleUniquePointer& operator=(SimpleUniquePointer&& other) noexcept {
        if(pointer) delete pointer;
        pointer = other.pointer;
        other.pointer = nullptr;
        return *this;
    }
    T* get() {
        return pointer;
    }
private:
    T* pointer;
};

struct Tracer {
    Tracer(const char* name) : name{ name } {
        printf("%s constructed.\n", name);
    }
    ~Tracer() {
        printf("%s destructed.\n", name);
    }
private:
    const char* const name;
};

void consumer(SimpleUniquePointer<Tracer> consumer_ptr) {
    printf("(cons) consumer_ptr: 0x%p\n", consumer_ptr.get());
}

void using_unique_pointer() {
    auto ptr_a = SimpleUniquePointer<Tracer>(new Tracer{ "ptr_a" });
    printf("(main) ptr_a_ 0x%p\n", ptr_a.get());
    consumer(std::move(ptr_a));
    printf("(main) ptr_a_ 0x%p\n", ptr_a.get());
}

/* Concepts */

// Concepts constraint template parameters, allowing for parameter checking at the point of instantation rather than the point of first use
// Concepts allow you to express requirements on template parameters directly in the language

// GCC 6.0 or later
// -fconcepts

// A Concepts is a template
// It's a constant expression involving template arguments, evaluated at compile time
// Thhink of a concept as a one big predicate: a function that evaluates to true or false

// If a set of template parameters meets a criteria for a given concept, that concept evaluates to true when instantiated with those parameters; otherwise it will evaluate to false

constexpr const char* as_str(bool x) { return x ? "True" : "False"; }

void using_traits() {
    printf("%s\n", as_str(std::is_integral<int>::value));
    printf("%s\n", as_str(std::is_integral<const int>::value));
    printf("%s\n", as_str(std::is_integral<char>::value));
    printf("%s\n", as_str(std::is_integral<uint64_t>::value));
    printf("%s\n", as_str(std::is_integral<int&>::value));
    printf("%s\n", as_str(std::is_integral<int*>::value));
    printf("%s\n", as_str(std::is_integral<float>::value));
}

/* Requirements */

// Requirements are ad hoc constraints on template parameters
// Each concept can specify any number of reuirements in its template parameters
// Requirements are encoded into requires expressions denoted by the `requires` keyword followed by function arguments and a body

/* Building Concepts from Requires Expressions */

// Because requires expressions are evaluated at compile time, concepts can contain any number of them
// Try to construct a concept that guards against the misuse of `mean`

template<typename T>
T mean(T* values, size_t length) {
    T result{};                         // T should be constructible
    for(size_t i{}; i < length; i++) {  // T should support +=
        result += values[i];
    }
    return result / length;             // Dividing a T by a size_t yields a T
}

// Template T
// Where
//  T must be default constructible
//  T support operator +=
//  Dividing a T by a size_t yields a T

// TODO: There was a problem trying to compile with c++20

/* Non-Type Template Parameters */

// A template parameter declared with the `typename` (or class) keyword is called a type template parameter
// Is a stand-in for a some yet-to-be-specified type

// Alternatively, you can use a non-type template parameter, which stand-ins for a some yet-to-be-specified value
// Non-type parameters can be any of the following
//  An integral type
//  An lvalue reference type
//  A pointer (or pointer-to-member) type
//  A std::nullptr_t (the type of nullptr)
//  An enum class

int& get(int (&arr)[10], size_t index) {
    if (index >= 10) throw std::out_of_range{ "Out of bounds" };
    return arr[index];
}

// You can relax the requirement that `arr` refer to an int array by making get a template function

template <typename T>
T& get1(T (&arr)[10], size_t index) {
    if (index >= 10) throw std::out_of_range{ "Out of bounds" };
    return arr[index];
}

// You can relax the requirement that `arr` refer to an array of length 10 by introducing non-type template parameter

template <typename T, size_t Length>
T& get2(T (&arr)[Length], size_t index) {
    if (index >= Length) throw std::out_of_range{ "Out of bounds" };
    return arr[index];
}

// Rather than replacing a specific type (int), you've replaced a specific integral value (10)
// Now you can use the function with arrays of any size

// You can perform a compile time bounds checking by taking size_t index as another non-type template paramter
// This allows you to replace the `std::out_of_range` with a statci_assert

template <size_t Index, typename  T, size_t Length>
T& get(T (&arr)[Length]) {
    static_assert(Index < Length, "Out-of-bounds access");
    return arr[Index];
}

void using_non_type_template_parameter() {
    int fib[]{ 1, 1, 2, 0 };
    printf("%d %d %d ", get<0>(fib), get<2>(fib), get<2>(fib));
    get<3>(fib) = get<1>(fib) + get<2>(fib);
    printf("%d\n", get<3>(fib));
    // printf("%d", get<4>(fib)); Bang!
}

/* Varadic Templates */

// You denote varadic templates using a final template paramter that has a special syntax, namely typename... arguments

template <typename T, typename... Arguments>
SimpleUniquePointer<T> make_simple_unique(Arguments... arguments) {
    return SimpleUniquePointer<T>{ new T{ arguments... }};
}

int main(int argc, const char * argv[]) {
    
    return 0;
}
