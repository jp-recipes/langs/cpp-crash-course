#include <new>
#include <stdexcept>

void logical_operators() {
    auto bitwiseAnd        = 0b1100 & 0b1010; // 0b1000
    auto bitwiseOr         = 0b1100 | 0b1010; // 0b1110
    auto bitwiseXor        = 0b1100 ^ 0b1010; // 0b0110
    auto bitwiseComplement = ~0b1010;         // 0b0101
    auto bitwiseLeftShift  = 0b1010 << 2;     // 0b101000
    auto bitwiseRightShift = 0b1010 >> 2;     // 0b10

    auto trueAnd           = true && true;    // true
    auto falseAnd          = true && false;   // false

    auto trueOr            = false || true;   // true
    auto falseOr           = false || false;  // false

    auto trueNot           = !false;          // true
    auto falseNot          = !true;           // false
}

void unary_arithmetic_operators() {
    char a = 10;  // char
    auto b = -a;  // promoted to int

    float x = 20; // float
    auto y = -x;  // float
}

void binary_arithmetic_operators() {
    long double          ld = 10;
    double               d  = 20;
    float                f  = 30;

    long long            ll = 40;
    long                  l = 50;
    int                   i = 60;

    unsigned long long  ull = 70;
    unsigned long        ul = 80;
    unsigned int         ui = 90;

    auto promoted_long_double = ld + d; // long double + double = long double
    auto promoted_double      = d + f;  // double + float       = double
    auto promoted_float       = f + i;  // float + int          = float

    auto promoted_long_long   = ll + l; // long long + long     = long long
    auto promoted_long        = l + i;  // long + int           = int
}

void assignment_operators() {
    auto y = 10;  // 10
    auto x = y;   // 10

    x += y;       // 20
    x -= y;       // 10
    x *= y;       // 100
    x /= y;       // 10
    x %= y;       // 0
}

void operator_overloading() {
    struct CheckedInteger {
        CheckedInteger(unsigned int value) : value{ value } {}

        CheckedInteger operator+(unsigned int other) const {
            CheckedInteger result{ value + other };
            if (result.value < value) throw std::runtime_error{ "Overflow!" };
            return result;
        }

        const unsigned int value;
    };

    CheckedInteger a{ 100 };
    auto b = a + 200;
    printf("a + 200 = %u\n", b.value);
}

void implicit_type_conversions() {
    auto x = 2.7543245675432456;
    uint8_t y = x; // Silent truncation. No compiler warning

    // uint8_t z{ x }; // BANG!
    // Braced Initialization doesn't permit narrowing conversions
}

void integer_to_integer_conversion() {
    uint8_t x = 0b11111111; // 255
    int8_t  y = 0b11111111; // Implementation defined
    printf("x: %u\ny; %d", x, y);
}

void floating_point_to_floating_point_conversion() {
    double x = std::numeric_limits<float>::max();
    long double y = std::numeric_limits<double>::max();
    float z = std::numeric_limits<long double>::max(); // Undefined Behavior
    printf("x: %g\ny: %Lg\nz: %g", x, y, z);
}

void print_addr(void* x) {
    printf("%p\n", x);
}

void pointer_to_void() {
    int x{};
    print_addr(&x);
    print_addr(nullptr);
}

void explicit_type_conversion() {
    int32_t a = 100;
    int64_t b{ a };

    if (a == b) printf("Non-narrowing conversion!\n");
    // int32_t c{ b }; // Bang!
}

/* C-Style Casts */

// (desired-type)object-to-cast
// Named conversion functions allow you to perform dangerous casts that braced initialization won't permit
// You can also perform C-style casts, but this is done mainly to maintain some compatibility between the languages

void trainwreck(const char* read_only) {
    // accidentally forget `const` correctness with C-style casts
    auto as_unsigned = (unsigned char*)read_only; // but is const
    *as_unsigned = 'b'; // Crashes on Windows 10 x64
}

void c_style_casts() {
    auto ezra = "Ezra";
    printf("Before trainwreck: %s\n", ezra);
    trainwreck(ezra);
    printf("After trainwreck: %s\n", ezra);
}

/* User-Defined Type Conversions */

// You can provide user-defined conversions functions
// This functions tell the compiler how your user-defined types behave during implicit and explicit type conversion

void user_defined_type_conversions() {
    struct ReadOnlyInt {
        ReadOnlyInt(int val): val{ val } { }
        operator int() const {
            return val;
        }
    private:
        const int val;
    };

    ReadOnlyInt the_answer{ 42 };
    auto ten_answers = static_cast<int>(the_answer) * 10;
}

/* Constant Expressions */

// Constant expressions are expressions that can be evaluated at compile time

struct Color {
    float H, S, V;
};

constexpr uint8_t max(uint8_t a, uint8_t b) {
    return a > b ? a : b;
}
constexpr uint8_t max(uint8_t a, uint8_t b, uint8_t c) {
    return max(max(a, b), max(a, c));
}
constexpr uint8_t min(uint8_t a, uint8_t b) {
    return a < b ? a : b;
}
constexpr uint8_t min(uint8_t a, uint8_t b, uint8_t c) {
    return min(min(a, b), min(a, c));
}
constexpr float modulo(float dividend, float divisor) {
    const auto quotient = dividend / divisor;
    return divisor * (quotient - static_cast<uint8_t>(quotient));
}

int main(int argc, const char * argv[]) {

    return 0;
}

