#include <cstdio>
#include <stdexcept>

/* Declaration Statements */

// Declaration statements (or just declarations) introduce identifiers
// such as functions, templates, and namespaces, into your programs

/* Functions */

// Functions that aren't member functions are called non-meber functions, or sometimes free functions, and they are allways declared outside of main() at namespace scope

// A function declaration (interface)
// also called functions's signature, or prototype
void randomize(uint32_t&);

void functions_declaration() {
    size_t iterations{};
    uint32_t number{ 0x4C4347 };
    while (number != 0x4C4347) {
        randomize(number);
        ++iterations;
    }
    printf("%zd", iterations);
}

// Another example

struct RandomNumberGenerator {
    explicit RandomNumberGenerator(uint32_t seed) : number{ seed } {}
    uint32_t next();
    size_t get_iterations() const;
private:
    size_t iterations;
    uint32_t number;
};

// Definitions

uint32_t RandomNumberGenerator::next() {
    ++iterations;
    number = 0x3FFFFFFF & (0x41C64E6D * number + 12345) % 0x80000000;
    return number;
}

size_t RandomNumberGenerator::get_iterations() const {
    return iterations;
}

void using_implementations() {
    RandomNumberGenerator rng{ 0x4C4347 };
    while (rng.next() != 0x4C4347) {
        // do nothing
    }
    printf("%zd", rng.get_iterations());
}

// A function definition (implementation)
void randomize(uint32_t& x) {
    x = 0x3FFFFFFF & (0x41C64E6D * x + 12345) % 0x80000000;
}

/* Namespaces */

// Namespaces prevent naming conflicts
// By default, all symbols you declare go into the global namespace

namespace BroopKidron14 {
    // All symbols declared within this block
    // belong to the BroopKidron13 namespace
}

// Namespaces can be nested in one of two ways
namespace BroopKidron14 {
    namespace Shaltanac {
        // symbols
    }
}

// Second, youcan use the scope-resolution
namespace BroopKidron14::Shaltanac {
    // symbols
}

/* Using Symbols in Namespaces */

namespace BroopKidron13::Shaltanac {
    enum class Color {
        Mauve, Pink, Russet
    };
}

void using_symbols_in_namespace() {
    const auto shaltanac_grass { BroopKidron13::Shaltanac::Color::Russet };
    if (shaltanac_grass == BroopKidron13::Shaltanac::Color::Russet) {
        printf("The other Shaltanac's joopleberry shrub is always a more "
               "mauvey shade of pinky russet.");
    }
}

/* Using Directives */

// You can employee a using directive to avoid a lot of typing
// A using directive imports a symbol into a block or, if you declare a using directive at namespaces scope, into the current namespace

void using_using_directive() {
    using BroopKidron13::Shaltanac::Color;
    const auto shaltanac_grass = Color::Russet;
    if (shaltanac_grass == Color::Russet) {
        printf("The other Shaltanac's joopleberry shrub is always a more "
               "mauvey shade of pinky russet.");
    }
}

/* Type Aliasing */

// A tyoe alias defines a name that refers to a previously defined name
// You can use a type alias as a synonym for the existing type name

using String = const char[260];
using ShaltanacColor = BroopKidron13::Shaltanac::Color;

void using_type_alias() {
    const auto my_color{ ShaltanacColor::Russet };
    String saying {
        "The other Shaltanac's joopleberry shrub is "
        "always a more mauvey shade of pinky russet."
    };
    if (my_color == ShaltanacColor::Russet) {
        printf("%s", saying);
    }
}



template <typename To, typename From>
struct NarrowCaster {
    To cast(From value) const {
        const auto converted = static_cast<To>(value);
        const auto backwards = static_cast<From>(converted);
        if (value != backwards) throw std::runtime_error{ "Narrowed!" };
        return converted;
    }
};

// Apply partial application on template parameteres

template <typename From>
using short_caster = NarrowCaster<short, From>;

void using_partial_application() {
    try {
        const short_caster<int> caster;
        const auto cyclic_short = caster.cast(142857);
        printf("cyclic_short: %d\n", cyclic_short);
    } catch (const std::runtime_error& e) {
        printf("Exception: %s\n", e.what());
    }
}

/* Structured Bindings */

struct TextFile {
    bool success;
    const char* contents;
    size_t n_bytes;
};

TextFile read_text_file(const char* path) {
    const static char contents[]{ "Sometimes the goat is you." };
    return TextFile {
        true,
        contents,
        sizeof(contents)
    };
}

void using_structured_bindings() {
    const auto [success, contents, length] = read_text_file("README.txt");
    if (success) {
        printf("Read %zd bytes: %s\n", length, contents);
    } else {
        printf("Failed to open REAMDE.txt.");
    }
}

/* constexpr */

template <typename  T>
auto value_of(T x) {
    if constexpr (std::is_pointer<T>::value) {
        if (!x) throw std::runtime_error{ "Null pointer dereference." };
        return *x;
    } else {
        return x;
    }
}

void using_constexpr() {
    unsigned long level{ 8998 };
    auto level_ptr = &level;
    auto &level_ref = level;
    printf("Power level = %lu\n", value_of(level_ptr));
    ++*level_ptr;
}

int main(int argc, const char * argv[]) {
    using_partial_application();
    return 0;
}
