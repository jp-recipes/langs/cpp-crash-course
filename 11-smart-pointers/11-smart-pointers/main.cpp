#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/smart_ptr/scoped_array.hpp>
#include <boost/smart_ptr/intrusive_ptr.hpp>

struct DeadMenOfDunharrow {
    DeadMenOfDunharrow(const char* m="") : message{ m } {
        oaths_to_fulfill++;
    }
    ~DeadMenOfDunharrow() {
        oaths_to_fulfill--;
    }
    const char* message;
    static int oaths_to_fulfill;
};

/* Scoped Pointers */

// A scoped pointer expresses non-transferable, exclusive ownership over a single dynamic object
// Non-transerable means that the scoped pointers cannot be moved from one scope to another
// Exclusive ownership means that they can't be copied, so no ther smart ponters can haver ownership of a scoped pointer's dynamic object

// There is no stdlib scoped pointer

// boost:scoped_ptr<PointedToType> my_ptr{ new PointedToType };

int DeadMenOfDunharrow::oaths_to_fulfill{};
using ScopedOathbreakers = boost::scoped_ptr<DeadMenOfDunharrow>;

/* Implicit bool Conversion Based on Ownership */

TEST_CASE("ScopedPtr evaluates to") {
    SECTION("true when full") {
        ScopedOathbreakers aragorn{ new DeadMenOfDunharrow{} };
        REQUIRE(aragorn);
    }
    SECTION("false when empty") {
        ScopedOathbreakers aragorn;
        REQUIRE_FALSE(aragorn);
    }
}

/* RAII Wrapper */

TEST_CASE("ScopedPtr is an RAII wrapper") {
    REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 0);
    ScopedOathbreakers aragorn{ new DeadMenOfDunharrow{} };
    REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
    {
        ScopedOathbreakers legolas{ new DeadMenOfDunharrow{} };
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 2);
    }
    REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
}

/* Pointer Semantics */

// For convenience, scoped_ptr implements the dereference operator* and the member dereference operator-> which simply delegate the calls to the owned dynamic object

TEST_CASE("ScopedPtr supports pointer semantics, like") {
    auto message = "The way is shut";
    ScopedOathbreakers aragorn{ new DeadMenOfDunharrow{ message } };
    SECTION("operator*") {
        REQUIRE((*aragorn).message == message);
    }
    SECTION("operator->") {
        REQUIRE(aragorn->message == message);
    }
    SECTION("get(), which returns a raw pointer") {
        REQUIRE(aragorn.get() != nullptr);
    }
}

/* Swapping */

// Sometimes you wnat to switch the dynamic object owned by a scoped_ptr with the dynamic object owned by another scoped_ptr

TEST_CASE("ScopedPtr supports swap") {
    auto message1 = "The way it shut.";
    auto message2 = "Until the time comes.";
    ScopedOathbreakers aragorn{ new DeadMenOfDunharrow{ message1 } };
    ScopedOathbreakers legolas{ new DeadMenOfDunharrow{ message2 } };

    aragorn.swap(legolas);

    REQUIRE(legolas->message == message1);
    REQUIRE(aragorn->message == message2);
}

/* Resetting and Replacing a scoped_ptr */

// Rarely do you want to destruct an object owned by scoped_ptr before the scoped_ptr dies
// For example, you might want to place its owned object with a new dynamic object

// If you provide no argument, reset simply destroys the owned object
// If you instead provide a new dynamic object as a parameter, rest will first destroy the currently owned object and then gain ownership of the parameter

TEST_CASE("ScopedPtr reset") {
    ScopedOathbreakers aragorn{ new DeadMenOfDunharrow{} };

    SECTION("destructs owned object.") {
        aragorn.reset();

        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 0);
    }

    SECTION("can replace an owned object.") {
        auto message = "It was made by those who are Dead.";
        auto new_dead_men = new DeadMenOfDunharrow{ message };
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 2);

        aragorn.reset(new_dead_men);

        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
        REQUIRE(aragorn->message == new_dead_men->message);
        REQUIRE(aragorn.get() == new_dead_men);
    }
}

/* Non-transferability */

// You cannot move or copy a scoped_ptr, making it non-transferable

void by_ref(const ScopedOathbreakers&) {}
void by_val(ScopedOathbreakers) {}

TEST_CASE("ScopedPtr can") {
    ScopedOathbreakers aragorn{ new DeadMenOfDunharrow };
    SECTION("be passed by reference") {
        by_ref(aragorn);
    }
    SECTION("not be copied") {
        // DOES NOT COMPILE
        //by_val(aragorn);
        //auto son_of_arathorn = aragorn;
    }
    SECTION("not be moved") {
        // DOES NOT COMPILE
        //by_val(std::move(aragorn));
        //auto son_of_arathorn = std::move(aragorn);
    }
}

// Generally, using a boost::scoped_ptr incurs no overhead compared with using a raw pointer

/* boost::scoped_array */

// The boost:scoped_array is a scoped pointer for dynamic arrays
// It supports the same usages as a boost:scoped_ptr but it also implements an operator[] so you can interact with elements of the scoped array in the same way as you can with a raw array

TEST_CASE("ScopedArray support") {
    boost::scoped_array<int> squares {
        new int[5] { 0, 4, 9, 16, 25 }
    };
    squares[0] = 1;
    REQUIRE(squares[0] == 1);
    REQUIRE(squares[1] == 4);
    REQUIRE(squares[2] == 9);
}


/* Unique Pointers */

// A unique pointer has transferable, exclusive ownership over a single dynamic object
// You can move unique pointers, which makes them transferable
// They also have exclusive ownership, so they cannot be copied
// The stdlib has a unique_ptr available in the <memory> header

// Boost dones't offer a unique pointer

// std::unique_ptr<int> my_otr{ new int{ 808 } };

// auto my_ptr = make_unique<int>(808);

/* Supported Operations */

// The std::unique_ptr function supports every operation that boost::scoped_ptr supports
// For example, you can use the following type alias as a drop-in replacement for ScopedOathbreakers

using UniqueOathbreakers = std::unique_ptr<DeadMenOfDunharrow>;

// One of the major differences between unique and scoped pointers is that you can move unique pointers because the're transferable

/* Transferable, Exclusive Ownership */

// Not only are unique pointers transferbale, but they have exclusive ownership (you cannot copy them)

TEST_CASE("UniquePtr can be used in move") {
    auto aragorn = std::make_unique<DeadMenOfDunharrow>();

    SECTION("construction") {
        auto son_of_arathorn{ std::move(aragorn) };
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
    }

    SECTION("assignment") {
        auto son_of_arathorn = std::make_unique<DeadMenOfDunharrow>();
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 2);
        son_of_arathorn = std::move(aragorn);
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
    }
}


/* Unique Arrays */

// Unlike boost:scoped_ptrm std::unique_ptr has built-in dynamic array support
// You just use the array type as the template parameter

// It's very important that you don't initialize a std::unique_ptr<T> with a dynamic array T[]
// Doing so will cause undefined behavior, because you'll be causing a delete of an array (rather than delete[]), the compiler cannot save you, because operator new[] returns a pointer that is indistinguisiable from the kind returned by operator new

TEST_CASE("UniquePtr to array supports operator[]") {
    std::unique_ptr<int[]> squares {
        new int[5]{ 0, 4, 9, 16, 25 }
    };
    squares[0] = 1;
    REQUIRE(squares[0] == 1);
    REQUIRE(squares[1] == 4);
    REQUIRE(squares[2] == 9);
}

/* Deleters */

// The std::unique_ptr has a second, optional template parameter called its deleter type
// A unique pointer's deleter is what gets called when the unique pointer needs to destroy its owned object

// std::unique_ptr<T, Deleter=std::default_delete<T>>

auto my_deleter = [](int* x) {
    printf("Deleting an int ar %p.", x);
    delete x;
};

std::unique_ptr<int, decltype(my_deleter)> my_up{
    new int,
    my_deleter
};

/* Custom Deleters and System Programming */

// You use a custom deleter whenever delete deson't provide the resource-releasing behavior you require
// In some settings, uou'll never need a custom deleter
// In other, like system programming, you might find them quite useful

using FileGuard = std::unique_ptr<FILE, int(*)(FILE*)>;

void say_hello(FileGuard file) {
    fprintf(file.get(), "HELLO DAVE");
}

int using_files() {
    auto file = fopen("HAL9000", "w");

    if (!file) return errno;

    FileGuard file_guard{ file, fclose };

    say_hello(std::move(file_guard));

    return 0;
}

/* Shared Pointers */

// A shared pointer has transferable, non-exclusive ownership over a single dynamic object
// You can move shared pointers, which makes them transferable, and you can copy them, which makes their ownership non-exclusive

// Non-exclusive ownership means that a shared_ptr checks whether any other shared_ptr objects also own the object before destroying it. This way, the last owner is the one to release the owned object

// Both the stdlib and Boost shared_ptr are essentially identical, with the notable exception that Boost's shared pointer doesn't support arrays and requires you to use to boost::shared_array class
// Boost offers a shared pointer for legacy reasons

/* Constructing */

// The std::shared_ptr pointer supports all the same constructors as std::unique_ptr
// std::shared_ptr<int> my_ptr{ new int{ 808 } };

// You also have a corollary std::make_shared template function that forwards argumentos to the pointed-to type's constructor
// auto my_ptr = std::make_shared<int>(808);

// You should generally use make_shared
// Shared pointers require a control blockm which keeps track of several quantities, including the number

// NOTE
// Sometimes you might want to avoid using `make_shared`
// For example, if you'll be using a `weak_ptr`, you`ll still need the control block even if you can deallocate the object even if you can deallocate the object
// In such a situation, you might prefer to have two allocations

// See Control Block

// Because a control block is a dynamic object, shared_ptr objects sometimes need to allocate dynamic objects
// If you wanted to take control over how shared_ptr allocates, you could override `new`
// But this is shooting a sparrow with a cannon
// A more tailored approach is to provide an optional template parameter called an allocator type

/* Specifying an Allocator */

// The allocator is responsible for allocating, creating, destroying, and dealocating objects
// The default

std::shared_ptr<int> sh_ptr {
    new int{ 10 },
    [](int* x) { delete x; },
    std::allocator<int>{}
};

// For technical reasons, you can't use a custom deleter or custom allocator with make_shared
// For this, you can use the sister function of make_shared, wich is std::allocate_shared

auto a_sh_ptr = std::allocate_shared<int>(std::allocator<int>{}, 10);

// NOTE
// Here are two reasons why you can't use a custom deleter with make_shared
// First, make_shared uses `new` to allocate space for the owned object and the control block
// The appropriate deleter for `new` is `delete`, so generally a custom deleter wouldn't be appropriate
// Second, the custom deleter can't generally know how to deal with the control block, only with the owned object

/* Supported Operations */

// The std::shared_ptr supports every operation that std::unique_ptr and boost:scoped_ptr support
// You could use the following type alias as a drop-in replacement for ScopedOathbreakers

using SharedOathbreakers = std::shared_ptr<DeadMenOfDunharrow>;

// The major functional difference between a shared pointer and a unique pointer is that you can copy shared pointers

/* Transferable, Non-Exclusive Ownership */

// Shared pointers are transferable (you can move them), and the have on-exclusive ownership (you can copy them)

TEST_CASE("SharedPtr can be used in copy") {
    auto aragorn = std::make_shared<DeadMenOfDunharrow>();

    SECTION("construction") {
        // Copy constructor
        auto son_of_arathorn{ aragorn };
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1); // still one
    }

    SECTION("assignment") {
        // Copy assignment
        SharedOathbreakers son_of_arathorn;
        son_of_arathorn = aragorn;
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1); // still one
    }

    SECTION("assignment, and original gets discarted") {
        auto son_of_arathorn = std::make_shared<DeadMenOfDunharrow>();
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 2);
        son_of_arathorn = aragorn;
        REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
    }
}

/* Shared Arrays */

// A shared array is a shared pointer that owns a dynamic array and supports operator[]
// It works just like a unique array except it has non-exclusive ownership

/* Deleters */

// Deleters work the same way for shared pointers as they do for unique pointers except you don't need to provide a template paramter with deleter's type
// Simply pass the deleter as the second constructor argument

using SharedFileGuard = std::shared_ptr<FILE>;

/* Weak Pointers */

// A weak pointer is a special kind of smart pointer that has no ownership over the object to which it refers
// Weak pointers allow you to track an object and to convert the weak pointer into a shared pointer only if the tracked object still exists
// This allows you to generate temporary ownership over an object
// Like shared pointers, weak pointers are movable and copyable

// A common useage for weak pointers is 'caches'
// A cache could keep weak pointers to objects so they destruct oonce all other owners release them
// Periodicallym the cache can scan its stored weak pointers and trim those with no other owners

// The stdlib has a std::weak_ptr, and Boost has a boost::weak_ptr
// These are essentially identical

/* Constructing */

// Weak pointer constructors are completely different from scoped, unique, and shared pointers because weak pointers don't directly own dynamic objects
// The default constructor constructs an empty weak pointer
// To construct a weak pointer that tracks a dynamic object, you must construct it using either a shared pointer or another weak pointer

// For example, the following passes a shared pointer into the weak pointer's construct

auto sp = std::make_shared<int>(808);
std::weak_ptr<int> wp{ sp };

// Now the weak pointer wp will track the object owned by the shared pointer sp

/* Obtaining Temporary Ownership */

// Weak pointers invoke their lock method to get temporary ownership of their tracked object
// The lock method always creates a shared pointer
// If the tracked object is alive, the returned shared pointer owns the tracked object
// If the tracked object is no longer alive, the returned shared pointer is empty

TEST_CASE("WeakPtr lock() yields") {
    auto message = "The way is shut";

    SECTION("a shared pointer when tracked object is alive") {
        auto aragorn = std::make_shared<DeadMenOfDunharrow>(message);
        std::weak_ptr<DeadMenOfDunharrow> legolas{ aragorn };
        auto sh_ptr = legolas.lock();
        REQUIRE(sh_ptr->message == message);
        REQUIRE(sh_ptr.use_count() == 2);
    }

    SECTION("empty when shared pointer is empty") {
        std::weak_ptr<DeadMenOfDunharrow> legolas;
        {
            auto aragorn = std::make_shared<DeadMenOfDunharrow>(message);
            legolas = aragorn;
        }
        auto sh_ptr = legolas.lock();
        REQUIRE(nullptr == sh_ptr);
    }
}

/* Intrusive Pointers */

// An intrusive pointer is a shared pointer to an object with an embedded reference count
// Because shared pointers usually keep reference counts, they are not suitable for owning such objects

// It's rare that a situation calls for an intrusive pointer
// But sometimes you'll use an operating system or a framework that contains embedded references
// For example, in Windows COM programming an intrusive pointer can be very useful: COM objects that inherit from IUnknown interface have an AddRef and a Release methodm which increment and decrement an embedded reference count (respectively)

using IntrusivePtr = boost::intrusive_ptr<DeadMenOfDunharrow>;
size_t ref_count{};

void intrusive_ptr_add_ref(DeadMenOfDunharrow* d) {
    ref_count++;
}

void intrusive_ptr_release(DeadMenOfDunharrow* d) {
    ref_count--;
    if (ref_count == 0) delete d;
}

// NOTE
// It's absolutely critical whtat you use only a single DeadMenOfDunharow dynamic object with intrusive pointers when using the previous setup
// The ref_count approach will correctly track only a single object
// If you have multiple dynamic objects owned by different intrusive pointers, the ref_count will become invalid, and you'll get incorrect delete behavior

TEST_CASE("IntrusivePtr use an embedded reference counter.") {
    REQUIRE(ref_count == 0);
    IntrusivePtr aragorn{ new DeadMenOfDunharrow{} };
    REQUIRE(ref_count == 1);
    {
        IntrusivePtr legolas{ aragorn };
        REQUIRE(ref_count == 2);
    }
    REQUIRE(DeadMenOfDunharrow::oaths_to_fulfill == 1);
}

/* Allocators */

// Allocators are low-level objects that service requests for memory
// Int the majority if cases, the default allocator std::allocate is totally sufficient
// It allocates memory using operator new(size_t), which allocate raw memory from the free storage
// It deallocates memory using delete(void*), which deallocates the raw memory

// In some settings, such as gaming, high-frequency trading, scientific analyses, and embedded applications, the memory and computational overhead associated with the default free store operations is unacceptable
// In such settings, it's relatively easy to implement your own allocator

// Note that you really shouldn't implement a custom allocator unless you've conducted some performance testing that indicates that the default allocator is a bottleneck

// You need to provide a template class with the following characteristics for it to work as an allocator

//   An appropriate default constructor
//   A value_type nember corresponding to the template parameter
//   A template constructor that can copy an allocator's internal state while dealing with a change in value_type
//   An allocate method
//   A deallocate method
//   An operator== and an operator!

static size_t n_allocated, n_deallocated;

template <typename T>
struct MyAllocator {
    using value_type = T;
    MyAllocator() noexcept { }

    template <typename U>
    MyAllocator(const MyAllocator<U>&) noexcept { }

    T* allocate(size_t n) {
        auto p = operator new(sizeof(T) * n);
        ++n_allocated;
        return static_cast<T*>(p);
    }
    void deallocate(T* p, size_t n) {
        operator delete(p);
        ++n_deallocated;
    }
};

template <typename  T1, typename T2>
bool operator==(const MyAllocator<T1>&, const MyAllocator<T2>&) {
    return true;
}

template <typename  T1, typename T2>
bool operator!=(const MyAllocator<T1>&, const MyAllocator<T2>&) {
    return false;
}

TEST_CASE("Allocator") {
    auto message = "The way is shut.";
    MyAllocator<DeadMenOfDunharrow> my_alloc;
    {
        auto aragorn = std::allocate_shared<DeadMenOfDunharrow>(my_alloc, message);

        REQUIRE(aragorn->message == message);
        REQUIRE(n_allocated == 1);
        REQUIRE(n_deallocated == 0);
    }

    REQUIRE(n_allocated == 1);
    REQUIRE(n_deallocated == 1);
}








