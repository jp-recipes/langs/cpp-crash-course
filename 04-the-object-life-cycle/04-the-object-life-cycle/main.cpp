#include <stdexcept>
#include <cstring>
#include <cstdio>
#include <utility>

/* An Object's Storage Duration */

// An `object` is a region of storage that has a type and a value
// When you declare a variable, you create an object
// A variable is simple and object that has a name

/* Allocation, Deallocation, and Lifetime */

// You reserve storage for object in a process called allocation
// When you are donde with an object, you release the object's storage in a process called deallocation

/* Automatic Storage Duration (local variables) */

// An automatic object is allocated at the beginning of an enclosing code block, and is deallocated at the end
// The enclosing block is the automatic object's `scope`
// Functiona parameters are automatic

/* Static Storage Duration */

// A static object is declared using the `static` or `extern` keyword
// You declare static variables at the same level you declare functions --at global scope (or namespace scope)
// Static objects with global scope have static storage duration and are allocated when the program starts and deallocated when the program stops

/* Static and the Translation Unit */

// Another feature of being declared at global scope es that it can be accessed from any function in the translation unit
// Remember that a translation unit is what a preprocessor produces after acting on a single source file

/* Internal Linkage */

// When you use the static keyword, you specify `internal linkage`
// Internal linkage means that a variable is inaccesible to other translation units

static int rat_things_power = 200; // Internal Linkage

void power_up_rat_thing(int nuclear_isotopes) {
    rat_things_power = rat_things_power + nuclear_isotopes;
    const auto waste_heat = rat_things_power * 20;
    if (waste_heat > 10000) {
        printf("Warning! Hot doggie!\n");
    }
}

void static_storage_duration() {
    printf("Rat-thing power: %d\n", rat_things_power);
    power_up_rat_thing(100);
    printf("Rat-thing power: %d\n", rat_things_power);
    power_up_rat_thing(500);
    printf("Rat-thing power: %d\n", rat_things_power);
}

/* External Linkage */

// You can alternatively specify `external linkage`
// This makes a variable accessible to other translation units
// For external linkage, you use the `extern` keyword instead of `static`

extern int raw_things_super = 300; // External Linkage

/* Local Static Variable */

void local_static_variable() {
    static int local_int = 1;

    local_int++;
}

/* Static Members */

// You can declare and define integral types within a class definition as long as they are `const`
struct RatThing {
    // These members are essentially similar to static variables and functions declared at global scope
    // However, you must refer to themo by its containing class's name
    // You can not initialize a member within a containing class definition
    static int rat_things_power;
    static void power_up_rat_thing(int nuclear_isotopes) {
        rat_things_power = rat_things_power + nuclear_isotopes;
        const auto waste_heat = rat_things_power * 20;
        if (waste_heat > 10000) {
            printf("Warning! Hot doggie!\n");
        }
        printf("Rat-thing power: %d\n", rat_things_power);
    }
};

int RatThing::rat_things_power = 200;

void static_members_using_scope_resolution_operator() {
    RatThing::power_up_rat_thing(100);
    RatThing::power_up_rat_thing(500);
}

/* Thread-Local Storage Duration */

// Each program has one or more threads that can perform independent operations
// The secuencie of instructions that a thread executes is called its `thread of execution`
// Programmers must take extra precautions when using more than one thread of execution

// Every thread gets its own copy of a variable

void power_up_rat_thing() {
    static thread_local int rat_thing_power = 200;
}

/* Dynamic Storage Duration */

// Objects with dynamic storage duration are allocated and deallocated on request
void dynamic_storage_allocation() {
    int* my_int_ptr1 = new int;         // Allocate memory
    int* my_int_ptr2 = new int{ 42 };   // Allocate memory and initialize it

    delete my_int_ptr1;
    delete my_int_ptr2;
}

/* Dynamic Arrays */

// Dynamic arrays are arrays with dynamic storage duration
void dynamic_arrays() {
    int* my_int_array_ptr = new int[100];

    delete [] my_int_array_ptr;
}

/* Life Cycle */

struct Tracer {
    Tracer(const char* name) : name{ name } {
        printf("%s constructed.\n", name);
    }
    ~Tracer() {
        printf("%s destructed.\n", name);
    }
private:
    const char* const name;
};

static Tracer t1{ "Static variable" };
thread_local Tracer t2{ "Thread-local variable" };

void life_cycle() {
    printf("A\n");
    Tracer t3{ "Automatic variable" };
    printf("B\n");
    const auto* t4 = new Tracer{ "Dynamic variable" };
    printf("C\n");
}

/* Exceptions */

struct Groucho {
    void forget(int x) {
        if (x == 0xFACE) {
            throw std::runtime_error { "I'd be glad to make an exception." };
        }
        printf("Forgot 0x%x", x);
    }
};

void try_catch_blocks() {
    Groucho groucho;
    try {
        groucho.forget(0xC0DE);
        groucho.forget(0xFACE);
        groucho.forget(0xC0FEE);
    } catch (const std::runtime_error& e) {
        printf("exception caught with message: %s\n", e.what());
    }
}

/* Logic Errors */

// Generally, you could avoid these exceptions through more careful programming
// A primary example is when a logical precondition of a class isn't satisfied, such as when a class invariant connot be established
// A class invariant is a feature of a class that is always true

// Since a class invariant is somehtin that a programmer defines, neither the compiler nor the runtime environment can enforce it without help
// You can use a class constructor to check for various conditions, and if you cannot establish a class invariant, you can throw an exception
// If the failure is the result of, say, passing an incorrect parameter to the constructor, a `logi_error` is an appropriate exception to throw

/* Runtime errors */

// Runtime errors derive from `runtime_error` class
// These exceptions help you report error conditions that are outside the program's scope

void handling_exceptions() {
    try {
        throw std::logic_error { "It's not about who wrong it's not about who right" };
    } catch (std::exception& e) {
        // Handles std::logic_error as it inherits from std::exception
    }
}

void handling_any_exception() {
    try {
        throw 'z';
    } catch (...) {
        // Handles any exception, even a 'z'
    }
}

struct CyberdyneSeries800 {
    CyberdyneSeries800() {
        printf("I'm a friend of Sarah Connor.");
    }
    ~CyberdyneSeries800() {
        throw std::runtime_error { "I'll be back." };
    }
};

void throwing_in_destructors() {
    try {
        CyberdyneSeries800 t800;
        throw std::runtime_error { "Come with me if you want to live." };
    } catch (const std::exception& e) {
        printf("Caught exception: %s\n", e.what());
    }
}

struct SimpleString {
    SimpleString(size_t max_size) : max_size { max_size }, length{} {
        if (max_size == 0) {
            throw std::runtime_error{ "Max size must be at least 1." };
        }
        buffer = new char[max_size];
        buffer[0] = 0;
    }

    // Copy Constructor
    SimpleString(const SimpleString& other)
        // Usamos member initializers
        : max_size{ other.max_size },
        // You can use array `new` to initialize buffer becouse you know `other.max_size` is greater than 0
          buffer{ new char[other.max_size] }, length{ other.length } {

        // This is a Deep Copy
        // Rather than copying the pointer buffer, you'll make a new allocation on the free store and then copy all the data pointed to by the original buffer
        std::strncpy(buffer, other.buffer, max_size);
    }

    // Move Constructor
    SimpleString(SimpleString&& other) noexcept
        : max_size{ other.max_size }, buffer(other.buffer), length(other.length) {
        other.length = 0;
        other.buffer = nullptr;
        other.max_size = 0;
    }

    // Copy Assignment
    SimpleString& operator=(const SimpleString& other) {
        if (this == &other) return *this;

        const auto new_buffer = new char[other.max_size];
        delete[] buffer;
        buffer = new_buffer;
        length = other.length;
        max_size = other.max_size;
        std::strncpy(buffer, other.buffer, max_size);
        return *this;
    }

    // Move Assignment
    SimpleString& operator=(SimpleString&& other) noexcept {
        if (this == &other) return *this;
        delete [] buffer;
        buffer = other.buffer;
        length = other.length;
        max_size = other.max_size;
        other.buffer = nullptr;
        other.length = 0;
        other.max_size = 0;
        return *this;
    }

    ~SimpleString() {
        // RAII (Resource Acquisition Is Initialization)
        // CADRe (Constructor Acquire, Destructor Releases)
        delete[] buffer;
    }

    void print(const char* tag) const {
        printf("%s: %s", tag, buffer);
    }

    bool append_line(const char* x) {
        const auto x_len = strlen(x);
        if (x_len + length + 2 > max_size) return false;
        std::strncpy(buffer + length, x, max_size - length);
        length += x_len;
        buffer[length++] = '\n';
        buffer[length] = 0;
        return true;
    }

private:
    size_t max_size;
    size_t length;
    char *buffer;
};

/* Exceptions alternative */

struct HumptyDumpty {
    HumptyDumpty() {}
    bool is_together_again() { return false; }
};

struct Result {
    HumptyDumpty hd;
    bool success;
};

Result make_humpty() {
    HumptyDumpty hd{};
    bool is_valid = true;

    return { hd, is_valid };
}

bool send_kinks_horses_and_men() {
    auto [hd, success] = make_humpty();
    if (!success) return false;
    // Class invariants established
    // --snip--
    return true;
}

/* Copy Semantics */

// Copy Constructors
// It creates a copy and assifns ot to a brand-new object

void copy_constructors() {
    SimpleString a{ 50 };
    a.append_line("We apologize for the");
    SimpleString a_copy{ a };
    a.append_line("inconvenience.");
    a_copy.append_line("incontinence.");
    a.print("a");
    a_copy.print("a_copy");
}

void copy_assignment() {
    SimpleString a{ 50 };
    a.append_line("We apologize for the");

    SimpleString b{ 50 };
    b.append_line("Last message");

    a.print("a");
    b.print("b");
    // If copy assignment is not implemented, this will cause `undefined behavior`
    b = a;
    a.print("a");
    b.print("b");
}

/* Default Copy */

// Oftern, the compiler will generate default implementations for copy construction and copy assignment
// The default implementantion is to invoke copy construction or copy assingment on each of a class's members

void default_copy() {
    struct Replicant {
        Replicant(const Replicant&) = default;
        Replicant& operator=(const Replicant&) = default;
    };

    // Some classes simply cannot or should not be copied
    struct Highlander {
        Highlander() {}
        Highlander(const Highlander&) = delete;
        Highlander& operator=(const Highlander&) = delete;
    };

    Highlander a;
    // Highlander b{ a }; // Bang! There can be only one
}

/* Copy Guidelines */

// Correctness
// You must ensure that class invariants are maintained
// The SimpleString class demostrated that the default copy constructor can violate invariants

// Independence
// After copy assignment or copy constructor, the original object and the copy shouldn't change each other's state during modification
// Had you simply copied buffer from on SimpleString to another, writing to one buffer could overwrite the data from the other

// Equivalence
// The original and the copy should be the same
// The semantics of sameness depend on context
// But generally, an operation applied to the original should yield the same result when applied to the copy

/* Move Semantics */

// Copying can be quite time-consuming at runtime when a large amount of data is involved

struct SimpleStringOwner {
    // my_string no será destruido
    SimpleStringOwner(const SimpleString&& x) : string{ std::move(x) } { }
private:
    SimpleString string;
};

void ref_type(int &x) {
    printf("lvalue reference %d\n", x);
}

void ref_type(int &&x) {
    printf("rvalue reference %d\n", x);
}

int main(int argc, const char * argv[]) {
    auto x = 1;
    ref_type(x);
    ref_type(2);
    ref_type(x + 2);
    return 0;
}
