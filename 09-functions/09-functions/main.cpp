#include <cstdio>
#include <cstdarg>
#include <cstdint>
#include <functional>

/* Function Declarations */

// Function declarations have the following familiar form:
// prefix-modifiers return-type func-name(arguments) suffix-mdifiers;

/* Prefix Modifiers */

// static:
// Indicates that a function that isn't a member of a class has internal linkage
// Internal linkage means the function won't be used outside of this translation unit
// A static method, indicates that the function isn't associated with a translation unit
// A static method is associated with a particular class

// virtual:
// Indicates that a method can be overriden by a child class
// The override modifier indicates to the compiler that a child class intends to override parent's virtual function

// constexpr:
// Indicates that a function should be evaluated at compile time if possible

// [[noreturn]]:
// Indicates that this function won't return
// This attribute helps the compiler to optimize your code

// inline:
// TODO

// On most platforms, a function call compiles into a series of instructions

// 1: Place arguments into registers and on the call stack
// 2: Push a return address onto the call stack
// 3: Jump to the called function
// 4: After the function completes, jump to the return address
// 5: Clean up the call stack

/* Suffix Modifiers */

// noexcept:
// Indicates that the function willnever throw an exception

// const:
// Indicates that the method won't modify an instance of its class
// Allows 'const references types' to invoke the method

// final and override:
// Indicates that a method cannot be overriden by a child class
// It's effectively the opposite of virtual

// volatile:
// A volatile object's value can change at any given time, so the compiler must treat all accesses to volatile objects as visible side effects for optimization purposes

struct Distillate {
    int apply() volatile {
        return ++applications;
    }
private:
    int applications{};
};

void using_volatile() {
    volatile Distillate ethanol;
    printf("%d Tequila\n", ethanol.apply());
    printf("%d Tequila\n", ethanol.apply());
    printf("%d Tequila\n", ethanol.apply());
    printf("Floor!");
}

/* auto Return Types */

// auto my-function(arg1-type arg1, arg2-type arg2, ...) {
//     return any type and the compiler will deduce what auto means
//}

// It's possible to extends the auto-return-type deduction syntax to provide the return type as a suffix with the arrow operator ->
// This way, you can append and expression that evaluates to the dunction's return type

// auto my-function(arg1-type arg1, arg2-type arg2, ...) -> type-expression {
//     return an object with the type matching the type-expression above
//}

// Usually, you wouldn't use this pedantic form, but in certain situations it's helpful
// For example, this form of auto type deduction is commonly paired with a decltype type expression
// A decltype expression yields another expressio's resultant type

// decltype(expression)

// This declatype yields an int type

// decltype(100)

template<typename X, typename Y>
auto add(X x, Y y) -> decltype(x + y){
    return x + y;
}

void using_auto_return_type() {
    auto my_double = add(100., -10);
    printf("decltype(double + int) = double; %f\n", my_double);

    auto my_uint = add(100U, -20);
    printf("decltype(uint + int) = uint; %u\n", my_uint);

    auto my_ulonglong = add(char{ 100 }, 54'999'900ull);
    printf("decltype(char + ulonglong) = ulonglong; %llu\n", my_ulonglong);
}

/* Overload Resolution */

// Is the process that the compiler executes when matching a function invocation with its proper implementation

// Roughly, the matching process proceeds as follows

// 1: The compiler will look for an exact type match
// 2: The compiler will try using integral and floating-point promotions to get a suitable overload (for example, int to long or float to double)
// 3: The compiler will try to match using standard conversions like integral type to floating-point or casting a pointer-to-child into a pointer-to-parent
// 4: The compiler will look for a user-defined vonversion
// 5: The compiler will look for a varadic function

/* Varadic Functions */

// You declare varadic functions by placing ...as the final paramter in the function's parameter list
// You cannot extract elements from the varadic parameters directly
// Instead , you access individual arguments using the utility functions in the <cstdarg> header

// va_list:  Used to declara a local variable representing the varadic arguments
// va_start: Enables access to the varadic arguments
// va_end:   Used to end iteration over the varadic arguments
// va_arg:   Used to iterate over each element in the varadic arguments
// va_copy:  Makes a copy of the varadic arguments

// Varadic functions are holdiver from C
// Generally, varadic functions are unsage and a common source of security vulnerabilities
// There are at least two major problems with varadic functions

// Varadic arguments are not type-safe
// The number of elements in the varadic arguments must be tracked separately

int sum(size_t n, ...) {
    va_list args;
    va_start(args, n);
    int result{};
    while (n--) {
        auto next_element = va_arg(args, int);
        result += next_element;
    }
    va_end(args);
    return result;
}

void using_varadic_function() {
    printf("The answer is %d.", sum(6, 2, 4, 6, 8, 10, 12));
}

/* Varadic Templates */

// Varadic templaes enables you to create function templates that accept varadic, same-typed arguments
// You can use sizeof...(args) to obtain the parameter pack's size

/* Programming with Parameter Packs */

// It's not possible to index into a parameter pack directly
// You must invoke the function template from within itself -a process called compile-time-recursion- to recursively iterate over the elements in a parameter pack

template<typename T, typename...Args>
void my_func(T x, Args...args) {
    // Use x, then recurse:
    my_func(args...);
}

/* Revisiting the sum Function */

template<typename T>
constexpr T sum(T x) {
    return x;
}

template<typename T, typename... Args>
constexpr T sum(T x, Args... args) {
    return x + sum(args...);
}

void using_varadic_template() {
    printf("The answer is %d.", sum(2, 4, 6, 8, 10, 12));
}

/* Fold Expressions */

// A fold expression computes the result of using a binary operator over all the arguments of a parameter pack
// Fold expressions are distinct from but related to varadic templates

// Their usage
// (... binary-operator parameter-pack)
// For example
// (... + args)

template<typename... T>
constexpr auto sum(T... args) {
    return (... + args);
}

void using_fold_exapression() {
    printf("The answer is %d.", sum(2, 4, 6, 8, 10, 12));
}

/* Function Pointers */

/* Declaring a Function Pointer */

// return-type (*pointer-name)(arg-type1, arg-type2, ...)

float add(float a, int b) {
    return a + b;
}

float substract(float a, int b) {
    return a - b;
}

void using_function_pointers() {
    const float first{ 100 };
    const int second{ 20 };

    float(*operation)(float, int){};
    printf("operation initialized to 0x%p", operation);

    operation = &add;
    printf("&add = 0x%p\n", operation);
    printf("%g + %d = %g\n", first, second, operation(first, second));

    operation = substract; // address operator not necessary
    printf("&subtract = 0x%p\n", operation);
    printf("%g - %d = %g\n", first, second, operation(first, second));
}

/* Type Aliases and Function Pointers */

// using alias-name = return-type(*)(arg-type1, arg-type2, ...)

using operation_func = float(*)(float, int);

/* The Function-Call Operator */

// You can make user-defined types callable or invocable by overloading the function-call operator `operator()()`
// Such a type is called function type, and instances of a function type are called function objects
// The function-call ioeratir permits any combination of argument types, and modifiers (except static)

// struct type-name {
//     return-type operator()(arg-type1 arg1, arg-type2 arg2, ...) {
//         // Body of function.call ioeratir
//     }
// }

/* A Counting Example */

struct CountIf {
    CountIf(char x) : x{ x } { }
    size_t operator()(const char* str) {
        size_t index{}, result{};
        while (str[index]) {
            if (str[index] == x) result++;
            index++;
        }
        return result;
    }
private:
    const char x;
};

void unsing_function_call_operator() {
    CountIf s_conunter{ 's' };
    auto sally = s_conunter("Sally sells seashells by the seashore.");
    printf("Sally: %zd\n", sally);

    auto sailor = s_conunter("Saolir went to sea to see what he could see.");
    printf("Salior: %zd\n", sailor);
    auto buffalo = CountIf{ 'f' }("Buffalo buffalo Buffalo buffalo "
                                  "Buffalo buffalo Buffalo buffalo.");
    printf("Buffalo: %zd\n", buffalo);
}

/* Lambda Expressions */

// Lambda expressions construct unnamed functions objects succintly
// The function object implies the function type, resulting in a quick way to declare a function object on the fly

// Usage

// There are five componentes to a lambda expression

// captures:    The member variables of the function object (the partially applied parameters)
// parameters:  The arguments required to invoke the function object
// body:        The function object's code
// specifiers:  Elements like constexpr, mutable, noexcept, and [[noreturn]]
// return type: The type returned by the function object

// Lambda expression usage is as follow:
// [captures] (paramteres) modifiers -> return-type { body }

/* Lambda Parameters and Bodies */

// Lambda expressions produce function objects
// As functions objects, lambdas are callable
// Most of the time, you'll want your function object to accept parameters upon invocation

// The following lambda expression yields a function object that will square its int argument
// [](int x) { return x * x }

template<typename Fn>
void transform(Fn fn, const int* in, int* out, size_t length) {
    for(size_t i{}; i<length; i++) {
        out[i] = fn(in[i]);
    }
}

void using_lambda_expression() {
    const size_t len{ 3 };
    int base[]{ 1, 2, 3 }, a[len], b[len], c[len];
    transform([](int x) { return 1; }, base, a, len);
    transform([](int x) { return x; }, base, b, len);
    transform([](int x) { return 10*x+5; }, base, c, len);
    for (size_t i{}; i<len; i++) {
        printf("Element %zd: %d %d %d\n", i, a[i], b[i], c[i]);
    }
}

/* Default arguments */

// You can provide default arguments to a lambda

void using_default_arguments() {
    auto increment = [](auto x, int y = 1) { return x + y; };
    printf("increment(10)    = %d\n", increment(10));
    printf("increment(10, 5) = %d\n", increment(10, 5));
}

/* Generic Lambdas */

// Generic lambdas are lambda expression templates
// For one or more parameters, you specify `auto` rather than a concrete type
// These `auto` types become template parameters, meaning the compiler will stamp out a custom instantiation of the lambda

template<typename Fn, typename T>
void transform(Fn fn, const T* in, T* out, size_t len) {
    for(size_t i{}; i<len; i++) {
        out[i] = fn(in[i]);
    }
}

void using_generic_lambdas() {
    constexpr size_t len{ 3 };
    int base_int[]{ 1, 2, 3 }, a[len];
    float base_float[]{ 10.f, 20.f, 30.f }, b[len];

    auto translate = [](auto x) { return 10 * x + 5; };

    transform(translate, base_int, a, len);
    transform(translate, base_float, b, len);

    // Without a generic lambda, you'd have to declare the parameter types explicitly
    // transform([](int x) { return 10 * x + 5 }, base_int, a, len);
    // transform([](double x) { return 10 * x + 5 }, base_float, b, len);

    for (size_t i{}; i < len; i++) {
        printf("Element %zd: %d %f\n", i, a[i], b[i]);
    }
}

/* Lambda Return Types */

// The compiler deduces a lambda's return type for you
// To take over from the compiler, you use the arrow -> syntax, as in the following

// [](int x, double y) -> double { return x + y; }

// This lambda expression eccepts and int and a double and returns a double
// You can also use `decltype` expressions wich can be useful with generic lambdas

// [](auto x, double y) -> decltype(x+y) { return x + y }

// This isn't common

/* Lambda Captures */

// Lambda captures inject objects into the lambda
// The injected objects help to modify the behavior of the lambda
// A lambda can capture by reference or by value. By default, lambda capure by value

// A lambda's capture list is analogous to a function type's constructor

void using_lambda_captures_by_value() {
    char to_count{ 's' };

    auto s_counter = [to_count](const char* str) {
        size_t index{}, result{};
        while(str[index]) {
            if(str[index] == to_count) result++;
            index++;
        }
        return result;
    };

    auto sally = s_counter("Sally sells seashells by the seashore.");
    printf("Sally: %zd\n", sally);

    auto sailor = s_counter("Salior went to sea to see what he could see.");
    printf("Sailor: %zd\n", sailor);
}

void using_lambda_captures_by_reference() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [to_count, &tally](const char* str) {
        size_t index{}, result{};
        while (str[index]) {
            if (str[index] == to_count) result++;
            index++;
        }
        tally += result;
        return result;
    };

    printf("Tally: %zd\n", tally);
    auto sally = s_counter("Sally sells seashells by the seashore.");

    printf("Sally: %zd\n", sally);
    printf("Tally: %zd\n", tally);

    auto sailor = s_counter("Sailor went to sea to see what he could see.");
    printf("Sailor: %zd\n", sailor);
    printf("Tally: %zd\n", tally);
}

/* Default Capture */

// So far, you've had to capture each element by name
// Sometimes this style od capturing is called named capure
// If you're lazy, you can capture all automatic variables used within a lambda using defult capture

void using_default_capture_by_reference() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [&](const char* str) { // Catpured: to_count, and tally;
        size_t index{}, result{};
        while(str[index]) {
            if (str[index] == to_count) result++;
            index++;
        }
        tally += result;
        return result;
    };

    printf("Tally: %zd\n", tally);
    auto sally = s_counter("Sally sells seashells by the seashore.");
    printf("Sally: %zd\n", sally);
    printf("Tally: %zd\n", tally);
    auto sailor = s_counter("Sailor went to sea to see what he could see.");
    printf("Sailor: %zd\n", sailor);
    printf("Tally: %zd\n", tally);
}

void using_default_capture_by_value() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [=](const char* str) { // Catpured: to_count, and tally;
        size_t index{}, result{};
        while(str[index]) {
            if (str[index] == to_count) result++;
            index++;
        }
        // tally += result;  BANG!, you are not allowed to modify variables captured by value
        // You have to add mutable keyword to the lambda expression
        // The mutable keyword allows you to modify value-captured variables
        // This includes calling non-const methods on that object
        return result;
    };

    printf("Tally: %zd\n", tally);
    auto sally = s_counter("Sally sells seashells by the seashore.");
    printf("Sally: %zd\n", sally);
    printf("Tally: %zd\n", tally);
    auto sailor = s_counter("Sailor went to sea to see what he could see.");
    printf("Sailor: %zd\n", sailor);
    printf("Tally: %zd\n", tally);
}

// The following will work, but each of the three times you print `tally`, you get a zero value
// That's because `tally` gets copied by value, inside the lambda is a differente value

void using_default_capture_by_value_and_mutable() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [=](const char* str) mutable { // MUTABLE
        size_t index{}, result{};
        while(str[index]) {
            if (str[index] == to_count) result++;
            index++;
        }
        tally += result; // Ok, because it's mutable
        return result;
    };

    printf("Tally: %zd\n", tally);
    auto sally = s_counter("Sally sells seashells by the seashore.");
    printf("Sally: %zd\n", sally);
    printf("Tally: %zd\n", tally);
    auto sailor = s_counter("Sailor went to sea to see what he could see.");
    printf("Sailor: %zd\n", sailor);
    printf("Tally: %zd\n", tally);
}

// Mix default caputre with a named capture

void mixing_default_capture_and_named_capture() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [&, to_count](const char* str) mutable { // MUTABLE
        // ...
    };

    // ...
}

/* Initializer Expressions in Capture Lists */

void using_initializer_in_capture_list() {
    char to_count{ 's' };
    size_t tally{};

    auto s_counter = [&tally, my_char=to_count](const char* str) mutable { // MUTABLE
        size_t index{}, result{};
        while(str[index]) {
            if (str[index] == my_char) result++;
            index++;
        }
        tally += result; // Ok, because it's mutable
        return result;
    };

    // ...
}

/* Capturing this */

struct LambdaFactory {
    LambdaFactory(char in) : to_count{ in }, tally{} { }

    auto make_lambda() {
        return [this](const char *str) {
            size_t index{}, result{};
            while (str[index]) {
                if (str[index] == to_count) result++;
                index++;
            }
            tally += result;
            return result;
        };
    }

    const char to_count;
    size_t tally;
};

void using_capture_this() {
    LambdaFactory factory{ 's' };
    auto lambda = factory.make_lambda();

    printf("Tally: %zd\n", factory.tally);
    printf("Sally: %zd\n", lambda("Sally sells seashells by the seashore."));
    printf("Tally: %zd\n", factory.tally);
    printf("Sailor: %zd\n", lambda("Sailor went to sea to see what he couldsee."));
    printf("Tally: %zd\n", factory.tally);
}

/* constexpr Lambda Expressions */

// All lambda expressions are constexpre as long as the lambda can be invoked at compile time
// [] (int x) constexpr { return x * x; }

/* std::function */

// Sometimes you just want a uniform container for storing callable objects
// The std::function class template from the <functional> header is a polymorphic wrapper around a callable object
// In other words, it's a generic function pointer
// You can store a static function, a function object, or a lambda into a std::function

// The function class is in the `stdlib`

// With functions, you can:
// * Invoke without the caller knowing the function's implementation
// * Assing, move, and copy
// * Have an empty state, similar to a nullptr

/* Declaring a Function */

// You must provide a single template parameter containing the function prototype of the callable object

// std::function<retur-type(arg-type-1, arg-type-2, etc.)>

/* Empty Functions */

// If you invoke a std::function with no contained object, std::function will throw a std::bad_function_call exception

void bad_empty_function_initialization() {
    std::function<void()> func; // It's in an empty state
    try {
        func();
    } catch (const std::bad_function_call& e) {
        printf("Exception: %s", e.what());
    }
}

/* Assigning a Callable Object to a Function */

// To assign a callable object to a function, you can either use the constructor or assignment operator of function

void static_func() {
    printf("A static function.\n");
}

void using_callable_object() {
    std::function<void()> func { [] { printf("A lambda.\n"); } };
    func();
    func = static_func;
    func();
}

/* An Extended Example */

// You can construct a function with callable objects, as long as the object supports the function semantics implied by the template parameter of function

size_t count_spaces(const char* str) {
    size_t index{}, result{};
    while (str[index]) {
        if (str[index] == ' ') result++;
        index++;
    }
    return result;
}

void extended_example() {
    std::function<size_t(const char*)> funcs[] {
        count_spaces,
        CountIf{ 'e' },
        [](const char* str) {
            size_t index{};
            while (str[index]) index++;
            return index;
        }
    };

    auto text = "Sailor went to sea to see what he could see.";

    size_t index{};
    for (const auto& func : funcs) {
        printf("func #%zd: %zd\n", index++, func(text));
    }
}

/* The main Function and the Command Line */

// All C++ programs must contins a global function with the name main

/* The Three main Overloads */

// int main();
// int main(int argc, char* argv[]);
// int main(int argc, char* argv[], impl-parameters);

int main(int argc, const char * argv[]) {
    return 0;
}
